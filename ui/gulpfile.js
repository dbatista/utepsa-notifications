//Librerias
var gulp = require('gulp-param')( require('gulp'), process.argv);
var Server = require('karma').Server;
var uglify = require('gulp-uglify');
var del = require('del');
var concat = require('gulp-concat');
var rename = require('gulp-rename');
var mainBowerFiles = require('main-bower-files');
var useref = require('gulp-useref');
var gulpNgConfig = require('gulp-ng-config');


//Variables
var buildDirectory = "build/";
var appDirectory = {
        app : ['app/**/*.js', 'app/**/*.html'],
        bower: 'bower_components',
        images: ['images/**/*.+(png|jpg|jpeg|gif|svg)',],
        js: ['js/**/*.min.js', 'js/**/*.css', 'js/**/*.png'],
        css: ['styles/**/*.min.css'],
        index: ['index.html']
    };
/**
 * Run test once and exit
 */
gulp.task('test', function (done) {
    new Server({
        configFile: __dirname + '/karma.conf.js',
        singleRun: true
    }, done).start();
});

gulp.task('clean:build', function () {
    del(buildDirectory);
});

//Copia los archivos para desarrollo
gulp.task('copy:app-dev', function () {
    gulp.src(appDirectory.app)
        .pipe(gulp.dest(buildDirectory + "app"));

    gulp.src(mainBowerFiles({paths: {bowerJson: 'bower.json', bowerDirectory: appDirectory.bower}}))
        .pipe(uglify())
        .pipe(gulp.dest(buildDirectory + "js"));
});

//Copia los archivo para produccion
gulp.task('copy:app-pro', function () {
    gulp.src(appDirectory.app[0])
        .pipe(uglify())
        .pipe(gulp.dest(buildDirectory + "app"));

    gulp.src(appDirectory.app[1])
        .pipe(gulp.dest(buildDirectory + "app"));
});

//Copia archivos de bower_components
gulp.task('copy:bower-components', function () {
    var bowerFiles = mainBowerFiles({includeDev: true});
    gulp.src(bowerFiles)
        .pipe(concat('bower_components.js'))
        .pipe(uglify())
        .pipe(rename({ suffix: '.min'}))
        .pipe(gulp.dest(buildDirectory + "js"))
});

//Copia imagenes
gulp.task('copy:images', function () {
   gulp.src(appDirectory.images)
       .pipe(gulp.dest(buildDirectory + 'images'));
});

//Copia js
gulp.task('copy:js', function () {
    gulp.src(appDirectory.js)
        .pipe(gulp.dest(buildDirectory + 'js'));
});

//Copia styles
gulp.task('copy:styles', function () {
    gulp.src(appDirectory.css)
        .pipe(gulp.dest(buildDirectory + 'styles'));
});

//Copia html
gulp.task('copy:index', function () {
    gulp.src(appDirectory.index)
        .pipe(useref())
        .pipe(gulp.dest(buildDirectory))
});

//Configuracion App Angular
gulp.task('app:config', function (ENV) {
    gulp.src('./app/config.json')
        .pipe(gulpNgConfig('utepsa-notifications', { environment: ENV, wrap: ';(function() { <%= module %> })();' }))
        .pipe(gulp.dest('./app'))
});


gulp.task('build:dev', ['clean:build','copy:app-dev', 'copy:bower-components', 'copy:images', 'copy:js', 'copy:styles', 'copy:index']);
gulp.task('build:pro', ['clean:build','copy:app-pro', 'copy:bower-components', 'copy:images', 'copy:js', 'copy:styles', 'copy:index']);