;(function() {

    angular.module("utepsa-notifications").factory("dashboardService", dashboardService);

    dashboardService.$inject = ['$http','API','$localStorage'];

    function dashboardService($http,API,$localStorage) {

        function getFilesAvailables(){
            return $http({
                method: 'GET',
                url: API.url+'student/'+ $localStorage.currentUserWeb.student+'/files',
                withCredentials: true,
                headers: {
                    'Content-Type': 'application/json; charset=utf-8'
                }
            }).then(function(result) {
                return result.data;
            }).catch(function(error) {
                return error;
            });
        }

        var service={
            getFilesAvailables: getFilesAvailables,
        };
        return service;
    }

})();
