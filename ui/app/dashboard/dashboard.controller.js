;(function() {

    angular.module("utepsa-notifications").controller('DashboardController', DashboardController);

    DashboardController.$inject = ['$state','$scope', 'resizeTemplate','dashboardService','notificationsService','status_code'];

    function DashboardController($state,$scope, resizeTemplate, dashboardService, notificationsService,status_code) {
        $scope.notificationss = [];
        $scope.itemsNotificationss="";
        $scope.itemsFiles ="";
        $scope.files=[];
        $scope.spinnerNotifications = true;
        $scope.spinnerFile = true;
        $scope.messageAlertNotification = "";
        $scope.alertErrorNotification = false;
        $scope.messageAlertFile= "";
        $scope.alertErrorFile = false;
        resizeTemplate.resize();
        getNotificatios();
        getFilesAvailable();

        function getNotificatios(){
            var promise = notificationsService.getNotifications(0);
            if(promise){
                promise.then(function (result) {
                    try{
                        $scope.itemsNotificationss = result;
                        $scope.spinnerNotifications = false;
                        if ($scope.itemsNotificationss.status == -1) {
                            $scope.messageAlertNotification = "  Problema de conexión";
                            $scope.alertErrorNotification = true;
                        }
                        if ($scope.itemsNotificationss.code == status_code.OK) {
                            angular.forEach($scope.itemsNotificationss.data, function(item){
                                $scope.notificationss.push(item);
                            });
                        }
                        if ($scope.itemsNotificationss.code == status_code.NOT_FOUND) {
                            $scope.messageAlertNotification = "  Estudiante no encontrado";
                            $scope.alertErrorNotification = true;
                        }
                        if ($scope.itemsNotificationss.code == status_code.INTERNAL_SERVER_ERROR) {
                            $scope.messageAlertNotification = "  Problema interno del servidor";
                            $scope.alertErrorNotification = true;
                        }
                        if ($scope.itemsNotificationss.status == status_code.INTERNAL_SERVER_ERROR) {
                            $scope.messageAlertNotification = "  Problema interno del servidor";
                            $scope.alertErrorNotification = true;
                        }
                    }catch (e){

                    }
                });
            }
        }

        function getFilesAvailable(){
            var promise = dashboardService.getFilesAvailables();
            if(promise){
                promise.then(function (result) {
                    $scope.itemsFiles = result;
                    $scope.spinnerFile = false;
                    if ($scope.itemsFiles.status == -1) {
                        $scope.messageAlertFile = "  Problema de conexión";
                        $scope.alertErrorFile = true;
                    }
                    if ($scope.itemsFiles.code == status_code.OK) {
                        angular.forEach($scope.itemsFiles.data, function(item){
                            $scope.files.push(item);
                        });
                    }
                    if ($scope.itemsFiles.code == status_code.NOT_FOUND) {
                        $scope.messageAlertFile = "  Estudiante no encontrado";
                        $scope.alertErrorFile = true;
                    }
                    if ($scope.itemsFiles.code == status_code.INTERNAL_SERVER_ERROR) {
                        $scope.messageAlertFile = "  Problema interno del servidor";
                        $scope.alertErrorFile = true;
                    }
                    if ($scope.itemsFiles.status == status_code.INTERNAL_SERVER_ERROR) {
                        $scope.messageAlertFile = "  Problema interno del servidor";
                        $scope.alertErrorFile = true;
                    }
                });
            }
        }

        var stopActions = function ($event) {
            if ($event.stopPropagation) {
                $event.stopPropagation();
            }
            if ($event.preventDefault) {
                $event.preventDefault();
            }
            $event.cancelBubble = true;
            $event.returnValue = false;
        };

        $scope.open = function ($event) {
            openSidebar();
            stopActions($event);
        };

        $scope.close = function ($event) {
            hideSidebar();
            stopActions($event);
        };

        $scope.stop = function ($event) {
            stopActions($event);
        }
    }

})();
