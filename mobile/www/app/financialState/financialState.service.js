;(function() {

    angular.module("utepsa-notifications").factory("financialStateService", financialStateService);

    financialStateService.$inject = ['$http','API','$localStorage'];

    function financialStateService($http,API,$localStorage) {

        function getFinancialStates(){
            return $http({
                method: 'GET',
                url: API.url+'student/'+$localStorage.currentUser.student+'/FinancialState',
                withCredentials: true,
                headers: {
                    'Content-Type': 'application/json; charset=utf-8'
                }
            }).then(function(result) {
                return result;
            }).catch(function(error) {
                return error;
            });
        }

        var service={
            getFinancialStates: getFinancialStates,
        };
        return service;
    }

})();
