;(function() {

    angular.module("utepsa-notifications").controller('DocumentsStudentController', DocumentsStudentController);

    DocumentsStudentController.$inject = ['$scope', 'resizeTemplate','documentsStudentService','status_code'];

    function DocumentsStudentController($scope, resizeTemplate, documentsStudentService,status_code) {
        $scope.documentsStudentItems = "";
        $scope.documentsStudent=[];
        resizeTemplate.resize();
        getDocumentsStudent();
        $scope.alertError=false;
        $scope.messageAlert="";
        $scope.spinnerDocumentsStudent = true;

        function getDocumentsStudent(){
            var promise = documentsStudentService.getDocumentsStudent();
            if(promise){
                promise.then(function (result) {
                    $scope.documentsStudentItems = result;
                    $scope.spinnerDocumentsStudent = false;
                    if ($scope.documentsStudentItems.status == -1) {
                        $scope.messageAlert = "  Problema de conexión";
                        $scope.alertError = true;
                    }
                    if ($scope.documentsStudentItems.code == status_code.OK) {
                        angular.forEach($scope.documentsStudentItems.data, function(item){
                            $scope.documentsStudent.push(item);
                        });
                    }
                    if ($scope.documentsStudentItems.code == status_code.NOT_FOUND) {
                        $scope.messageAlert = "  Estudiante no encontrado";
                        $scope.alertError = true;
                    }
                    if ($scope.documentsStudentItems.code == status_code.INTERNAL_SERVER_ERROR) {
                        $scope.messageAlert = "  Problema interno del servidor";
                        $scope.alertError = true;
                    }
                    if ($scope.documentsStudentItems.status == status_code.INTERNAL_SERVER_ERROR) {
                        $scope.messageAlert = "  Problema interno del servidor";
                        $scope.alertError = true;
                    }
                });
            }
        }

        $scope.numberDocumentsDelivered = function(){
            var total = 0;
            angular.forEach($scope.documentsStudent, function(item){
                if(item.state==true){
                    total ++;
                }
            });
            return total;
        };

        $scope.numberDocuments = function(){
            var total = 0;
            angular.forEach($scope.documentsStudent, function(item){
                total ++;
            });
            return total;
        };

        $scope.percentageOfDocuments = function(){
            var total = parseFloat(100/$scope.numberDocuments()*$scope.numberDocumentsDelivered());
            return total.toFixed(2);
        };
    }

})();