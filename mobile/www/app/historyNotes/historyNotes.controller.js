;(function() {

  angular.module("utepsa-notifications").controller('HistoryNotesController', HistoryNotesController);

    HistoryNotesController.$inject = ['$scope', 'resizeTemplate', 'historyNotesService','status_code'];

    function HistoryNotesController($scope, resizeTemplate, historyNotesService,status_code) {
        $scope.itemshistoryNotes=[];
        $scope.historyNotesOfStudent=[];
        $scope.coursesFailed=[];
        resizeTemplate.resize();
        getHistoryNote();
        $scope.alertError=false;
        $scope.messageAlert="";
        $scope.spinnerHistoryNotes = true;

        function getHistoryNote(){
          var promise = historyNotesService.getHistoryNotes();
          if(promise){
            promise.then(function (result) {
                $scope.itemshistoryNotes = result;
                getCoursesReproved($scope.itemshistoryNotes);
                $scope.spinnerHistoryNotes = false;
                if ($scope.itemshistoryNotes.status == -1) {
                    $scope.messageAlert = "  Problema de conexión";
                    $scope.alertError = true;
                }
                if ($scope.itemshistoryNotes.code == status_code.OK) {
                    $scope.historyNotesOfStudent =$scope.itemshistoryNotes;
                }
                if ($scope.itemshistoryNotes.code == status_code.NOT_FOUND) {
                    $scope.messageAlert = "  Estudiante no encontrado";
                    $scope.alertError = true;
                }
                if ($scope.itemshistoryNotes.code == status_code.INTERNAL_SERVER_ERROR) {
                    $scope.messageAlert = "  Problema interno del servidor";
                    $scope.alertError = true;
                }
                if ($scope.itemshistoryNotes.status == status_code.INTERNAL_SERVER_ERROR) {
                    $scope.messageAlert = "  Problema interno del servidor";
                    $scope.alertError = true;
                }
            });
          }
        }

    function getCoursesReproved(result) {
      if(result.data.disapproved>0) {
          angular.forEach(result.data.courses, function(item){
              if(item.note < item.minimunNote){
                  $scope.coursesFailed.push(item);
              }
          });
      }
    }

    }
})();