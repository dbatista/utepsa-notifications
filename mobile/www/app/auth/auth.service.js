(function () {
    'use strict';
 
    angular
        .module('utepsa-notifications')
        .factory('AuthenticationService', AuthenticationService);
    
    AuthenticationService.$inject = ['$state', '$http', '$localStorage' ,'API'];

    function AuthenticationService($state, $http, $localStorage, API) {
        var service = {};
 
        service.Login = Login;
        service.Logout = Logout;
        service.GcmRegistration=GcmRegistration;
 
        return service;
 
        function Login(idStudent, password, callback) {
            $http({
              method: 'POST',
              url: API.url + 'credential',
              withCredentials: true,
              headers: { username: idStudent, password: password }
            }).then(function(result) {
                callback(result);
            }).catch(function(error) {
                callback(error);
            });
        }

        function Logout() {
            // remove user from local storage and clear http auth header
            GcmRegistration($localStorage.currentUser.idCredential,'NotRegistered', function (result) {
                    if (result === true) {
                        // console.log('GCM registration complete. Id is: ', gcmId);
                    } else {
                        // console.log('No se pudo registrar el dispositivo');
                    }
                });
                delete $localStorage.currentUser;
                delete $localStorage.registrationGCM;
                delete $localStorage.postGcm;
                $state.transitionTo("login");
                event.preventDefault();
        }

        function GcmRegistration(idCredential, gcmId, callback){
            return $http({
                method: 'PUT',
                url: API.url + 'credential/'+ idCredential +"/registerGcm/"+ gcmId,
                withCredentials: true
            }).then(function(response) {
                if (response.status == 200) {
                    callback(true);
                } else {
                    callback(false);
                }
            }).catch(function(error) {
                    return error;
                });
        }
    }
})();
