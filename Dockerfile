FROM nginx

COPY ./ui /usr/share/nginx/html

COPY ./administrator /usr/share/nginx/html/admin

ADD ./files /usr/share/nginx/html/files

EXPOSE 80
