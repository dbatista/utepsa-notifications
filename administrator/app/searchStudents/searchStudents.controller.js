/**
 * Created by Android on 2/13/2017.
 */
;(function() {

    angular.module("utepsa-administrator").controller('searchStudentController', searchStudentController);

    searchStudentController.$inject = ['$scope', 'resizeTemplate', 'searchStudentService','status_code','API'];

    function searchStudentController($scope, resizeTemplate, searchStudentService,status_code,API) {

        resizeTemplate.resize();
        //spinners
        $scope.spinnerStudentProfile = true;
        //alerts
        $scope.messageAlert= "";
        $scope.alertError = false;
        $scope.messageAlertHisotryNotes= "";
        $scope.alertErrorHisotryNotes = false;
        $scope.alertErrorHistoryNotesByCourse=false;
        $scope.messageAlertHistoryNotesByCourse="";

        $scope.studentByName="";
        $scope.listStudentByName="";

        $scope.itemsPermission=[];
        $scope.permissions=[];

        $scope.select2s = function () {
            $(".select2").select2({
                ajax: {
                    url: function (term) {
                        return API.url+'student/search?fullName='+term.term;
                    },
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            q: params.term,
                            page: params.page
                        };
                    },
                    processResults: function (data, params) {
                        // parse the results into the format expected by Select2
                        // since we are using custom formatting functions we do not need to
                        // alter the remote JSON data, except to indicate that infinite
                        // scrolling can be used
                        params.page = params.page || 1;
                        return {
                            results: data.data,
                            pagination: {
                                more: (params.page * 30) < data.total_count
                            }
                        };
                    },
                    cache: true
                },
                escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
                minimumInputLength: 3,
                templateResult: formatState,
                templateSelection: formatRepoSelection,
            });
        };
        function formatRepoSelection(item){
            if (item.agendCode==undefined) {
                item.title="Busqueda por Registro, Nombres, Apellidos, Carrera";
                return item.title;  
            }else {
                getStudentProfile(item.registerNumber);
                return item.registerNumber + '&nbsp;&nbsp;' +item.name+'&nbsp;'+item.fatherLastname+'&nbsp;'+item.motherLastname;
            }
        }

        function formatState (state) {
            var initAgendCode=formatRegisterCodePhoto(state.agendCode, 10);
            if (!state.id) { return state.text; }
            var $state = $(
                '<div class="user-panel">'+
                '<div class="pull-left image">'+
                '<img class="img-circle img-responsive" style="width:50px; height:50px;border-radius:50%;" src="http://www.utepsa.edu/miutepsa/imagenes/faces/'+initAgendCode+'/'+state.agendCode+'.jpg" alt="User Avatar">'+
                '</div>'+
                '<div style="color: black" class="pull-left info">'+
                '<p>'+state.registerNumber+'&nbsp;&nbsp;&nbsp;'+state.career.name+'</p>'+
                '<p>'+state.name+'&nbsp;'+state.fatherLastname+'&nbsp;'+state.motherLastname+'</p>'+
                '</div>'+
                '</div>'
            );
            return $state;
        }

        $scope.typeSearchSelected="";
        var formatRegisterCode = function(accountOrRegisterCode, tamano) {
            if( $scope.typeSearchSelected=='registerNumber'){
                return new Array(tamano + 1 - (accountOrRegisterCode + '').length).join('0') + accountOrRegisterCode;
            }else{
                return accountOrRegisterCode;
            }
        };

        var formatRegisterCodePhoto = function(accountOrRegisterCode, tamano) {
            var num ='0';
            var register="";
            try{
                for(var i=0;i<tamano ; i++){
                    if(accountOrRegisterCode.charAt(i) > num){
                        if(accountOrRegisterCode.substring(i).length == 2){ //si el registro contiene 2
                            register= accountOrRegisterCode.substring(i-2,tamano-2);
                            break;
                        }
                        if(accountOrRegisterCode.substring(i).length == 3){ //si el registro contiene 3
                            register= accountOrRegisterCode.substring(i-2,tamano-3);
                            break;
                        }
                        if(accountOrRegisterCode.substring(i).length == 4){ //si el registro contiene 3
                            register= accountOrRegisterCode.substring(i-1,tamano-3);
                            break;
                        }
                        if(accountOrRegisterCode.substring(i).length == 5){ //si el registro contiene 5
                            register= accountOrRegisterCode.substring(i,tamano-3);
                            break;
                        }
                        if(accountOrRegisterCode.substring(i).length == 6){ //si el registro contiene 5
                            register= accountOrRegisterCode.substring(i,tamano-4);
                            break;
                        }
                        if(accountOrRegisterCode.substring(i).length == 7){ //si el registro contiene 5
                            register= accountOrRegisterCode.substring(i,tamano-5);
                            break;
                        }
                        if(accountOrRegisterCode.substring(i).length == 8){ //si el registro contiene 5
                            register= accountOrRegisterCode.substring(i,tamano-6);
                            break;
                        }
                        if(accountOrRegisterCode.substring(i).length == 9){ //si el registro contiene 5
                            register= accountOrRegisterCode.substring(i,tamano-7);
                            break;
                        }
                        if(accountOrRegisterCode.substring(i).length == 10){ //si el registro contiene 5
                            register= accountOrRegisterCode.substring(i,tamano-8);
                            break;
                        }
                    }
                }
                return register;
            }catch (e){

            }
        };

        $scope.itemshistoryNotes=[];
        $scope.itemshistoryNotesPensum = [];
        $scope.documentsStudentItems = "";
        $scope.documentsStudent=[];
        $scope.itemsCoursesRegistered=[];
        $scope.coursesRegistered=[];
        $scope.coursesFailed=[];
        $scope.currentSemester=[];
        $scope.itemsCurrentSemester=[];
        $scope.searchStudent = [];
        $scope.showParameters = false;
        $scope.initRegisterStudent=[];
        $scope.photoRegisterStudent="";
        $scope.registerNumbers="";
        $scope.getStudentProfile = getStudentProfile;

        function getStudentProfile(registerCode) {
            $scope.tabsPanels = 'perfil';
            $scope.tabsPanelsHistoryNotes = 'cronologico';
            $scope.itemshistoryNotes=[];
            $scope.itemshistoryNotesPensum = [];
            $scope.documentsStudentItems = "";
            $scope.documentsStudent=[];
            $scope.itemsCoursesRegistered=[];
            $scope.coursesRegistered=[];
            $scope.coursesFailed=[];
            $scope.itemsCurrentSemester=[];
            $scope.currentSemester=[];
            $scope.messageAlert= "";
            $scope.alertError = false;
            $scope.spinnerSearchStudent = true;
            $scope.messageAlertHisotryNotes= "";
            $scope.alertErrorHisotryNotes = false;
            $scope.showParameters = false;

            var student = formatRegisterCode(registerCode,10);
            var promise = searchStudentService.getStudent(student);
            if (promise) {
                $scope.spinnerWidgetHistoryNotes = true;
                $scope.spinnerBoxDocumentStudent = true;
                $scope.spinnerWidgetFinancialState = true;
                $scope.alertErrorFinancialState = false;
                $scope.financialStateOfStudent = [];
                promise.then(function (result) {
                    $scope.searchStudent = result;
                    $scope.spinnerSearchStudent=false;
                    if ($scope.searchStudent.status == -1) {
                        $scope.showParameters = false;
                        $scope.messageAlertHisotryNotes = "  Problema de conexión";
                        $scope.alertErrorHisotryNotes = true;
                    }
                    if($scope.searchStudent.code == 200){
                        $scope.spinnerStudentProfile = false;
                        $scope.showParameters = true;
                        $scope.initRegisterStudent = formatRegisterCodePhoto($scope.searchStudent.data.agendCode, 10);
                        $scope.photoRegisterStudent=$scope.searchStudent.data.agendCode;
                        getHistoryNote($scope.searchStudent.data.id);
                        getHistoryNoteByPensum($scope.searchStudent.data.id);
                        getFinancialStates($scope.searchStudent.data.id);
                        getStudentsProfile($scope.searchStudent.data.id),
                        CurrentSemester();
                        CourseRegistered($scope.searchStudent.data.id);
                        getDocumentsStudent($scope.searchStudent.data.id);
                    }
                    if($scope.searchStudent.code == 404){
                        $scope.showParameters = false;
                        $scope.registerNumber="";
                        $scope.messageAlertHisotryNotes= " Estudiante no encontrado.";
                        $scope.alertErrorHisotryNotes = true;
                    }
                    if ($scope.searchStudent.code == status_code.INTERNAL_SERVER_ERROR) {
                        $scope.showParameters = false;
                        $scope.messageAlertHisotryNotes = "  Problema interno del servidor";
                        $scope.alertErrorHisotryNotes = true;
                    }
                    if ($scope.searchStudent.status == status_code.INTERNAL_SERVER_ERROR) {
                        $scope.showParameters = false;
                        $scope.messageAlertHisotryNotes = "  Problema interno del servidor";
                        $scope.alertErrorHisotryNotes = true;
                    }

                });
            }
        }

        var courseCronological="";
        var headerCronological="";
        $scope.semesterCronological=[];
        $scope.semesterOrderCronological=[];
        $scope.headerSemesterCronological=[];
        function getHistoryNote(idStudent){
            var promise = searchStudentService.getHistoryNotes(idStudent);
            if(promise){
                promise.then(function (result) {
                    $scope.spinnerWidgetHistoryNotes = false;
                    $scope.coursesFailed=[];
                    courseCronological=result.data.courses[0].semester;
                    headerCronological="";
                    $scope.semesterCronological=[];
                    $scope.semesterOrderCronological=[];
                    $scope.headerSemesterCronological=[];
                    $scope.finalOrderSemesterCronological=[];
                    $scope.itemshistoryNotes = result;
                    angular.forEach($scope.itemshistoryNotes.data.courses, function (item) {

                        if( headerCronological != item.semester){
                            $scope.headerSemesterCronological.push(item.semester);
                            headerCronological = item.semester;
                        }
                        if(courseCronological != item.semester){
                            $scope.semesterCronological.push(
                                $scope.semesterOrderCronological
                            );
                            courseCronological = item.semester;
                            $scope.semesterOrderCronological=[];
                        }
                        $scope.semesterOrderCronological.push({
                            "id": item.id,
                            "course": item.course,
                            "note": item.note,
                            "module": item.module,
                            "semester":item.semester
                        });
                    });
                    $scope.semesterCronological.push(
                        $scope.semesterOrderCronological
                    );

                    $scope.finalOrderSemesterCronological=Arr2object($scope.headerSemesterCronological, $scope.semesterCronological);
                    getCoursesReproved($scope.itemshistoryNotes);
                });
            }
        }

        function Arr2object(keys, vals) {
            return keys.reduce(
                function(prev, val, i) {
                    prev[val] = vals[i];
                    return prev;
                }, {}
            );
        }

        $scope.itemsCourseDetail="";
        $scope.courseDetail=[];

        $scope.getHistoryNotesByCourses= function (idStudent,idCourse) {
            $scope.spinnerHistoryNotesByCourse=true;
            var promise = searchStudentService.getHistoryNotesByCourse(idStudent,idCourse);
            if(promise){
                promise.then(function (result) {
                    $scope.courseDetail=[];
                    $scope.itemsCourseDetail = result;
                    $scope.spinnerHistoryNotesByCourse=false;
                    $scope.alertErrorHistoryNotesByCourse=false;
                    $scope.messageAlertHistoryNotesByCourse="";
                    if ($scope.itemsCourseDetail.status == -1) {
                        $scope.messageAlertHistoryNotesByCourse = "  Problema de conexión";
                        $scope.alertErrorHistoryNotesByCourse = true;
                    }
                    if($scope.itemsCourseDetail.code == 200){
                        angular.forEach($scope.itemsCourseDetail.data, function(item){
                            $scope.courseDetail.push(item);
                        });
                    }
                    if($scope.itemsCourseDetail.code == 404){
                        $scope.messageAlertHistoryNotesByCourse= " Materia no cursada.";
                        $scope.alertErrorHistoryNotesByCourse = true;
                    }
                    if ($scope.itemsCourseDetail.code == status_code.INTERNAL_SERVER_ERROR) {
                        $scope.messageAlertHistoryNotesByCourse = "  Problema interno del servidor";
                        $scope.alertErrorHistoryNotesByCourse = true;
                    }
                    if ($scope.itemsCourseDetail.status == status_code.INTERNAL_SERVER_ERROR) {
                        $scope.messageAlertHistoryNotesByCourse = "  Problema interno del servidor";
                        $scope.alertErrorHistoryNotesByCourse = true;
                    }
                });
            }
        };

        function getHistoryNoteByPensum(idStudent){
            var promise = searchStudentService.getHistoryNotesByPensum(idStudent);
            if(promise){
                promise.then(function (result) {
                    $scope.coursesFailed=[];
                    $scope.itemshistoryNotesPensum = result;

                    getCoursesReproved($scope.itemshistoryNotes);
                    PensumLevels($scope.itemshistoryNotesPensum);
                });
            }
        }

        function PensumLevels(result) {
            var pensum=1;
            $scope.semester=[];
            $scope.semesterOrder=[];
            $scope.totalSemester=[];
            angular.forEach(result.data.courses, function (item) {
                if(pensum != item.pensumLevel){
                    $scope.semester.push(
                        $scope.semesterOrder
                    );
                    pensum = item.pensumLevel;
                    $scope.semesterOrder=[];
                }
                $scope.semesterOrder.push({
                    "idCourse": item.idCourse,
                    "idStudent": item.idStudent,
                    "course": item.course,
                    "note": item.note
                });
            });
            $scope.semester.push(
                $scope.semesterOrder
            );
        }

        function getDocumentsStudent(idStudent){
            var promise = searchStudentService.getDocumentsStudents(idStudent);
            if(promise){
                promise.then(function (result) {
                    $scope.spinnerBoxDocumentStudent = false;
                    $scope.documentsStudentItems = "";
                    $scope.documentsStudent=[];
                    $scope.documentsStudentItems = result;
                    if($scope.documentsStudentItems.data === undefined){
                        return;
                    }
                    for(var i = 0; i < $scope.documentsStudentItems.data.length; i++){
                        $scope.documentsStudent.push($scope.documentsStudentItems.data[i]);
                    }
                });
            }
        }

        function getCoursesReproved(result) {
            try{
                if(result.data.disapproved>0) {
                    angular.forEach(result.data.courses, function(item){
                        if(item.note < item.minimunNote){
                            $scope.coursesFailed.push(item);
                        }
                    });
                }
            }catch (e){

            }
        }

        $scope.numberDocumentsDelivered = function(){
            var total = 0;
            angular.forEach($scope.documentsStudent, function(item){
                if(item.state==true){
                    total ++;
                }
            });

            return total;
        };

        $scope.numberDocuments = function(){
            var total = 0;
            angular.forEach($scope.documentsStudent, function(item){
                total ++;
            });
            return total;
        };

        $scope.percentageOfDocuments = function(){
            var total = parseFloat(100/$scope.numberDocuments()*$scope.numberDocumentsDelivered());
            return total.toFixed(2);
        };

        function CourseRegistered(idStudent){
            var promise = searchStudentService.getCoursesRegistered(idStudent);
            if(promise){
                promise.then(function (result) {
                    $scope.itemsCoursesRegistered = result;
                    if ($scope.itemsCoursesRegistered.status == -1) {
                        $scope.messageAlert = "  Problema de conexión";
                        $scope.alertError = true;
                    }
                    if ($scope.itemsCoursesRegistered.code == status_code.OK) {
                        $scope.coursesRegistered =$scope.itemsCoursesRegistered;
                    }
                    if ($scope.itemsCoursesRegistered.code == status_code.NO_CONTENT) {
                        $scope.messageAlert = "  El estudiante no tiene materias programadas para este semestre";
                        $scope.alertError = true;
                    }
                    if ($scope.itemsCoursesRegistered.code == status_code.INTERNAL_SERVER_ERROR) {
                        $scope.messageAlert = "  Problema interno del servidor";
                        $scope.alertError = true;
                    }
                    if ($scope.itemsCoursesRegistered.status == status_code.INTERNAL_SERVER_ERROR) {
                        $scope.messageAlert = "  Problema interno del servidor";
                        $scope.alertError = true;
                    }
                });
            }
        }

        function CurrentSemester(){
            var promise = searchStudentService.getCurrentSemester();
            if(promise){
                promise.then(function (result) {
                    $scope.itemsCurrentSemester = result;
                    $scope.currentSemester =$scope.itemsCurrentSemester.data;
                });
            }
        }

        $scope.tabsPanels = 'perfil';
        $scope.changedTabs = function (tab) {
            $scope.tabsPanels = tab;
        };

        $scope.tabsPanelsHistoryNotes = 'cronologico';
        $scope.changedTabsHistoryNotes =function (tab) {
            $scope.tabsPanelsHistoryNotes = tab;
        };

        $scope.resultFinancialState = [];
        $scope.financialStateOfStudent = [];
        function getFinancialStates(idStudent){
            $scope.spinnerWidgetFinancialState = false;
            var promise = searchStudentService.getFinancialState(idStudent);
            if(promise){
                promise.then(function (result) {
                    $scope.resultFinancialState = result;
                    if($scope.resultFinancialState.code == status_code.OK){
                        $scope.financialStateOfStudent = $scope.resultFinancialState;
                    }
                    if($scope.resultFinancialState.code == status_code.NOT_FOUND){
                        $scope.alertErrorFinancialState = true;
                    }
                })
            }
        }

        $scope.studentProfile = [];
        $scope.agendCodeWithoutZero="";
        function getStudentsProfile(idStudent) {
            var promise = searchStudentService.getStudentProfile(idStudent);
            if(promise){
                promise.then(function (result) {
                    $scope.studentProfile = result;
                    $scope.agendCodeWithoutZero = splitAgendCode(result.data.agendCode, 10);
                })
            }
        }

        function splitAgendCode (result,tamano){
            var newRegister="";
            for(var i=0;i<tamano; i++) {
                if (result.charAt(i) > 0) {
                    newRegister= result.substring(i,tamano);
                    break;
                }
            }
            return  newRegister;
        }
    }
})();