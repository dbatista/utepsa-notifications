/**
 * Created by Front-End on 27/03/2017.
 */
/**
 * Created by CRISTIAN on 07/03/2017.
 */
;(function() {

    angular.module("utepsa-administrator").controller('permissionController', permissionController);

    permissionController.$inject = ['$scope','permissionService'];

    function permissionController($scope,permissionService) {

        getPermission();
        $scope.spinnerPermissions = true;

        function getPermission(){
            var promise = permissionService.getPermisssions();
            if(promise){
                promise.then(function (result) {
                    $scope.spinnerPermissions = false;
                    $scope.itemsPermission = result;
                    angular.forEach($scope.itemsPermission.data, function(item){
                    
                        if(item.permission.tag == 'CREATE_NOTIFICATION'){
                            $scope.createNotification=item.state;
                        }
                        if(item.permission.tag == 'CREATE_USER'){
                            $scope.createUser=item.state;
                        }
                        if(item.permission.tag == 'UPDATE_USER'){
                            $scope.updateUser=item.state;
                        }
                        if(item.permission.tag== 'UPDATE_NOTIFICATION'){
                            $scope.updateNotification=item.state;
                        }
                        if(item.permission.tag == 'DELETE_USER'){
                            $scope.deleteUser=item.state;
                        }
                        if(item.permission.tag == 'DELETE_NOTIFICATION'){
                            $scope.deleteNotification=item.state;
                        }
                        if(item.permission.tag == 'QUERY_NOTIFICATION'){
                            $scope.queryNotification=item.state;
                        }
                        if(item.permission.tag == 'QUERY_DASHBOARD'){
                            $scope.queryDashboard=item.state;
                        }
                        if(item.permission.tag == 'QUERY_REPORT_MANAGMENT'){
                            $scope.queryReportManagment=item.state;
                        }
                        if(item.permission.tag == 'QUERY_USER'){
                            $scope.queryUser=item.state;

                        }
                        if(item.permission.tag == 'QUERY_STUDENT'){
                            $scope.queryStudent=item.state;
                        }
                        if(item.permission.tag == 'QUERY_PERMISSIONS_USER'){
                            $scope.queryPermissionsUser=item.state;
                        }
                        if(item.permission.tag == 'UPDATE_PERMISSION_USER'){
                            $scope.updatePermissionsUser=item.state;
                        }

                    });
                });
            }
        }
    }
})();