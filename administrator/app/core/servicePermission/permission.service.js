/**
 * Created by Android on 3/14/2017.
 */
;(function() {

    angular.module("utepsa-administrator").factory("permissionService", permissionService);

    permissionService.$inject = ['$http','API','$localStorage'];

    function permissionService($http, API, $localStorage){

        function getPermisssions(){
            return $http({
                method: 'GET',
                url: API.url+'administrator/credentials/'+$localStorage.currentAdministrator.idCredential+'/permissions'
            }).then(function(result) {
                return result.data;
            }).catch(function(error) {
                return error;
            });
        }

        var service = {
            getPermisssions: getPermisssions
        };

        return service;
    }

})();