(function () {
    'use strict';
 
    angular
        .module('utepsa-administrator')
        .factory('AuthenticationService', AuthenticationService);
    
    AuthenticationService.$inject = ['$state', '$http', '$localStorage', 'API'];

    function AuthenticationService($state, $http, $localStorage, API) {
        var service = {};
 
        service.Login = Login;
        service.Logout = Logout;
 
        return service;
 
        function Login(idStudent, password, callback) {
            $http({
              method: 'POST',
              url: API.url + 'administrator/credentials/auth',
              withCredentials: true,
              headers: { username: idStudent, password: password }
            }).then(function(result) {
                callback(result);
            })
            .catch(function(error) {
                callback(error);
            });
        }

        function Logout() {
            // remove user from local storage and clear http auth header
            delete $localStorage.currentAdministrator;
            $state.transitionTo("login");
            event.preventDefault();
            //$http.defaults.headers.common.Authorization = '';
        }
    }
})();