;(function() {

  	angular.module("utepsa-administrator").controller('LoginController', LoginController);

  	LoginController.$inject = ['$scope', '$state', 'resizeTemplate', 'AuthenticationService', '$localStorage','status_code'];

  	function LoginController($scope, $state, resizeTemplate, AuthenticationService, $localStorage,status_code) {
    	resizeTemplate.resize();
    	$scope.userAgentAdministrator="";
    	$scope.messageAlert="";
        $scope.username="";
        $scope.password="";
        $scope.login=login;
        $scope.logout = logout;

        function logout(){
            AuthenticationService.Logout();
        }

        function login() {
            $scope.spinnerLogin=true;
            $scope.alertError = false;
            AuthenticationService.Login($scope.username.toUpperCase(), $scope.password, function (result) {
                try {
                    $scope.userAgentAdministrator = result;
                    $scope.spinnerLogin=false;
                    if ($scope.userAgentAdministrator.status == -1) {
                        $scope.messageAlert = "  Problema de conexión";
                        $scope.alertError = true;
                        cleanInputs();
                    }
                    if ($scope.userAgentAdministrator.data.code == status_code.OK) {
                        $localStorage.currentAdministrator = {
                            administratorKey: $scope.userAgentAdministrator.data.data.profile.id,
                            idCredential: $scope.userAgentAdministrator.data.data.id
                        };
                        $state.go('searchStudents');
                    }
                    if ($scope.userAgentAdministrator.data.code == status_code.FORBIDDEN) {
                        $localStorage.tempUser = {idCredential: $scope.userAgentAdministrator.data.data.id};
                        $localStorage.currentAdministrator = {
                            administratorKey: $scope.userAgentAdministrator.data.data.profile.id,
                            idCredential: $scope.userAgentAdministrator.data.data.id
                        };
                        $state.go('resetPassword');
                    }
                    if ($scope.userAgentAdministrator.data.code == status_code.NOT_FOUND) {
                        $scope.messageAlert = "  Usuario y/o contraseña incorrectos";
                        $scope.alertError = true;
                        cleanInputs();
                    }
                    if ($scope.userAgentAdministrator.data.code == status_code.INTERNAL_SERVER_ERROR) {
                        $scope.messageAlert = "  Problema interno del servidor";
                        $scope.alertError = true;
                        cleanInputs();
                    }
                    if ($scope.userAgentAdministrator.status == status_code.INTERNAL_SERVER_ERROR) {
                        $scope.messageAlert = "  Problema interno del servidor";
                        $scope.alertError = true;
                        cleanInputs();
                    }
                }catch (e){

                }
            });
        };

        function cleanInputs() {
            try{
                $scope.password="";
            }catch(e){

            }
        }
  	}
})();