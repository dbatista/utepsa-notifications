/**
 * Created by Android on 2/6/2017.
 */
;(function() {

    angular.module("utepsa-administrator").controller('DashboardController', DashboardController);

    DashboardController.$inject = ['$scope', 'resizeTemplate', 'dashboardService', 'status_code','notificationsService'];

    function DashboardController($scope, resizeTemplate, dashboardService, status_code,notificationsService) {
        resizeTemplate.resize();
        $scope.alertError=false;
        $scope.messageAlert="";
        $scope.spinnerDashboard=true;
        $scope.itemsExecuterReport="";
        $scope.executeReport="";
        $scope.alertErrorStudent = false;
        $scope.messageAlerStudent = "";
        $scope.alertErrorStudentActive = false;
        $scope.messageAlerStudentActive = "";


        getExecuteReport();
        function getExecuteReport() {
            $scope.alertError= false;
            $scope.alertErrorStudent = false;
            $scope.alertErrorStudentActive = false;
            var promise = dashboardService.getExecuteReport();
            if (promise) {
                promise.then(function (result) {
                    $scope.itemsExecuterReport=result;
                    $scope.spinnerDashboard=false;
                    if ($scope.itemsExecuterReport.status == -1) {
                        $scope.messageAlert = "  Problema de conexión";
                        $scope.alertError = true;
                    }
                    if($scope.itemsExecuterReport.code == 200){
                        $scope.executeReport= $scope.itemsExecuterReport.data;
                        averageByCareer($scope.executeReport);
                    }
                    if($scope.itemsExecuterReport.code == 409){
                        $scope.messageAlert= " No se pudo traer los datos.";
                        $scope.alertError = true;
                    }
                    if ($scope.itemsExecuterReport.code == status_code.INTERNAL_SERVER_ERROR) {
                        $scope.messageAlert = "  Problema interno del servidor";
                        $scope.alertError = true;
                    }
                    if ($scope.itemsExecuterReport.status == status_code.INTERNAL_SERVER_ERROR) {
                        $scope.messageAlert = "  Problema interno del servidor";
                        $scope.alertError = true;
                    }
                });
            }
        }

        $scope.careersByGrade = [];
        $scope.careersByGradeItems=null;
        getCareersByGrade();
        function getCareersByGrade() {
            var promise = notificationsService.getCarrers();
            if (promise) {
                promise.then(function (result) {
                    $scope.careersByGradeItems = result;
                    if($scope.careersByGradeItems === undefined){
                        return;
                    }
                    for(var i = 0; i < $scope.careersByGradeItems.data.length; i++){
                        $scope.careersByGrade.push($scope.careersByGradeItems.data[i]);
                    }
                });
            }
        }

        $scope.selectedCareer="";
        $scope.showReportStudentByModule=false;
        $scope.itemsExecuterReportByCareer="";
        $scope.ExecuteReportByCareer="";
        $scope.spinnerReportStudentByModule=false;
        $scope.loadResource = true;

        $scope.studentByModuleSerie=[];
        $scope.stundentByModuleName=[];
        $scope.studentByModuleData=[];

        $scope.studentsByModule= function (result) {
            if(result !== ""){
                $scope.loadResource = true;
                $scope.studentByModuleSerie = ['Inscritos'];
                $scope.spinnerReportStudentByModule=true;
                var promise = dashboardService.getExecuteReportByCareer(result);
                if(promise){
                    promise.then(function (result) {
                        console.log(result);
                        $scope.itemsExecuterReportByCareer=result;
                        $scope.spinnerReportStudentByModule=false;
                        if($scope.itemsExecuterReportByCareer.code === 200){
                            $scope.loadResource = false;
                            StudentPerCareer(result);
                            StudentActive(result);
                            StudentCareerManAndStudentCareerWoman(result);
                            StudentActiveManAndStudentActiveWoman(result);
                            StudentRegisterPerModule(result);
                            $scope.showReportStudentByModule=true;
                        }
                        if($scope.itemsExecuterReportByCareer.code == 409){
                            $scope.messageAlert= " No se pudo traer los datos.";
                            $scope.alertError = true;
                        }
                        if ($scope.itemsExecuterReportByCareer.code == status_code.INTERNAL_SERVER_ERROR) {
                            $scope.messageAlert = "  Problema interno del servidor";
                            $scope.alertError = true;
                        }
                        if ($scope.itemsExecuterReportByCareer.status == status_code.INTERNAL_SERVER_ERROR) {
                            $scope.messageAlert = "  Problema interno del servidor";
                            $scope.alertError = true;
                        }
                    })
                }
            }
        };

        $scope.studentsPerCareer = "";
        function StudentPerCareer(result) {
            $scope.studentsPerCareer = "";
            $scope.studentsPerCareer = result.data.studentCareer;
        }

        $scope.studentActive = "";
        function StudentActive(result) {
            $scope.studentActive = "";
            $scope.studentActive = result.data.studentActive;
        }

        $scope.studentCareerMan = "";
        $scope.studentCareerWoman = "";
        $scope.labelsStudents = [];
        function StudentCareerManAndStudentCareerWoman(result) {
            $scope.alertErrorStudent = false;
            if($scope.studentsPerCareer > 0){
                $scope.studentCareerMan = "";
                $scope.studentCareerWoman = "";
                $scope.studentCareerMan = result.data.studentCareerMan;
                $scope.studentCareerWoman = result.data.studentCareerWoman;
                $scope.labelsStudents = ["Estudiantes registrados hombres","Estudiantes registrados mujeres"];
                $scope.dataStudents = [$scope.studentCareerMan, $scope.studentCareerWoman];
                $scope.type = 'pie';
            }
            else{
                $scope.alertErrorStudent = true;
                $scope.messageAlerStudent = "No hay estudiantes inscritos. Por favor elija otra carrera";
            }
        }

        $scope.studentActiveMan = "";
        $scope.studentActiveWoman = "";
        function StudentActiveManAndStudentActiveWoman(result) {
            $scope.alertErrorStudentActive = false;
            if($scope.studentActive > 0){
                $scope.studentActiveMan = "";
                $scope.studentActiveWoman = "";
                $scope.studentActiveMan = result.data.studentActiveMan;
                $scope.studentActiveWoman = result.data.studentActiveWoman;
                $scope.labelsActiveStudents = ["Estudiantes ativos: hombres","Estudiantes ativos: mujeres"];
                $scope.dataActiveStudents = [$scope.studentActiveMan, $scope.studentActiveWoman];
                $scope.type = 'pie';
            }
            else{
                $scope.alertErrorStudentActive = true;
                $scope.messageAlerStudentActive = "No hay estudiantes activos. Por favor elija otra carrera";
            }
        }

        $scope.labelsModule = [];
        $scope.dataStudentRegisterPerModule = [];
        function StudentRegisterPerModule(result) {
            $scope.labelsModule = [];
            angular.forEach(result.data.module, function (item) {
                $scope.labelsModule.push('Modulo '+item.module);
                $scope.dataStudentRegisterPerModule.push(item.studentRegister);
            });
        }

        $scope.averageCarrerSerie=[];
        $scope.averageCareerName=[];
        $scope.averageData=[];
        function averageByCareer(result) {
            $scope.averageCarrerSerie = ['Promedio'];
            angular.forEach(result.dataCareers, function (item) {
                if(item.average>0){
                    $scope.averageCareerName.push(item.nameCareer);
                    $scope.averageData.push(item.average);
                }
            });
        }
    }

})();