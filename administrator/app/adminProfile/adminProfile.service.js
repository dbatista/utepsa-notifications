/**
 * Created by CRISTIAN on 07/03/2017.
 */
;(function() {

    angular.module("utepsa-administrator").factory("adminProfileService", adminProfileService);

    adminProfileService.$inject = ['$http','API','$localStorage'];

    function adminProfileService($http, API, $localStorage){

        function getAdminProfile(){
            return $http({
                method: 'GET',
                url: API.url+'administrator/credentials/'+$localStorage.currentAdministrator.idCredential,
                headers: {
                    'Content-Type': 'application/json; charset=utf-8'
                }
            }).then(function(result) {
                return result.data;
            }).catch(function(error) {
                return error;
            });
        }

        var service = {
            getAdminProfile: getAdminProfile
        };
        return service;
    }

})();
