;(function() {

    angular.module("utepsa-administrator").factory("userManagementService", userManagementService);

    userManagementService.$inject = ['$http','API'];

    function userManagementService($http, API){

        function getUsersManagement(){
            return $http({
                method: 'GET',
                url: API.url+'administrator/credentials',
            }).then(function(result) {
                return result.data;
            }).catch(function(error) {
                return error;
            });
        }

        function putUserManagemet(dataUsers, callback) {
            return $http({
                method: 'PUT',
                url: API.url+'administrator/credentials/'+dataUsers.id+'/profile',
                data:{
                    "id": dataUsers.profile.id,
                    "name": dataUsers.profile.name,
                    "fatherLastname": dataUsers.profile.fatherLastname,
                    "motherLastname": dataUsers.profile.motherLastname,
                    "ocupation": dataUsers.profile.ocupation,
                    "phoneNumber": dataUsers.profile.phoneNumber,
                    "email": dataUsers.profile.email
                }
            }).then(function(result) {
                callback(result);
            }).catch(function(error) {
                callback(error);
            });
        }

        function deleteUserMangement(idCredential, callback) {
            return $http({
                method: 'DELETE',
                url: API.url+'administrator/credentials/'+idCredential
            }).then(function(result) {
                callback(result);
            }).catch(function(error) {
                callback(error);
            });
        }

        function postUserMangement(dataUser, roleSelected,changePasswordForced, userName, callback) {
            return $http({
                method: 'POST',
                url: API.url+'administrator/credentials',
                data: {
                    "username": userName,
                    "password": API.constaPassword,
                    "token": " ",
                    "gcmId": " ",
                    "lastConnection": " ",
                    "state": true,
                    "role": {
                        "id": roleSelected
                    },
                    "profile": {
                        "name": dataUser.profile.name,
                        "fatherLastname": dataUser.profile.fatherLastname,
                        "motherLastname": dataUser.profile.motherLastname,
                        "ocupation": dataUser.profile.ocupation,
                        "phoneNumber": dataUser.profile.phoneNumber,
                        "email": dataUser.profile.email
                    },
                    "changePasswordForced":changePasswordForced
                }
            }).then(function(result) {
                callback(result);
            }).catch(function(error) {
                callback(error);
            });
        }

        function permissionsUserManagement(idPermissionsCredential) {
            return $http({
                method:'GET',
                url: API.url+'administrator/credentials/'+idPermissionsCredential+'/permissions'
            }).then(function(result) {
                return result.data;
            }).catch(function(error) {
                return error;
            });
        }

        function putModifyPermissions(permissionsResult){
            return $http({
                method: 'PUT',
                url: API.url+'administrator/credentials',
                data: angular.toJson(permissionsResult),
                headers: {
                    'Content-Type': 'application/json; charset=utf-8'
                }
            }).then(function(result) {
                return result;
            }).catch(function(error) {
                return error;
            });
        }

        var service = {
            getUsersManagement: getUsersManagement,
            putUserManagemet: putUserManagemet,
            deleteUserMangement: deleteUserMangement,
            postUserMangement: postUserMangement,
            permissionsUserManagement: permissionsUserManagement,
            putModifyPermissions: putModifyPermissions
        };

        return service;
    }

})();