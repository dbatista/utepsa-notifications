package com.utepsa.api.serializers;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.utepsa.models.Student;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by David on 25/01/2017.
 */
public class StudentSerializer extends JsonSerializer<Student>{
    @Override
    public void serialize(Student student, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException, JsonProcessingException {
        jsonGenerator.writeStartObject();

        jsonGenerator.writeNumberField("id", student.getId());
        jsonGenerator.writeStringField("agendCode", student.getAgendCode());
        jsonGenerator.writeStringField("registerNumber", student.getRegisterNumber());
        jsonGenerator.writeObjectField("career", student.getCareer());
        jsonGenerator.writeNumberField("pensum", student.getPensum());
        jsonGenerator.writeStringField("name", student.getName());
        jsonGenerator.writeStringField("fatherLastname", student.getFatherLastname());
        jsonGenerator.writeStringField("motherLastname", student.getMotherLastname());
        Date birthday = null;
        if(student.getBirthday() != null){
            SimpleDateFormat formaterShortDate = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
            String birthdayFormat = formaterShortDate.format(student.getBirthday());
            try {
                birthday = formaterShortDate.parse(birthdayFormat);
                jsonGenerator.writeNumberField("birthday", birthday.getTime()/1000);
            } catch (ParseException e) {
                e.printStackTrace();
                jsonGenerator.writeNumberField("birthday", null);
            }
        }else{
            jsonGenerator.writeNumberField("birthday", null);
        }

        jsonGenerator.writeStringField("documentType", student.getDocumentType());
        jsonGenerator.writeStringField("documentNumber", student.getDocumentNumber());
        jsonGenerator.writeStringField("gender", student.getGender());
        jsonGenerator.writeStringField("phoneNumber1", student.getPhoneNumber1());
        jsonGenerator.writeStringField("phoneNumber2", student.getPhoneNumber2());
        jsonGenerator.writeStringField("email1", student.getEmail1());
        jsonGenerator.writeStringField("email2", student.getEmail2());
        jsonGenerator.writeStringField("firstSemester", student.getFirstSemester());
        jsonGenerator.writeEndObject();
    }
}
