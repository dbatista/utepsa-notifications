package com.utepsa.api.custom;

/**
 * Created by Luana Chavez on 24/04/2017.
 */
public class ModulesReport {
    private String module;
    private Long studentRegister;

    public ModulesReport() {
    }

    public String getModule() {
        return module;
    }

    public void setModule(String module) {
        this.module = module;
    }

    public Long getStudentRegister() {
        return studentRegister;
    }

    public void setStudentRegister(Long studentRegister) {
        this.studentRegister = studentRegister;
    }

}
