package com.utepsa.api.custom;

/**
 * Created by Luana Chavez on 12/05/2017.
 */
public class StudentCourseRegisterCustom {
    private long idStudent;
    private long idCareer;
    private String gender;
    private String semester;
    private String module;

    public StudentCourseRegisterCustom() {
    }

    public long getIdStudent() {
        return idStudent;
    }

    public void setIdStudent(long idStudent) {
        this.idStudent = idStudent;
    }

    public long getIdCareer() {
        return idCareer;
    }

    public void setIdCareer(long idCareer) {
        this.idCareer = idCareer;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getSemester() {
        return semester;
    }

    public void setSemester(String semester) {
        this.semester = semester;
    }

    public String getModule() {
        return module;
    }

    public void setModule(String module) {
        this.module = module;
    }
}
