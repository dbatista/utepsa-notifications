package com.utepsa.api.custom;

import java.util.List;

/**
 * Created by Luana Chavez on 18/04/2017.
 */
public class ExecutiveReport {

    private String semester;
    private long total;
    private long totalActive;
    private String average;
    private List<DataCareer> dataCareers;

    public ExecutiveReport() {
    }

    public String getSemester() {
        return semester;
    }

    public void setSemester(String semester) {
        this.semester = semester;
    }

    public long getTotal() {
        return total;
    }

    public void setTotal(long total) {
        this.total = total;
    }

    public long getTotalActive() {
        return totalActive;
    }

    public void setTotalActive(long totalActive) {
        this.totalActive = totalActive;
    }

    public String getAverage() {
        return average;
    }

    public void setAverage(String average) {
        this.average = average;
    }

    public List<DataCareer> getDataCareers() {
        return dataCareers;
    }

    public void setDataCareers(List<DataCareer> dataCareers) {
        this.dataCareers = dataCareers;
    }
}
