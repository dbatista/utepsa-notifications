package com.utepsa.api.views;

import org.hibernate.annotations.Subselect;

import javax.persistence.*;

/**
 * Created by David on 22/01/2017.
 */
@Entity
@Subselect("select * from view_file")
public class ViewFile {
    private Long id;
    private String name;
    private String description;
    private String url;
    private Long dateUpload;
    private String type;
    private Boolean state;

    @Id
    @GeneratedValue
    @Column(name = "id", updatable = false, nullable = false)
    public Long getId() {
        return id;
    }

    @Column(name = "name", nullable = true, length = 255)
    public String getName() {
        return name;
    }

    @Column(name = "description", nullable = true, length = 255)
    public String getDescription() {
        return description;
    }

    @Column(name = "url", nullable = true, length = 255)
    public String getUrl() {
        return url;
    }

    @Column(name = "date_upload", nullable = true)
    public Long getDateUpload() {
        return dateUpload;
    }

    @Column(name = "type", nullable = true, length = 255)
    public String getType() {
        return type;
    }

    @Column(name = "state", nullable = true)
    public Boolean getState() {
        return state;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ViewFile viewFile = (ViewFile) o;

        if (id != null ? !id.equals(viewFile.id) : viewFile.id != null) return false;
        if (name != null ? !name.equals(viewFile.name) : viewFile.name != null) return false;
        if (description != null ? !description.equals(viewFile.description) : viewFile.description != null)
            return false;
        if (url != null ? !url.equals(viewFile.url) : viewFile.url != null) return false;
        if (dateUpload != null ? !dateUpload.equals(viewFile.dateUpload) : viewFile.dateUpload != null) return false;
        if (type != null ? !type.equals(viewFile.type) : viewFile.type != null) return false;
        if (state != null ? !state.equals(viewFile.state) : viewFile.state != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (url != null ? url.hashCode() : 0);
        result = 31 * result + (dateUpload != null ? dateUpload.hashCode() : 0);
        result = 31 * result + (type != null ? type.hashCode() : 0);
        result = 31 * result + (state != null ? state.hashCode() : 0);
        return result;
    }
}
