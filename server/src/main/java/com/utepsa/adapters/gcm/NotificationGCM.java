package com.utepsa.adapters.gcm;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Set;

/**
 * Created by roberto on 25/8/2016.
 */
public class NotificationGCM {

    private Data notification;
    private Set<String> registration_ids;

    public NotificationGCM(){ }

    public NotificationGCM(Data notification, Set<String> registration_ids) {
        this.notification = notification;
        this.registration_ids = registration_ids;
    }

    @JsonProperty
    public Data getNotification() {
        return notification;
    }

    @JsonProperty
    public void setNotification(Data notification) {
        this.notification = notification;
    }

    @JsonProperty
    public Set<String> getRegistration_ids() {
        return registration_ids;
    }

    @JsonProperty
    public void setRegistration_ids(Set<String> registration_ids) {
        this.registration_ids = registration_ids;
    }
}
