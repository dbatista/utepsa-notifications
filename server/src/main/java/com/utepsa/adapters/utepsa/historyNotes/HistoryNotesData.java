package com.utepsa.adapters.utepsa.historyNotes;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by David on 11/10/2016.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class HistoryNotesData {

    private String alm_registro;
    private String crr_codigo;
    private String crr_descripcion;
    private String pns_codigo;
    private String agd_codigo;
    private String mat_codigo;
    private String mat_descripcion;
    private String sem_codigo;
    private String mdu_codigo;
    private String tipo_planilla;
    private long not_nota;
    private long pln_notaminima;
    private boolean pns_electiva;

    public HistoryNotesData() {}

    public HistoryNotesData(String alm_registro, String crr_codigo, String crr_descripcion, String pns_codigo, String agd_codigo, String mat_codigo, String mat_descripcion, String sem_codigo, String mdu_codigo, String tipo_planilla, long not_nota, long pln_notaminima, boolean pns_electiva) {
        this.alm_registro = alm_registro;
        this.crr_codigo = crr_codigo;
        this.crr_descripcion = crr_descripcion;
        this.pns_codigo = pns_codigo;
        this.agd_codigo = agd_codigo;
        this.mat_codigo = mat_codigo;
        this.mat_descripcion = mat_descripcion;
        this.sem_codigo = sem_codigo;
        this.mdu_codigo = mdu_codigo;
        this.tipo_planilla = tipo_planilla;
        this.not_nota = not_nota;
        this.pln_notaminima = pln_notaminima;
        this.pns_electiva = pns_electiva;
    }
    @JsonProperty
    public String getAlm_registro() {
        return alm_registro;
    }

    @JsonProperty
    public void setAlm_registro(String alm_registro) {
        this.alm_registro = alm_registro;
    }

    @JsonProperty
    public String getCrr_codigo() {
        return crr_codigo;
    }

    @JsonProperty
    public void setCrr_codigo(String crr_codigo) {
        this.crr_codigo = crr_codigo;
    }

    @JsonProperty
    public String getCrr_descripcion() {
        return crr_descripcion;
    }

    @JsonProperty
    public void setCrr_descripcion(String crr_descripcion) {
        this.crr_descripcion = crr_descripcion;
    }

    @JsonProperty
    public String getPns_codigo() {
        return pns_codigo;
    }

    @JsonProperty
    public void setPns_codigo(String pns_codigo) {
        this.pns_codigo = pns_codigo;
    }

    @JsonProperty
    public String getAgd_codigo() {
        return agd_codigo;
    }

    @JsonProperty
    public void setAgd_codigo(String agd_codigo) {
        this.agd_codigo = agd_codigo;
    }

    @JsonProperty
    public String getMat_codigo() {
        return mat_codigo;
    }

    @JsonProperty
    public void setMat_codigo(String mat_codigo) {
        this.mat_codigo = mat_codigo;
    }

    @JsonProperty
    public String getMat_descripcion() {
        return mat_descripcion;
    }

    @JsonProperty
    public void setMat_descripcion(String mat_descripcion) {
        this.mat_descripcion = mat_descripcion;
    }

    @JsonProperty
    public String getSem_codigo() {
        return sem_codigo;
    }

    @JsonProperty
    public void setSem_codigo(String sem_codigo) {
        this.sem_codigo = sem_codigo;
    }

    @JsonProperty
    public String getMdu_codigo() {
        return mdu_codigo;
    }

    @JsonProperty
    public void setMdu_codigo(String mdu_codigo) {
        this.mdu_codigo = mdu_codigo;
    }

    @JsonProperty
    public String getTipo_planilla() {
        return tipo_planilla;
    }

    @JsonProperty
    public void setTipo_planilla(String tipo_planilla) {
        this.tipo_planilla = tipo_planilla;
    }

    @JsonProperty
    public long getNot_nota() {
        return not_nota;
    }

    @JsonProperty
    public void setNot_nota(long not_nota) {
        this.not_nota = not_nota;
    }

    @JsonProperty
    public long getPln_notaminima() {
        return pln_notaminima;
    }

    @JsonProperty
    public void setPln_notaminima(long pln_notaminima) {
        this.pln_notaminima = pln_notaminima;
    }

    @JsonProperty
    public boolean isPns_electiva() {
        return pns_electiva;
    }

    @JsonProperty
    public void setPns_electiva(boolean pns_electiva) {
        this.pns_electiva = pns_electiva;
    }
}
