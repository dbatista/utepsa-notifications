package com.utepsa.adapters.utepsa.documentsStudent;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by david on 27/10/16.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class DocumentsStudentData {
    private String alm_registro;
    private String doc_codigo;
    private String almdoc_fechaentregar;
    private String almdoc_fecharecepcion;
    private String almdoc_tipopapel;
    private String doc_descripcion;
    private String ESTADO;

    public DocumentsStudentData() {
    }

    public DocumentsStudentData(String alm_registro, String doc_codigo, String almdoc_fechaentregar, String almdoc_fecharecepcion, String almdoc_tipopapel, String doc_descripcion, String ESTADO) {
        this.alm_registro = alm_registro;
        this.doc_codigo = doc_codigo;
        this.almdoc_fechaentregar = almdoc_fechaentregar;
        this.almdoc_fecharecepcion = almdoc_fecharecepcion;
        this.almdoc_tipopapel = almdoc_tipopapel;
        this.doc_descripcion = doc_descripcion;
        this.ESTADO = ESTADO;
    }

    public String getAlm_registro() {
        return alm_registro;
    }
    @JsonProperty
    public void setAlm_registro(String alm_registro) {
        this.alm_registro = alm_registro;
    }
    @JsonProperty
    public String getDoc_codigo() {
        return doc_codigo;
    }
    @JsonProperty
    public void setDoc_codigo(String doc_codigo) {
        this.doc_codigo = doc_codigo;
    }
    @JsonProperty
    public String getAlmdoc_fechaentregar() {
        return almdoc_fechaentregar;
    }
    @JsonProperty
    public void setAlmdoc_fechaentregar(String almdoc_fechaentregar) {
        this.almdoc_fechaentregar = almdoc_fechaentregar;
    }
    @JsonProperty
    public String getAlmdoc_fecharecepcion() {
        return almdoc_fecharecepcion;
    }
    @JsonProperty
    public void setAlmdoc_fecharecepcion(String almdoc_fecharecepcion) {
        this.almdoc_fecharecepcion = almdoc_fecharecepcion;
    }
    @JsonProperty
    public String getAlmdoc_tipopapel() {
        return almdoc_tipopapel;
    }
    @JsonProperty
    public void setAlmdoc_tipopapel(String almdoc_tipopapel) {
        this.almdoc_tipopapel = almdoc_tipopapel;
    }
    @JsonProperty
    public String getDoc_descripcion() {
        return doc_descripcion;
    }
    @JsonProperty
    public void setDoc_descripcion(String doc_descripcion) {
        this.doc_descripcion = doc_descripcion;
    }
    @JsonProperty("ESTADO")
    public String getESTADO() {
        return ESTADO;
    }
    @JsonProperty
    public void setESTADO(String ESTADO) {
        this.ESTADO = ESTADO;
    }
}
