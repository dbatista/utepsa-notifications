package com.utepsa.adapters.utepsa.financialState;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.utepsa.config.ExternalServer;
import com.utepsa.models.FinancialState;
import io.dropwizard.jackson.Jackson;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 * Created by Gerardo on 28/03/2017.
 */
public class FinancialStateAdapter {
    private final ObjectMapper MAPPER = Jackson.newObjectMapper();
    private ExternalServer externalServer;

    public FinancialStateAdapter(ExternalServer externalServer) {
        this.externalServer = externalServer;
    }

    private HttpResponse doRequestTo(String url) throws IOException {
        final HttpClient client = HttpClientBuilder.create().build();
        HttpGet request = new HttpGet(url);
        return client.execute(request);
    }

    public boolean isServerUp() throws IOException {
        return this.doRequestTo(externalServer.getUrl()+"/deudas").getStatusLine()
                .getStatusCode() == HttpStatus.SC_OK;
    }

    private String buildRequestUrl(String agendCode) {
        return String.format("%sdeudas/%s",
                this.externalServer.getUrl(),
                agendCode);
    }

    public List<FinancialStateData> getAllFinancialState() throws IOException{
        String url = this.buildRequestUrl("");
        HttpResponse response = this.doRequestTo(url);

        if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
            List<FinancialStateData> financialState;

            financialState = MAPPER.readValue(response.getEntity().getContent(), new TypeReference<List<FinancialStateData>>(){});

            return financialState;
        }
        return null;
    }

    public FinancialStateData getFinancialState(String agendCode) throws IOException {
        String url = this.buildRequestUrl(agendCode);
        HttpResponse response = this.doRequestTo(url);

        if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
            FinancialStateData financialStateData = MAPPER.readValue(response.getEntity().getContent(), new TypeReference<FinancialStateData>(){});
            return financialStateData;
        }
        return null;
    }
}
