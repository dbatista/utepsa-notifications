package com.utepsa.adapters.utepsa.schedules;

/**
 * Created by Gerardo on 14/03/2017.
 */
public class SchedulesDATA {

    private String tur_codigo;
    private String tur_descripcion;
    private String ttur_codigo;
    private String tur_horario;
    private String tur_status;
    private String ttur_descripcion;

    public SchedulesDATA(){

    }

    public SchedulesDATA(String tur_codigo, String tur_descripcion, String ttur_codigo, String tur_horario, String tur_status, String ttur_descripcion){
        this.tur_codigo = tur_codigo;
        this.tur_descripcion = tur_descripcion;
        this.ttur_codigo = ttur_codigo;
        this.tur_horario = tur_horario;
        this.tur_status = tur_status;
        this.ttur_descripcion = ttur_descripcion;
    }

    public String getTur_codigo() {
        return tur_codigo;
    }

    public void setTur_codigo(String tur_codigo) {
        this.tur_codigo = tur_codigo;
    }

    public String getTur_descripcion() {
        return tur_descripcion;
    }

    public void setTur_descripcion(String tur_descripcion) {
        this.tur_descripcion = tur_descripcion;
    }

    public String getTtur_codigo() {
        return ttur_codigo;
    }

    public void setTtur_codigo(String ttur_codigo) {
        this.ttur_codigo = ttur_codigo;
    }

    public String getTur_horario() {
        return tur_horario;
    }

    public void setTur_horario(String tur_horario) {
        this.tur_horario = tur_horario;
    }

    public String getTur_status() {
        return tur_status;
    }

    public void setTur_status(String tur_status) {
        this.tur_status = tur_status;
    }

    public String getTtur_descripcion() {
        return ttur_descripcion;
    }

    public void setTtur_descripcion(String ttur_descripcion) {
        this.ttur_descripcion = ttur_descripcion;
    }

    @Override
    public String toString() {
        return "SchedulesDATA{" +
                "tur_codigo='" + tur_codigo + '\'' +
                ", tur_descripcion='" + tur_descripcion + '\'' +
                ", ttur_codigo='" + ttur_codigo + '\'' +
                ", tur_horario='" + tur_horario + '\'' +
                ", tur_status='" + tur_status + '\'' +
                ", ttur_descripcion='" + ttur_descripcion + '\'' +
                '}';
    }
}
