package com.utepsa.resources.credential;

import com.google.inject.Inject;
import com.utepsa.api.custom.ChangePasswordForced;
import com.utepsa.api.response.BasicResponse;
import com.utepsa.core.Operations;
import com.utepsa.core.StatusCode;
import com.utepsa.db.credential.CredentialDAO;
import com.utepsa.db.student.StudentDAO;
import com.utepsa.models.Credential;

/**
 * Created by david on 19/10/16.
 */
public class CredentialService {
    private final CredentialDAO credentialDAO;
    private final StudentDAO studentDAO;

    @Inject
    public CredentialService(CredentialDAO credentialDAO, StudentDAO studentDAO) {
        this.credentialDAO = credentialDAO;
        this.studentDAO = studentDAO;
    }

    public BasicResponse login(String username, String password){
        BasicResponse basicResponse = new BasicResponse();
        Credential credential = null;
        try {
            credential = credentialDAO.login(username, password);
            if(credential != null){
                if(!credential.isChangePasswordForced()){
                    credentialDAO.registerLastConnection(credential);
                    basicResponse.setCode(StatusCode.OK);
                    basicResponse.setUniqueResult(credential);
                }else {
                    basicResponse.setCode(StatusCode.FORBIDDEN);
                    basicResponse.setMessage("You must change your password");
                    basicResponse.setUniqueResult(credential);
                }
            }else{
                basicResponse.setCode(StatusCode.NOT_FOUND);
                basicResponse.setMessage("The user does not exist");
            }

        } catch (Exception e) {
            basicResponse = Operations.getResponseErrors(e);
        }
        return basicResponse;
    }

    public BasicResponse registerGcmId(long idCredential, String gcmId){
        BasicResponse basicResponse = new BasicResponse();
        try {
            credentialDAO.registerGcmId(idCredential, gcmId);
            basicResponse.setCode(StatusCode.OK);
            basicResponse.setMessage("Gcm ID successfully registered");
        } catch (Exception e) {
            basicResponse = Operations.getResponseErrors(e);
        }

        return basicResponse;
    }

    public BasicResponse changePasswordForced(ChangePasswordForced changePasswordForced){
        BasicResponse basicResponse = new BasicResponse();
        try {
            if(changePasswordForced.getNewPassword().equals(changePasswordForced.getConfimNewPassword())){
                Credential credential = credentialDAO.getById(changePasswordForced.getId());
                if(credential != null){
                    if(credential.isChangePasswordForced()){
                        if(!credential.getPassword().equals(changePasswordForced.getNewPassword())){
                            credentialDAO.changePasswordForced(credential, changePasswordForced.getNewPassword());
                            basicResponse.setCode(StatusCode.OK);
                        }else{
                            basicResponse.setCode(StatusCode.NOT_MODIFIED);
                            basicResponse.setMessage("It was not possible to change the password, Because the new password is the same as the old one.");
                        }
                    }else {
                        basicResponse.setCode(StatusCode.UNAUTHORIZED);
                        basicResponse.setMessage("You are not authorized to change your password");
                    }
                }else{
                    basicResponse.setCode(StatusCode.NOT_FOUND);
                    basicResponse.setMessage("User does not exist");
                }
            }else{
                basicResponse.setCode(StatusCode.NOT_MODIFIED);
                basicResponse.setMessage("It was not possible to change the password, because the confirmation of the password is not the same as the new password.");
            }
        } catch (Exception e) {
            basicResponse = Operations.getResponseErrors(e);
        }
        return basicResponse;
    }

    public BasicResponse resetPassword(long id,String password, String confirmPassword, boolean forcedPssword)
    {
        BasicResponse basicResponse =new BasicResponse();
        try {
            if (credentialDAO.getById(id)==null)
            {
                basicResponse.setCode(StatusCode.NOT_FOUND);
                basicResponse.setMessage("credential doesn't exist");
                return basicResponse;
            }
            if (password.equals(confirmPassword))
            {
                Credential credential = credentialDAO.getById(id);
                credential.setPassword(password);
                credential.setChangePasswordForced(forcedPssword);
                basicResponse.setCode(StatusCode.OK);
            }
            else{
                basicResponse.setCode(StatusCode.NOT_MODIFIED);
                basicResponse.setMessage("It was not possible to change the password, Because the password wasn't entered correctly.");
            }
        }catch (Exception e){
            basicResponse = Operations.getResponseErrors(e);
        }
        return basicResponse;
    }
}
