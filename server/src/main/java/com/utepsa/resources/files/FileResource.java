package com.utepsa.resources.files;

import com.codahale.metrics.annotation.Timed;
import com.google.inject.Inject;
import com.utepsa.api.response.BasicResponse;
import com.utepsa.core.StatusCode;
import com.utepsa.models.File;
import io.dropwizard.hibernate.UnitOfWork;
import io.dropwizard.jersey.params.LongParam;
import io.swagger.annotations.*;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URI;
import java.util.List;

/**
 * Created by shigeots on 09-11-16.
 */
@Path(FileResource.SERVICE_PATH)
@Api(value = FileResource.SERVICE_PATH, description = "Operations about Students")
@Produces(MediaType.APPLICATION_JSON)
public class FileResource {
    public final static String SERVICE_PATH = "/file";
    private final FileService service;

    @Inject
    public FileResource(FileService service) {
        this.service = service;
    }

    @GET
    @Timed
    @UnitOfWork
    @Path("/available")
    @ApiOperation(value = "Gets all available Files", response = BasicResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = StatusCode.OK, message = "List of Files found"),
            @ApiResponse(code = StatusCode.NO_CONTENT, message = "No files are available")})
    public BasicResponse getAllAvailableFiles() {
        return service.getAllAvailable();
    }

    @GET
    @Timed
    @UnitOfWork
    @Path("/{id}")
    @ApiOperation( value = "Gets the file by id", response = BasicResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = StatusCode.OK, message = "File found"),
            @ApiResponse(code = StatusCode.NOT_FOUND, message = "File not found"),
            @ApiResponse(code = StatusCode.INTERNAL_SERVER_ERROR, message = "Internal server error")})
    public BasicResponse getFilesById(@ApiParam(value = "File ID needed to find the file", required = true)
                                          @PathParam("id") LongParam id) {
        return service.getById(id.get());
    }

    @POST
    @Timed
    @UnitOfWork
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation( value = "Create a file", response = BasicResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = StatusCode.CREATED, message = "The file has been created"),
            @ApiResponse(code = StatusCode.INTERNAL_SERVER_ERROR, message = "Internal server error") })
    public BasicResponse createFile(
            @ApiParam(value = "JSON format is received with the file structure.", required = true) File file) {
        return service.createFile(file);
    }
}
