package com.utepsa.resources.files;

import com.google.inject.Inject;
import com.utepsa.api.response.BasicResponse;
import com.utepsa.core.Operations;
import com.utepsa.core.StatusCode;
import com.utepsa.models.File;
import com.utepsa.db.file.FileDAO;

import java.util.List;

/**
 * Created by shigeots on 09-11-16.
 */
public class FileService {
    private final FileDAO dao;

    @Inject
    public FileService(FileDAO dao) {
        this.dao = dao;
    }

    public BasicResponse createFile(File file) {
        BasicResponse basicResponse = new BasicResponse();
        try {
            dao.create(file);
            basicResponse.setCode(StatusCode.CREATED);
            basicResponse.setMessage("File created correctly");
        } catch (Exception e) {
            basicResponse = Operations.getResponseErrors(e);
        }
        return basicResponse;
    }

    public BasicResponse getById(long id){
        BasicResponse basicResponse = new BasicResponse();
        try {
            File file = dao.getById(id);
            if(file != null){
                basicResponse.setCode(StatusCode.OK);
                basicResponse.setUniqueResult(file);
            }else{
                basicResponse.setCode(StatusCode.NOT_FOUND);
                basicResponse.setMessage("This file was not found");
            }
        } catch (Exception e) {
            basicResponse = Operations.getResponseErrors(e);
        }
        return basicResponse;
    }

    public BasicResponse getAllAvailable() {
        BasicResponse basicResponse = new BasicResponse();
        try {
            List<File> files = dao.getAllAvailable();
            if(files != null && files.size() > 0){
                basicResponse.setCode(StatusCode.OK);
                basicResponse.setData(files);
            }else {
                basicResponse.setCode(StatusCode.NO_CONTENT);
                basicResponse.setMessage("No files are available");
            }
        } catch (Exception e) {
            basicResponse = Operations.getResponseErrors(e);
        }
        return basicResponse;
    }
}
