package com.utepsa.resources.credentialAdministrator;

import com.google.inject.Inject;
import com.utepsa.api.custom.ChangePasswordForced;
import com.utepsa.api.response.BasicResponse;
import com.utepsa.core.Operations;
import com.utepsa.core.StatusCode;
import com.utepsa.db.Profile.ProfileDAO;
import com.utepsa.db.credentialAdministrator.CredentialAdministratorDAO;
import com.utepsa.db.permissionCredential.PermissionCredentialDAO;
import com.utepsa.db.permissionRole.PermissionRoleDAO;
import com.utepsa.models.*;
import java.util.List;
/**
 * Created by Gerardo on 12/02/2017.
 */
public class CredentialAdministratorService {
    private final ProfileDAO profileDAO;
    private final CredentialAdministratorDAO credentialAdministratorDAO;
    private final PermissionRoleDAO permissionRoleDAO;
    private final PermissionCredentialDAO permissionCredentialDAO;

    @Inject
    public CredentialAdministratorService(ProfileDAO profileDAO, CredentialAdministratorDAO credentialAdministratorDAO, PermissionRoleDAO permissionRoleDAO, PermissionCredentialDAO permissionCredentialDAO) {
        this.credentialAdministratorDAO = credentialAdministratorDAO;
        this.profileDAO = profileDAO;
        this.permissionRoleDAO = permissionRoleDAO;
        this.permissionCredentialDAO = permissionCredentialDAO;
    }

    public BasicResponse create(CredentialAdministrator credentialAdministrator) {
        BasicResponse basicResponse = new BasicResponse();
        try {
            Profile profile = credentialAdministrator.getProfile();
            if(profile.getName().trim().equals("") || profile.getName().equals(null)) {
                basicResponse.setMessage("Not created, because name is missing");
                basicResponse.setCode(StatusCode.NOT_FOUND);
            }
            else if (profile.getFatherLastname().trim().equals("") || profile.getFatherLastname().equals(null)) {
                basicResponse.setMessage("Not created, because father lastname is missing");
                basicResponse.setCode(StatusCode.NOT_FOUND);
            }
            else if (profile.getMotherLastname().trim().equals("") || profile.getMotherLastname().equals(null)) {
                basicResponse.setMessage("Not created, because mother lastname is missing");
                basicResponse.setCode(StatusCode.NOT_FOUND);
            }
            else
            {
                if(credentialAdministratorDAO.getByUsername(credentialAdministrator.getUsername()) != null){
                    basicResponse.setCode(StatusCode.CONFLICT);
                    basicResponse.setMessage("User already exist");
                } else if(credentialAdministrator.getPassword().trim().isEmpty()) {
                    basicResponse.setCode(StatusCode.NOT_FOUND);
                    basicResponse.setMessage("Password can not be empty");
                } else if (credentialAdministrator.getRole() == null){
                    basicResponse.setCode(StatusCode.NOT_FOUND);
                    basicResponse.setMessage("I do not assign a role");
                } else if(credentialAdministrator.getPassword().length() <= 7 || credentialAdministrator.getPassword().length() >= 51){
                    basicResponse.setCode(StatusCode.CONFLICT);
                    basicResponse.setMessage("The password must be at least eight characters and a maximum of fifty");
                } else {
                    profileDAO.create(profile);
                    credentialAdministrator.setProfile(profile);
                    credentialAdministrator.setState(true);
                    long idCredential = credentialAdministratorDAO.create(credentialAdministrator);
                    List<PermissionRole> permissionRoles = permissionRoleDAO.getByIdRole(credentialAdministrator.getRole().getId());
                    for(PermissionRole permissionRole: permissionRoles)
                    {
                        PermissionCredential credential = new PermissionCredential();
                        credential.setCredentialAdministrator(idCredential);
                        credential.setPermission(permissionRole.getPermission());
                        credential.setState(permissionRole.isState());
                        permissionCredentialDAO.create(credential);
                    }
                    basicResponse.setCode(StatusCode.CREATED);
                    basicResponse.setMessage("The Credential Administrator has been created");
                }
            }
        } catch (Exception e) {
            basicResponse = Operations.getResponseErrors(e);
        }
        return basicResponse;
    }

    public BasicResponse modify(long idCredential, Profile profile){
        BasicResponse basicResponse = new BasicResponse();
        try{
            if(profile != null){
                CredentialAdministrator credentialAdministrator = credentialAdministratorDAO.getById(idCredential);
                if(credentialAdministrator != null && credentialAdministrator.isState() != false){
                    profile.setId(credentialAdministrator.getProfile().getId());
                    profileDAO.modify(profile);
                    basicResponse.setCode(StatusCode.OK);
                    basicResponse.setMessage("Modified correctly");
                }else {
                    basicResponse.setCode(StatusCode.NOT_MODIFIED);
                    basicResponse.setMessage("It has not been modified because credential administrator not found");
                }
            } else {
                basicResponse.setCode(StatusCode.NOT_MODIFIED);
                basicResponse.setMessage("It has not been modified because it is null");
            }
        } catch (Exception e) {
            basicResponse = Operations.getResponseErrors(e);
        }
        return basicResponse;
    }

    public BasicResponse delete(long id){
        BasicResponse basicResponse = new BasicResponse();
        try{
            CredentialAdministrator credentialAdministrator = credentialAdministratorDAO.getById(id);
            if(credentialAdministrator.isState() == true){
                credentialAdministratorDAO.delete(id);
                basicResponse.setCode(StatusCode.OK);
                basicResponse.setMessage("delete correctly");
            }else if (credentialAdministrator.isState() == false){
                basicResponse.setCode(StatusCode.NOT_FOUND);
                basicResponse.setMessage("This credential administrator was not exist");
            }else{
                basicResponse.setCode(StatusCode.INTERNAL_SERVER_ERROR);
            }
        } catch (Exception e) {
            basicResponse = Operations.getResponseErrors(e);
        }
        return basicResponse;
    }

    public BasicResponse getAll(){
        BasicResponse basicResponse = new BasicResponse();
        try {
            List<CredentialAdministrator> credentialAdministrators = credentialAdministratorDAO.getAll();
            if(credentialAdministrators.size() == 0 || credentialAdministrators.equals(null)){
                basicResponse.setCode(StatusCode.NO_CONTENT);
                basicResponse.setMessage("There are no credential administrator to show");
            }else{
                basicResponse.setCode(StatusCode.OK);
                basicResponse.setData(credentialAdministrators);
            }
        } catch (Exception e) {
            basicResponse = Operations.getResponseErrors(e);
        }
        return basicResponse;
    }

    public BasicResponse getById(long id){
        BasicResponse basicResponse = new BasicResponse();
        try {
            CredentialAdministrator credentialAdministrator = credentialAdministratorDAO.getById(id);
            if(credentialAdministrator != null && credentialAdministrator.isState() == true){
                basicResponse.setCode(StatusCode.OK);
                basicResponse.setUniqueResult(credentialAdministrator);
            }
            else{
                basicResponse.setCode(StatusCode.NOT_FOUND);
                basicResponse.setMessage("This credential administrator was not exist");
            }
        } catch (Exception e) {
            basicResponse = Operations.getResponseErrors(e);
        }
        return basicResponse;
    }

    public BasicResponse getByIdCredential(long idCredential){
        BasicResponse basicResponse = new BasicResponse();
        try {
            List<PermissionCredential> permissionCredentials = permissionCredentialDAO.getByIdCredential(idCredential);
            if(permissionCredentials.size() == 0){
                basicResponse.setCode(StatusCode.NO_CONTENT);
                basicResponse.setMessage("There are no permission credential to show");
            }else{
                basicResponse.setCode(StatusCode.OK);
                basicResponse.setData(permissionCredentials);
            }
        } catch (Exception e) {
            basicResponse = Operations.getResponseErrors(e);
        }
        return basicResponse;
    }

    public BasicResponse modifyPermissionCredential(List<PermissionCredential> permissionCredentials){
        BasicResponse basicResponse = new BasicResponse();
        try{
            if(permissionCredentials != null){
                permissionCredentialDAO.modify(permissionCredentials);
                basicResponse.setCode(StatusCode.OK);
                basicResponse.setMessage("modify correctly");
            } else {
                basicResponse.setCode(StatusCode.NOT_MODIFIED);
                basicResponse.setMessage("It was not modified");
            }
        } catch (Exception e) {
            basicResponse = Operations.getResponseErrors(e);
        }
        return basicResponse;
    }

    public BasicResponse login(String username, String password){
        BasicResponse basicResponse = new BasicResponse();
        CredentialAdministrator credentialAdministrator = null;
        try {
            credentialAdministrator = credentialAdministratorDAO.login(username, password);
            if(credentialAdministrator != null){
                if(!credentialAdministrator.isChangePasswordForced()){
                    credentialAdministratorDAO.registerLastConnection(credentialAdministrator);
                    basicResponse.setCode(StatusCode.OK);
                    basicResponse.setUniqueResult(credentialAdministrator);
                }else {
                    basicResponse.setCode(StatusCode.FORBIDDEN);
                    basicResponse.setMessage("You must change your password");
                    basicResponse.setUniqueResult(credentialAdministrator);
                }
            }else{
                basicResponse.setCode(StatusCode.NOT_FOUND);
                basicResponse.setMessage("The user does not exist");
            }

        } catch (Exception e) {
            basicResponse = Operations.getResponseErrors(e);
        }
        return basicResponse;
    }

    public BasicResponse registerGcmId(long idCredential, String gcmId){
        BasicResponse basicResponse = new BasicResponse();
        try {
            credentialAdministratorDAO.registerGcmId(idCredential, gcmId);
            basicResponse.setCode(StatusCode.OK);
            basicResponse.setMessage("Gcm ID successfully registered");
        } catch (Exception e) {
            basicResponse = Operations.getResponseErrors(e);
        }
        return basicResponse;
    }

    public BasicResponse changePasswordForced(ChangePasswordForced changePasswordForced){
        BasicResponse basicResponse = new BasicResponse();
        try {
            if(changePasswordForced.getNewPassword().equals(changePasswordForced.getConfimNewPassword())){
                CredentialAdministrator credentialAdministrator = credentialAdministratorDAO.getById(changePasswordForced.getId());
                if(credentialAdministrator != null && credentialAdministrator.isState() != false){
                    if(credentialAdministrator.isChangePasswordForced()){
                        if(!credentialAdministrator.getPassword().equals(changePasswordForced.getNewPassword())){
                            credentialAdministratorDAO.changePasswordForced(credentialAdministrator, changePasswordForced.getNewPassword());
                            basicResponse.setCode(StatusCode.OK);
                            basicResponse.setMessage("Password changed correctly.");
                        }else{
                            basicResponse.setCode(StatusCode.NOT_MODIFIED);
                            basicResponse.setMessage("It was not possible to change the password, Because the new password is the same as the old one.");
                        }
                    }else {
                        basicResponse.setCode(StatusCode.UNAUTHORIZED);
                        basicResponse.setMessage("You are not authorized to change your password");
                    }
                }else{
                    basicResponse.setCode(StatusCode.NOT_FOUND);
                    basicResponse.setMessage("User does not exist");
                }
            }else{
                basicResponse.setCode(StatusCode.NOT_MODIFIED);
                basicResponse.setMessage("It was not possible to change the password, because the confirmation of the password is not the same as the new password.");
            }
        } catch (Exception e) {
            basicResponse = Operations.getResponseErrors(e);
        }
        return basicResponse;
    }
}
