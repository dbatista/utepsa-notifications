package com.utepsa.resources.credentialAdministrator;

import com.codahale.metrics.annotation.Timed;
import com.google.inject.Inject;
import com.utepsa.api.custom.ChangePasswordForced;
import com.utepsa.api.response.BasicResponse;
import com.utepsa.core.StatusCode;
import com.utepsa.models.CredentialAdministrator;
import com.utepsa.models.PermissionCredential;
import com.utepsa.models.Profile;
import io.dropwizard.hibernate.UnitOfWork;
import io.swagger.annotations.*;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

/**
 * Created by Gerardo on 12/02/2017.
 */
@Path(CredentialAdministratorResource.SERVICE_PATH)
@Produces(MediaType.APPLICATION_JSON)
@Api(value = "Administrator / Credentials", description = "Operations about Auth")
public class CredentialAdministratorResource {
    public static final String SERVICE_PATH = "administrator/credentials";

    private final CredentialAdministratorService service;

    @Inject
    public CredentialAdministratorResource(CredentialAdministratorService service) {
        this.service = service;
    }

    @POST
    @Timed
    @UnitOfWork
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value = "Create Credential Administrator", response = BasicResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = StatusCode.CREATED, message = "The Credential Administrator has been created"),
            @ApiResponse(code = StatusCode.NOT_FOUND, message = "Not created, because name is missing"),
            @ApiResponse(code = StatusCode.NOT_FOUND, message = "Not created, because father lastname is missing"),
            @ApiResponse(code = StatusCode.NOT_FOUND, message = "Not created, because mother lastname is missing"),
            @ApiResponse(code = StatusCode.NOT_FOUND, message = "Password can not be empty"),
            @ApiResponse(code = StatusCode.NOT_FOUND, message = "I do not assign a role"),
            @ApiResponse(code = StatusCode.CONFLICT, message = "User already exist"),
            @ApiResponse(code = StatusCode.CONFLICT, message = "The password must be at least eight characters and a maximum of fifty"),
            @ApiResponse(code = StatusCode.INTERNAL_SERVER_ERROR, message = "Internal server error") })
    public BasicResponse create(
            @ApiParam(value = "JSON format is received with the file structure.", required = true) CredentialAdministrator credentialAdministrator) {
        return service.create(credentialAdministrator);
    }

    @PUT
    @Timed
    @UnitOfWork
    @Path("{id}/profile")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation( value = "Modifies the profile of an administrator's credentials.", response = BasicResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = StatusCode.OK, message = "The administrator credential profile was modified"),
            @ApiResponse(code = StatusCode.NOT_MODIFIED, message = "The credential administrator profile was not modified because it is null"),
            @ApiResponse(code = StatusCode.NOT_MODIFIED, message = "The credential administrator profile was not modified because credential administrator not found"),
            @ApiResponse(code = StatusCode.INTERNAL_SERVER_ERROR, message = "Internal server error") })
    public BasicResponse modify(
            @ApiParam(value = "ID Credential", required = true) @PathParam("id") long id,
            @ApiParam(value = "JSON format is received with the Credential Administrator structure.", required = true)Profile profile) {
        return service.modify(id, profile);
    }

    @DELETE
    @Timed
    @UnitOfWork
    @Path("/{id}")
    @ApiOperation(value = "Deletes a credential of administrator by id.", response = BasicResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = StatusCode.OK, message = "The credential administrator was deleted"),
            @ApiResponse(code = StatusCode.NOT_FOUND, message = "This credential administrator was not exist"),
            @ApiResponse(code = StatusCode.INTERNAL_SERVER_ERROR, message = "Internal server error") })
    public BasicResponse delete(@PathParam("id") long id) {
        return service.delete(id);
    }

    @POST
    @Timed
    @UnitOfWork
    @Path("/auth")
    @Consumes(MediaType.TEXT_PLAIN)
    @Produces("application/json")
    @ApiOperation( value = "Allows a student to initiate a session", response = BasicResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = StatusCode.OK, message = "Signed in successfully"),
            @ApiResponse(code = StatusCode.FORBIDDEN, message = "You must change your password"),
            @ApiResponse(code = StatusCode.NOT_FOUND, message = "User does not exist"),
            @ApiResponse(code = StatusCode.INTERNAL_SERVER_ERROR, message = "Internal Server Error")})
    public BasicResponse loginUser(@ApiParam(value = "Username needed to login", required = true)
                                   @HeaderParam("username") String username,
                                   @ApiParam(value = "Password needed to login", required = true)
                                   @HeaderParam("password") String password) {
        try {
            return service.login(username.toUpperCase(), password);
        } catch (Exception e) {
            throw new WebApplicationException(e.getMessage(), Response.Status.NOT_ACCEPTABLE);
        }
    }

    @GET
    @Timed
    @UnitOfWork
    @ApiOperation(value = "Gets all available Credentials Administrator", response = BasicResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = StatusCode.OK, message = "List of Credentials Administrator found"),
            @ApiResponse(code = StatusCode.NO_CONTENT, message = "No Credential Administrator are available"),
            @ApiResponse(code = StatusCode.INTERNAL_SERVER_ERROR, message = "Internal server error")})
    public BasicResponse getAll() {
        return service.getAll();
    }

    @GET
    @Timed
    @UnitOfWork
    @Path("/{id}")
    @ApiOperation( value = "Gets the Credential Administrator by id", response = BasicResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = StatusCode.OK, message = "Credential Administrator found"),
            @ApiResponse(code = StatusCode.NOT_FOUND, message = "Credential Administrator not found"),
            @ApiResponse(code = StatusCode.INTERNAL_SERVER_ERROR, message = "Internal server error")})
    public BasicResponse getById(@ApiParam(value = "Credential Administrator ID needed to find the Credentials", required = true)
                                      @PathParam("id") long id) {
        return service.getById(id);
    }

    @PUT
    @Timed
    @UnitOfWork
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/changePasswordForced/")
    @ApiOperation( value = "Allows the change of password of an administrator's credentials and establishes a new.", response = BasicResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = StatusCode.OK, message = "Password changed correctly."),
            @ApiResponse(code = StatusCode.NOT_MODIFIED, message = "It was not possible to change the password, Because the new password is the same as the old one."),
            @ApiResponse(code = StatusCode.NOT_MODIFIED, message = "It was not possible to change the password, because the confirmation of the password is not the same as the new password."),
            @ApiResponse(code = StatusCode.UNAUTHORIZED, message = "You are not authorized to change your password"),
            @ApiResponse(code = StatusCode.NOT_FOUND, message = "User does not exist"),
            @ApiResponse(code = StatusCode.INTERNAL_SERVER_ERROR, message = "Internal server error")})
    public BasicResponse changePasswordForced(@ApiParam(value = "Received in JSON format.", required = true)
                                                      ChangePasswordForced changePasswordForced) {
        return service.changePasswordForced(changePasswordForced);
    }

    @GET
    @Timed
    @UnitOfWork
    @Path("/{id}/permissions")
    @ApiOperation(value = "Gets by ID Credential available Permission Credential", response = BasicResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = StatusCode.OK, message = "List of Credentials Permission found"),
            @ApiResponse(code = StatusCode.NO_CONTENT, message = "No Credential Permission are available"),
            @ApiResponse(code = StatusCode.INTERNAL_SERVER_ERROR, message = "Internal server error") })
    public BasicResponse getByIdCredential(@ApiParam(value = "Credential Administrator ID needed to find the Credentials Permissions", required = true)
                                                        @PathParam("id") long idCredential) {
        return service.getByIdCredential(idCredential);
    }

    @PUT
    @Timed
    @UnitOfWork
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value = "Modify the permissions of an administrator credential.", response = BasicResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = StatusCode.OK, message = "The permission was modified"),
            @ApiResponse(code = StatusCode.NOT_MODIFIED, message = "The permission was not modified"),
            @ApiResponse(code = StatusCode.INTERNAL_SERVER_ERROR, message = "Internal server error") })
    public BasicResponse modifyPermissionCredential(
            @ApiParam(value = "JSON format is received with the Credential Administrator structure.", required = true)
                    List<PermissionCredential> permissionCredentials) {
        return service.modifyPermissionCredential(permissionCredentials);
    }
}
