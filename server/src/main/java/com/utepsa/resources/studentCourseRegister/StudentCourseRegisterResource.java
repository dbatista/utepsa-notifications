package com.utepsa.resources.studentCourseRegister;

import com.codahale.metrics.annotation.Timed;
import com.google.inject.Inject;
import com.utepsa.api.response.BasicResponse;
import com.utepsa.core.StatusCode;
import io.dropwizard.hibernate.UnitOfWork;
import io.swagger.annotations.*;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 * Created by Luana Chavez on 02/03/2017.
 */
@Path("/StudentCourseRegister")
@Api(value = "Student Course Register", description = "Operations about Student Course Registers")
@Produces(MediaType.APPLICATION_JSON)
public class StudentCourseRegisterResource {

    private final StudentCourseRegisterService service;

    @Inject
    public StudentCourseRegisterResource(StudentCourseRegisterService service) {
        this.service = service;
    }

    @GET
    @Timed
    @UnitOfWork
    @Path("/{id}")
    @ApiOperation( value = "Gets Student Course Register by id", response = BasicResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = StatusCode.OK, message = "Student Course Register were successfully obtained."),
            @ApiResponse(code = StatusCode.NOT_FOUND, message = "Course Register not found"),
            @ApiResponse(code = StatusCode.INTERNAL_SERVER_ERROR, message = "Internal server error")})
    public BasicResponse getCourseRegistrationById (@ApiParam(value = "ID of Course Register that needs to be fetched", required = true)
                                                    @PathParam("id") long id) {
        return service.getCourseRegisterById(id);
    }
}
