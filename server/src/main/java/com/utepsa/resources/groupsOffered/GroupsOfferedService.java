package com.utepsa.resources.groupsOffered;

import com.google.inject.Inject;
import com.utepsa.api.response.BasicResponse;
import com.utepsa.core.Operations;
import com.utepsa.core.StatusCode;
import com.utepsa.db.academicArea.AcademicAreaDAO;
import com.utepsa.db.career.CareerDAO;
import com.utepsa.db.groupsOffered.GroupsOfferedDAO;
import com.utepsa.models.GroupsOffered;
import java.util.List;

/**
 * Created by Gerardo on 26/04/2017.
 */
public class GroupsOfferedService {

    private GroupsOfferedDAO groupsOfferedDAO;
    private CareerDAO careerDAO;
    private AcademicAreaDAO academicAreaDAO;

    @Inject
    public GroupsOfferedService(GroupsOfferedDAO groupsOfferedDAO, CareerDAO careerDAO, AcademicAreaDAO academicAreaDAO){
        this.groupsOfferedDAO = groupsOfferedDAO;
        this.careerDAO = careerDAO;
        this.academicAreaDAO = academicAreaDAO;
    }

    public BasicResponse getByCareerSemesterAndModule(long idCareer, int pensum, String semester, String module) throws Exception{
        BasicResponse basicResponse = new BasicResponse();
            if(careerDAO.getById(idCareer) == null){
                basicResponse.setCode(StatusCode.NOT_FOUND);
                basicResponse.setMessage("career doesn't exist");
                return  basicResponse;
            }

            if(semester == null) {
                basicResponse.setCode(StatusCode.NOT_FOUND);
                basicResponse.setMessage("semester is empty");
                return basicResponse;
            }

            List<GroupsOffered> groupsOffereds;
            if(module == null){
                groupsOffereds = groupsOfferedDAO.getByCareerAndSemester(idCareer, pensum, semester);
            }else {
                groupsOffereds = groupsOfferedDAO.getByCareerSemesterAndModule(idCareer, pensum, semester, module);
            }

            if(groupsOffereds.size() > 0){
                basicResponse.setCode(StatusCode.OK);
                basicResponse.setUniqueResult(groupsOffereds);
            }else{
                basicResponse.setCode(StatusCode.NOT_FOUND);
                basicResponse.setMessage("This Groups Offered was not found");
            }
        return basicResponse;
    }

    public BasicResponse getByAcademicAreaAndModule(long idAcademicArea, String semester, String module){
        BasicResponse basicResponse = new BasicResponse();
        try {
            if(semester == null){
                basicResponse.setCode(StatusCode.NOT_FOUND);
                basicResponse.setMessage("semester is empty");
                return basicResponse;
            }

            if(academicAreaDAO.getById(idAcademicArea) == null){
                basicResponse.setCode(StatusCode.NO_CONTENT);
                basicResponse.setMessage("Academic area doesn't exist");
                return basicResponse;
            }

            List<GroupsOffered> groupsOffereds;
            if(module != null){
                groupsOffereds = groupsOfferedDAO.getByAcademicAreaAndSemester(idAcademicArea, semester, module);
            } else {
                groupsOffereds = groupsOfferedDAO.getByAcademicArea(idAcademicArea, semester);
            }

            if(groupsOffereds.size() > 0){
                basicResponse.setCode(StatusCode.OK);
                basicResponse.setUniqueResult(groupsOffereds);
                return basicResponse;
            }else{
                basicResponse.setCode(StatusCode.NOT_FOUND);
                basicResponse.setMessage("This Groups Offered was not found");
            }
        } catch (Exception e) {
            basicResponse = Operations.getResponseErrors(e);
        }
        return basicResponse;
    }
}
