package com.utepsa.resources.academicArea;

import com.google.inject.Inject;
import com.utepsa.api.response.BasicResponse;
import com.utepsa.core.Operations;
import com.utepsa.core.StatusCode;
import com.utepsa.db.academicArea.AcademicAreaDAO;
import com.utepsa.models.AcademicArea;

import java.util.List;

/**
 * Created by Gerardo on 30/04/2017.
 */
public class AcademicAreaService {

    private AcademicAreaDAO academicAreaDAO;

    @Inject
    public AcademicAreaService(AcademicAreaDAO academicAreaDAO){
        this.academicAreaDAO = academicAreaDAO;
    }

    public BasicResponse getAll(){
        BasicResponse basicResponse = new BasicResponse();
        try {
            List<AcademicArea> academicAreas = academicAreaDAO.getAll();
            if(academicAreas.size() == 0){
                basicResponse.setCode(StatusCode.NO_CONTENT);
                basicResponse.setMessage("There are no academic area to show");
            }else{
                basicResponse.setCode(StatusCode.OK);
                basicResponse.setData(academicAreas);
            }
        } catch (Exception e) {
            basicResponse = Operations.getResponseErrors(e);
        }
        return basicResponse;
    }
}
