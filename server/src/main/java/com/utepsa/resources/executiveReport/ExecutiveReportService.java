package com.utepsa.resources.executiveReport;

import com.google.inject.Inject;
import com.utepsa.api.custom.*;
import com.utepsa.api.response.BasicResponse;
import com.utepsa.core.StatusCode;
import com.utepsa.db.career.CareerDAO;
import com.utepsa.db.executiveReport.ExecutiveReportDAO;
import com.utepsa.models.Career;

import java.util.*;
import java.util.stream.Stream;

/**
 * Created by Luana Chavez on 17/04/2017.
 */
public class ExecutiveReportService {
    private final ExecutiveReportDAO executiveReportDAO;
    private final CareerDAO careerDao;

    @Inject
    public ExecutiveReportService(ExecutiveReportDAO executiveReportDAO, CareerDAO careerDao) {
        this.executiveReportDAO = executiveReportDAO;
        this.careerDao = careerDao;
    }

    public BasicResponse getQuantityOfStudent(String semester){
        BasicResponse basicResponse = new BasicResponse();
        try {
            ExecutiveReport executiveReport = new ExecutiveReport();
            List<DataCareer> dataCareers = new ArrayList<>();
            List<StudentCustom> studentCustoms = executiveReportDAO.getStudentCustom();
            List<StudentCourseRegisterCustom> studentCourseRegisterCustoms = executiveReportDAO.getStudentCourseRegisterCustom(semester.trim());
            executiveReport.setSemester(semester);
            executiveReport.setTotal(studentCustoms.stream().count());
            executiveReport.setTotalActive(studentCourseRegisterCustoms.stream().count());
            executiveReport.setAverage(executiveReportDAO.getotalAverage());
            Stream<Career> careers = careerDao.getAll().stream();
            careers.forEach( career -> {
                DataCareer newDataCareer = new DataCareer();
                newDataCareer.setIdCareer(career.getId());
                newDataCareer.setSameCareer(career.getSameCareer());
                newDataCareer.setPlan(career.getPlan());
                newDataCareer.setNameCareer(career.getName());
                try {
                    newDataCareer.setAverage(executiveReportDAO.getAverageByCareer(career.getId()));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                newDataCareer.setStudentCareer(studentCustoms.stream().filter(student -> student.getIdCareer() == career.getId()).count());
                newDataCareer.setStudentCareerMan(studentCustoms.stream().filter(student -> student.getIdCareer() == career.getId() && student.getGender().toUpperCase().equals("M")).count());
                newDataCareer.setStudentCareerWoman(newDataCareer.getStudentCareer() - newDataCareer.getStudentCareerMan());
                newDataCareer.setStudentActive(studentCourseRegisterCustoms.stream().filter(studentCourseRegisterCustom -> studentCourseRegisterCustom.getIdCareer() == career.getId()).count());
                newDataCareer.setStudentActiveMan(studentCourseRegisterCustoms.stream().filter(studentCourseRegisterCustom -> studentCourseRegisterCustom.getIdCareer() == career.getId()
                && studentCourseRegisterCustom.getGender().toUpperCase().equals("M")).count());
                newDataCareer.setStudentActiveWoman(newDataCareer.getStudentActive() - newDataCareer.getStudentActiveMan());

                List<String> modules = new ArrayList(Arrays.asList("0","1","2","3", "4", "5", "6"));
                Collections.sort(modules);
                Iterator iterator=modules.iterator();
                List<ModulesReport> modulesReports = new ArrayList<>();
                modules.stream().forEach(module ->{
                    String moduleSemester = String.valueOf(iterator.next());
                    ModulesReport modulesReport = new ModulesReport();
                    modulesReport.setStudentRegister(studentCourseRegisterCustoms.stream().filter(studentCourseRegisterCustom ->
                    studentCourseRegisterCustom.getModule().equals(moduleSemester)&& studentCourseRegisterCustom.getSemester().equals(semester)).count());
                    modulesReport.setModule(moduleSemester);
                    modulesReports.add(modulesReport);
                });
                newDataCareer.setModule(modulesReports);
                dataCareers.add(newDataCareer);
            });
            executiveReport.setDataCareers(dataCareers);

            if (executiveReport != null){
                executiveReport.setDataCareers(dataCareers);
                basicResponse.setCode(StatusCode.OK);
                basicResponse.setUniqueResult(executiveReport);
                basicResponse.setMessage("Report correctly execute");
            }
        }catch (Exception e){
            basicResponse.setCode(StatusCode.INTERNAL_SERVER_ERROR);
            basicResponse.setMessage(e.getMessage());
        }
        return basicResponse;
    }

    public BasicResponse getQuantityOfStudentByCareer(Long idCareer, String semester){
        BasicResponse basicResponse = new BasicResponse();
        try
        {
            List<StudentCustom> studentCustoms = executiveReportDAO.getStudentCustom();
            List<StudentCourseRegisterCustom> studentCourseRegisterCustoms = executiveReportDAO.getStudentCourseRegisterCustom(semester.trim());
            Career career = careerDao.getById(idCareer);
            DataCareer newDataCareer = new DataCareer();
            newDataCareer.setIdCareer(career.getId());
            newDataCareer.setSameCareer(career.getSameCareer());
            newDataCareer.setPlan(career.getPlan());
            newDataCareer.setNameCareer(career.getName());
            try {
                newDataCareer.setAverage(executiveReportDAO.getAverageByCareer(career.getId()));
            } catch (Exception e) {
                e.printStackTrace();
            }
            newDataCareer.setStudentCareer(studentCustoms.stream().filter(student -> student.getIdCareer() == career.getId()).count());
            newDataCareer.setStudentCareerMan(studentCustoms.stream().filter(student -> student.getIdCareer() == career.getId() && student.getGender().toUpperCase().equals("M")).count());
            newDataCareer.setStudentCareerWoman(newDataCareer.getStudentCareer()- newDataCareer.getStudentCareerMan());
            newDataCareer.setStudentActive(studentCourseRegisterCustoms.stream().filter(studentCourseRegisterCustom -> studentCourseRegisterCustom.getIdCareer() == career.getId()).count());
            newDataCareer.setStudentActiveMan(studentCourseRegisterCustoms.stream().filter(studentCourseRegisterCustom -> studentCourseRegisterCustom.getIdCareer() == career.getId()
            && studentCourseRegisterCustom.getGender().toUpperCase().equals("M")).count());
            newDataCareer.setStudentActiveWoman(newDataCareer.getStudentActive() - newDataCareer.getStudentActiveMan());

            List<String> modules = new ArrayList(Arrays.asList("0","1","2","3", "4", "5", "6"));
            Collections.sort(modules);
            Iterator iterator = modules.iterator();
            List<ModulesReport> modulesReports = new ArrayList<>();
            modules.stream().forEach(module ->{
                String moduleSemester = String.valueOf(iterator.next());
                ModulesReport modulesReport = new ModulesReport();
                modulesReport.setStudentRegister(studentCourseRegisterCustoms.stream().filter(studentCourseRegisterCustom ->
                studentCourseRegisterCustom.getModule().equals(moduleSemester) && studentCourseRegisterCustom.getSemester().equals(semester)).count());
                modulesReport.setModule(moduleSemester);
                modulesReports.add(modulesReport);
            });
            newDataCareer.setModule(modulesReports);
            if (newDataCareer != null)
            {
                basicResponse.setCode(StatusCode.OK);
                basicResponse.setUniqueResult(newDataCareer);
                basicResponse.setMessage("Report correctly execute");
            }
        }catch (Exception e){
            basicResponse.setCode(StatusCode.INTERNAL_SERVER_ERROR);
            basicResponse.setMessage(e.getMessage());
        }
        return basicResponse;
    }

}
