package com.utepsa.resources.executiveReport;

import com.codahale.metrics.annotation.Timed;
import com.google.inject.Inject;
import com.utepsa.api.response.BasicResponse;
import com.utepsa.core.StatusCode;
import io.dropwizard.hibernate.UnitOfWork;
import io.swagger.annotations.*;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

/**
 * Created by Luana Chavez on 17/04/2017.
 */
@Path(ExecutiveReportResource.SERVICE_PATH)
@Api(value = ExecutiveReportResource.SERVICE_PATH, description = "Operations about ExecutiveReport")
@Produces(MediaType.APPLICATION_JSON)
public class ExecutiveReportResource {
    public static final String SERVICE_PATH = "ExecutiveReport/";
    private final ExecutiveReportService service;

    @Inject
    public ExecutiveReportResource(ExecutiveReportService service) {
        this.service = service;
    }

    @GET
    @Timed
    @UnitOfWork
    @Path("/Career")
    @ApiOperation( value = "Get Report Students by Career")
    @ApiResponses(value = {
            @ApiResponse(code = StatusCode.OK, message = "Report correctly execute"),
            @ApiResponse(code = StatusCode.CONFLICT, message = "Conflict to execute report"),
            @ApiResponse(code = StatusCode.INTERNAL_SERVER_ERROR, message = "Internal server error")})
    public BasicResponse getQuantityOfStudent(@ApiParam(value = "semester needed to find the quantity student", required = true)
                                                  @QueryParam("semester")String semester) {
        return service.getQuantityOfStudent(semester);
    }

    @GET
    @Timed
    @UnitOfWork
    @Path("/Career/{idCareer}")
    @ApiOperation( value = "Get Report Students by Career")
    @ApiResponses(value = {
            @ApiResponse(code = StatusCode.OK, message = "Report correctly execute"),
            @ApiResponse(code = StatusCode.CONFLICT, message = "Conflict to execute report"),
            @ApiResponse(code = StatusCode.INTERNAL_SERVER_ERROR, message = "Internal server error")})
    public BasicResponse getQuantityOfStudentByCareer(@ApiParam(value = "Career ID needed to find the quantity student", required = true)
                                                          @PathParam("idCareer") long idCareer,
                                                      @ApiParam(value = "semester needed to find the quantity student", required = true)
                                                           @QueryParam("semester")String semester) {
        return service.getQuantityOfStudentByCareer(idCareer,semester);
    }

}
