package com.utepsa.resources.permissions;

import com.codahale.metrics.annotation.Timed;
import com.google.inject.Inject;
import com.utepsa.api.response.BasicResponse;
import com.utepsa.core.StatusCode;
import io.dropwizard.hibernate.UnitOfWork;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 * Created by Luana Chavez on 16/03/2017.
 */
@Path("/permission")
@Api(value = "Permission", description = "Operations about Permissions")
@Produces(MediaType.APPLICATION_JSON)
public class PermissionResource  {
    private final PermissionService permissionService;

    @Inject
    public PermissionResource(PermissionService permissionService) {
        this.permissionService = permissionService;
    }

    @GET
    @Timed
    @UnitOfWork
    @ApiOperation( value = "Get all Permissions", response = BasicResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = StatusCode.OK, message = "Permissions found"),
            @ApiResponse(code = StatusCode.NO_CONTENT, message = "There are no permissions to show"),
            @ApiResponse(code = StatusCode.INTERNAL_SERVER_ERROR, message = "Internal Server Error")})
    public BasicResponse getAll(){
        return permissionService.getAll();
    }
}
