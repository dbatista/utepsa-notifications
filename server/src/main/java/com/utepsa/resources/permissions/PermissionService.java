package com.utepsa.resources.permissions;

import com.google.inject.Inject;
import com.utepsa.api.response.BasicResponse;
import com.utepsa.core.Operations;
import com.utepsa.core.StatusCode;
import com.utepsa.db.permission.PermissionDAO;
import com.utepsa.models.Permission;

import java.util.List;

/**
 * Created by Luana Chavez on 16/03/2017.
 */
public class PermissionService {
    private final PermissionDAO permissionDAO;

    @Inject
    public PermissionService(PermissionDAO permissionDAO) {
        this.permissionDAO = permissionDAO;
    }

    public BasicResponse getAll()
    {
        BasicResponse basicResponse =new BasicResponse();
        try {
            List<Permission> permissions = permissionDAO.getAll();
            if(permissions.size() == 0){
                basicResponse.setCode(StatusCode.NO_CONTENT);
                basicResponse.setMessage("There are no permissions to show");
            }else{
                basicResponse.setCode(StatusCode.OK);
                basicResponse.setData(permissions);
            }
        }catch (Exception e){
            basicResponse = Operations.getResponseErrors(e);
        }
        return basicResponse;
    }
}
