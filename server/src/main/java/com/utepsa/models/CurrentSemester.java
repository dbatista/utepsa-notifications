package com.utepsa.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;

/**
 * Created by Luana Chavez on 24/02/2017.
 */
@Entity
@Table(name = "current_semester")
@org.hibernate.annotations.NamedQueries({
        @org.hibernate.annotations.NamedQuery(
                name = "com.utepsa.models.CurrentSemester.getCurrentSemester",
                query = "SELECT m FROM CurrentSemester m WHERE m.state = true"
        )
})
@ApiModel(value = "CurrentSemester entity", description = "Complete info of a entity currentSemester")
public class CurrentSemester {
    private long id;
    private String semester;
    private int module;
    private boolean state;

    public CurrentSemester() {
    }

    public CurrentSemester(long id, String semester, int module,boolean state) {
        this.id = id;
        this.semester = semester;
        this.module = module;
        this.state = state;
    }

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    @ApiModelProperty(value = "The id of the currentSemester in application", required = true)

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
    @Basic
    @Column(name = "semester", nullable = false)
    @ApiModelProperty(value = "The semester of the currentSemester in application", required = true)

    public String getSemester() {
        return semester;
    }

    public void setSemester(String semester) {
        this.semester = semester;
    }

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @Basic
    @Column(name = "state", nullable = false)
    @ApiModelProperty(value = "The state of the currentSemester in application", required = true)
    public boolean isState() {
        return state;
    }

    public void setState(boolean state) {
        this.state = state;
    }

    public int getModule() {
        return module;
    }

    public void setModule(int module) {
        this.module = module;
    }

    @Override
    public String toString() {
        return String.format("currentSemester{id=%d,semester=%s,module=%d,state=%s}",
                this.id,this.semester,this.module,this.state);
    }
}
