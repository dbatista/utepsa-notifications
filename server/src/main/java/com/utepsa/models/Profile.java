package com.utepsa.models;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Created by Gerardo on 02/02/2017.
 */
@Entity
@Table(name = "profile")
@ApiModel(value = "Profile entity", description = "Complete info of a entity profile")
public class Profile {
    private long id;
    private String name;
    private String fatherLastname;
    private String motherLastname;
    private String ocupation;
    private String phoneNumber;
    private String email;

    public Profile(){

    }

    public Profile(long id, String name, String fatherLastname, String motherLastname, String ocupation, String phoneNumber, String email){
        this.id = id;
        this.name = name;
        this.fatherLastname = fatherLastname;
        this.motherLastname = motherLastname;
        this.ocupation = ocupation;
        this.phoneNumber = phoneNumber;
        this.email = email;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    @ApiModelProperty(value = "The id of the Profile in application", required = true)
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "name", nullable = false)
    @ApiModelProperty(value = "The name of the profile in application", required = true)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "father_lastname", nullable = false)
    @ApiModelProperty(value = "The fatherLastname of the profile in application", required = true)
    public String getFatherLastname() {
        return fatherLastname;
    }

    public void setFatherLastname(String lastnamePaternal) {
        this.fatherLastname = lastnamePaternal;
    }

    @Basic
    @Column(name = "mother_lastname", nullable = false)
    @ApiModelProperty(value = "The motherLastname of the profile in application", required = true)
    public String getMotherLastname() {
        return motherLastname;
    }

    public void setMotherLastname(String lastnameMaternal) {
        this.motherLastname = lastnameMaternal;
    }

    @Basic
    @Column(name = "ocupation", nullable = false)
    @ApiModelProperty(value = "The ocupation of the profile in application")
    public String getOcupation() {
        return ocupation;
    }

    public void setOcupation(String ocupation) {
        this.ocupation = ocupation;
    }

    @Basic
    @Column(name = "phone_number", nullable = false)
    @ApiModelProperty(value = "The phone number of the profile in application")
    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String telephone) {
        this.phoneNumber = telephone;
    }

    @Basic
    @Column(name = "email", nullable = false)
    @ApiModelProperty(value = "The email of the profile in application")
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return String.format("Profile{id=%d,name=%s,fatherLastname=%s,motherLastname=%s,ocupation=%s,phoneNumber=%s,email=%s}",
                this.id,this.name,this.fatherLastname,this.motherLastname,this.ocupation,this.phoneNumber,this.email);
    }
}
