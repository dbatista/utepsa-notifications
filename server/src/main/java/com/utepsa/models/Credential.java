package com.utepsa.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.utepsa.api.serializers.CredentialSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.annotations.NamedQueries;
import org.hibernate.annotations.NamedQuery;
import javax.persistence.*;

/**
 * Created by David on 22/01/2017.
 */
@Entity
@Table(name = "credential")
@NamedQueries({
        @NamedQuery(
                name = "com.utepsa.models.Credential.login",
                query = "SELECT u FROM Credential u WHERE u.username = :username AND u.password = :password"
        ),
        @NamedQuery(
                name = "com.utepsa.models.Credential.getByIdStudent",
                query = "SELECT u FROM Credential u WHERE u.student = :idStudent"
        )
})
@JsonSerialize(using = CredentialSerializer.class)
@ApiModel(value = "Credential entity", description = "Complete info of a entity credential")
public class Credential {

    private long id;
    private String username;
    private String password;
    private String token;
    private String gcmId;
    private String lastConnection;
    private boolean state;
    private Role role;
    private Student student;
    private boolean changePasswordForced;

    public Credential() {
    }

    public Credential(long id, String username, String password, String token, String gcmId, String lastConnection, boolean state, Role role, Student student, boolean changePasswordForced) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.token = token;
        this.gcmId = gcmId;
        this.lastConnection = lastConnection;
        this.state = state;
        this.role = role;
        this.student = student;
        this.changePasswordForced = changePasswordForced;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    @ApiModelProperty(value = "The id of the credential in application", required = true)
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "username", nullable = false)
    @ApiModelProperty(value = "The username of the credential in application", required = true)
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Basic
    @Column(name = "password", nullable = false)
    @ApiModelProperty(value = "The password of the credential in application", required = true)
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Basic
    @Column(name = "token", nullable = false)
    @ApiModelProperty(value = "The token of the credential in application", required = true)
    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    @Basic
    @Column(name = "gcm_id", nullable = false)
    @ApiModelProperty(value = "The gcmId of the credential in application", required = true)
    public String getGcmId() {
        return gcmId;
    }

    public void setGcmId(String gcmId) {
        this.gcmId = gcmId;
    }

    @Basic
    @Column(name = "last_connection", nullable = false)
    @ApiModelProperty(value = "The lastConnection of the credential in application", required = true)
    public String getLastConnection() {
        return lastConnection;
    }

    public void setLastConnection(String lastConnection) {
        this.lastConnection = lastConnection;
    }

    @Basic
    @Column(name = "state", nullable = false)
    @ApiModelProperty(value = "The state of the credential in application", required = true)
    public boolean isState() {
        return state;
    }

    public void setState(boolean state) {
        this.state = state;
    }

    @ManyToOne
    @JoinColumn(name = "id_role", referencedColumnName = "id", nullable = false)
    @ApiModelProperty(value = "The role of the credential in application", required = true)
    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    @ManyToOne
    @JoinColumn(name = "id_student", referencedColumnName = "id", nullable = false)
    @ApiModelProperty(value = "The student of the credential in application", required = true)
    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    @JsonIgnore
    @Basic
    @Column(name = "change_password_forced", nullable = false)
    @ApiModelProperty(value = "The student of the credential in application", required = true)
    public boolean isChangePasswordForced() {
        return changePasswordForced;
    }

    public void setChangePasswordForced(boolean changePasswordForced) {
        this.changePasswordForced = changePasswordForced;
    }

    @Override
    public String toString() {
        return String.format("credential{id=%d,username=%d,password=%d,token=%s,gcmId=%s,lastConnection=%s,state=%s,role=%d}",
                this.id,this.username,this.password,this.token,this.gcmId,this.lastConnection,this.state,this.role);
    }
}
