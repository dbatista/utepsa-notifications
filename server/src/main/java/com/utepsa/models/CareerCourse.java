package com.utepsa.models;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.annotations.NamedQueries;
import org.hibernate.annotations.NamedQuery;


import javax.persistence.*;

/**
 * Created by David on 22/01/2017.
 */
@Entity
@Table(name = "career_course")
@NamedQueries({
        @NamedQuery(
                name = "com.utepsa.models.CareerCourse.getCoursesByCareer",
                query = "SELECT c FROM CareerCourse c WHERE id_career = :idCareer AND pensum = :pensum"
        ),
        @NamedQuery(
                name = "com.utepsa.models.CareerCourse.getCourseByCoursePensum",
                query = "SELECT c FROM CareerCourse c WHERE id_course = :idCourse AND id_career = :idCareer AND pensum = :pensum"
        )
})
@ApiModel(value = "career_course entity", description = "Complete info of a entity career_course")
public class CareerCourse {

    private long id;
    private Career career;
    private Course course;
    private int pensum;
    private int pensumLevel;
    private int pensumOrder;

    public CareerCourse() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    @ApiModelProperty(value = "The id of the CareerCourse in application", required = true)
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @ManyToOne
    @JoinColumn(name = "id_career", referencedColumnName = "id", nullable = false)
    @ApiModelProperty(value = "The idCareer of the Career in application", required = true)
    public Career getCareer() {
        return career;
    }

    public void setCareer(Career career) {
        this.career = career;
    }

    @ManyToOne
    @JoinColumn(name = "id_course", referencedColumnName = "id", nullable = false)
    @ApiModelProperty(value = "The idCourse of the Course in application", required = true)
    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    @Column(name = "pensum", nullable = false)
    @ApiModelProperty(value = "The pensum of the Career in Utepsa", required = true)
    public int getPensum() {
        return pensum;
    }

    public void setPensum(int pensum) {
        this.pensum = pensum;
    }

    @Basic
    @Column(name = "pensum_level", nullable = false)
    @ApiModelProperty(value = "The pensum level of the pensum in application")
    public int getPensumLevel() {
        return pensumLevel;
    }

    public void setPensumLevel(int pensumLevel) {
        this.pensumLevel = pensumLevel;
    }

    @Basic
    @Column(name = "pensum_order", nullable = false)
    @ApiModelProperty(value = "The pensum order of the pensum in application")
    public int getPensumOrder() {
        return pensumOrder;
    }

    public void setPensumOrder(int pensumOrder) {
        this.pensumOrder = pensumOrder;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CareerCourse that = (CareerCourse) o;

        if (id != that.id) return false;
        if (pensum != that.pensum) return false;
        if (pensumLevel != that.pensumLevel) return false;
        if (pensumOrder != that.pensumOrder) return false;
        if (career != null ? !career.equals(that.career) : that.career != null) return false;
        return course != null ? course.equals(that.course) : that.course == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (career != null ? career.hashCode() : 0);
        result = 31 * result + (course != null ? course.hashCode() : 0);
        result = 31 * result + pensum;
        result = 31 * result + pensumLevel;
        result = 31 * result + pensumOrder;
        return result;
    }
}
