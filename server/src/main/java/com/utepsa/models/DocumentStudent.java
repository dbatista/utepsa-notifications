package com.utepsa.models;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.utepsa.api.serializers.DocumentStudentSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by David on 22/01/2017.
 */
@Entity
@Table(name = "document_student")
@NamedQueries({
        @NamedQuery(
                name = "com.utepsa.models.DocumentStudent.getDocumentStudentById",
                query = "SELECT ds FROM DocumentStudent ds INNER JOIN ds.document d "+
                        "WHERE ds.idStudent = :idStudent"

        )
})
@JsonSerialize(using = DocumentStudentSerializer.class)
@ApiModel(value = "document_student entity", description = "Complete info of a entity document_student")
public class DocumentStudent {

    private long id;
    private long idStudent;
    private Document document;
    private Date dateDelivery;
    private Date dateReceipt;
    private String typePaper;
    private boolean state;

    public DocumentStudent() {
    }

    public DocumentStudent(long id, long idStudent, Document document, Date dateDelivery, Date dateReceipt, String typePaper, boolean state) {
        this.id = id;
        this.document = document;
        this.idStudent = idStudent;
        this.dateDelivery = dateDelivery;
        this.dateReceipt = dateReceipt;
        this.typePaper = typePaper;
        this.state = state;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    @ApiModelProperty(value = "The id of the Career in application", required = true)
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "id_student", nullable = false)
    @ApiModelProperty(value = "The idStudent of the document_student in application", required = true)
    public long getIdStudent() {
        return idStudent;
    }

    public void setIdStudent(long idStudent) {
        this.idStudent = idStudent;
    }

    @ManyToOne
    @JoinColumn(name = "id_document", referencedColumnName = "id", nullable = false)
    @ApiModelProperty(value = "The document of the document_student in application", required = true)
    public Document getDocument() {
        return document;
    }

    public void setDocument(Document document) {
        this.document = document;
    }

    @Basic
    @Column(name = "date_delivery", nullable = false)
    @ApiModelProperty(value = "The dateDelivery of the document_student in application", required = true)
    public Date getDateDelivery() {
        return dateDelivery;
    }

    public void setDateDelivery(Date dateDelivery) {
        this.dateDelivery = dateDelivery;
    }

    @Basic
    @Column(name = "date_receipt", nullable = false)
    @ApiModelProperty(value = "The dateReceipt of the document_student in application", required = true)
    public Date getDateReceipt() {
        return dateReceipt;
    }

    public void setDateReceipt(Date dateReceipt) {
        this.dateReceipt = dateReceipt;
    }

    @Basic
    @Column(name = "type_paper", nullable = false)
    @ApiModelProperty(value = "The typePaper of the document_student in application", required = true)
    public String getTypePaper() {
        return typePaper;
    }

    public void setTypePaper(String typePaper) {
        this.typePaper = typePaper;
    }

    @Basic
    @Column(name = "state", nullable = false)
    @ApiModelProperty(value = "The state of the document_student in application", required = true)
    public boolean isState() {
        return state;
    }

    public void setState(boolean state) {
        this.state = state;
    }

    @Override
    public String toString() {
        return String.format("documentStudent{id=%d,document=%d,idStudent=%d,dateDelivery=%s,dateReceipt=%s,typePaper=%s,state=%s}",
                this.id,this.document,this.idStudent,this.dateDelivery,this.dateReceipt,this.typePaper,this.state);
    }
}
