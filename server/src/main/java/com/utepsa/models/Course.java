package com.utepsa.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.annotations.Table;
import org.hibernate.annotations.NamedQueries;
import org.hibernate.annotations.NamedQuery;
import javax.persistence.*;

/**
 * Created by David on 22/01/2017.
 */
@Entity
@Table(appliesTo = "Course")
@NamedQueries({
        @NamedQuery(
                name = "com.utepsa.models.Course.getByCodeUtepsa",
                query = "SELECT c FROM Course c WHERE codeUtepsa = :codeUtepsa"
        )
})
@ApiModel(value = "course entity", description = "Complete info of a entity course")
public class Course {

    private long id;
    private String codeUtepsa;
    private String name;
    private String initials;
    private AcademicArea academicArea;

    public Course() {
    }

    public Course(long id) {
        this.id = id;
    }

    public Course(long id, String codeUtepsa, String name, String initials, AcademicArea academicArea) {
        this.id = id;
        this.codeUtepsa = codeUtepsa;
        this.name = name;
        this.initials = initials;
        this.academicArea = academicArea;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    @ApiModelProperty(value = "The id of the course in application", required = true)
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }


    @JsonIgnore
    @Basic
    @Column(name = "code_utepsa", length = 10)
    @ApiModelProperty(value = "The codeUtepsa of the course in application", required = true)
    public String getCodeUtepsa() {
        return codeUtepsa;
    }

    public void setCodeUtepsa(String codeUtepsa) {
        this.codeUtepsa = codeUtepsa;
    }

    @Basic
    @Column(name = "name", nullable = false, length = 255)
    @ApiModelProperty(value = "The name of the course in application", required = true)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "initials", nullable = false, length = 20)
    @ApiModelProperty(value = "The initials of the course in application", required = true)
    public String getInitials() {
        return initials;
    }

    public void setInitials(String initials) {
        this.initials = initials;
    }

    @ManyToOne
    @JoinColumn(name = "id_academic_area", referencedColumnName = "id", nullable = true)
    @ApiModelProperty(value = "The Academic Area of the Course in application", required = true)
    public AcademicArea getAcademicArea() {
        return academicArea;
    }

    public void setAcademicArea(AcademicArea academicArea) {
        this.academicArea = academicArea;
    }

    @Override
    public String toString() {
        return String.format("course{id=%d,codeUtepsa=%s,name=%s,initials=%s,academicArea=%d}",
                this.id,this.codeUtepsa,this.name,this.initials,this.academicArea);
    }
}
