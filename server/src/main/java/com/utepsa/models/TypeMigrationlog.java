package com.utepsa.models;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;

/**
 * Created by David on 29/01/2017.
 */
@Entity
@Table(name = "type_migrationlog")
@ApiModel(value = "Migrationlog entity", description = "Complete info of a entity Migrationlog")
public class TypeMigrationlog {
    private long id;
    private String name;

    public TypeMigrationlog() {
    }

    public TypeMigrationlog(long id) {
        this.id = id;
    }

    public TypeMigrationlog(long id, String name) {
        this.id = id;
        this.name = name;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    @ApiModelProperty(value = "The id of the type_file in application", required = true)
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "name", nullable = false)
    @ApiModelProperty(value = "The name of the type_file in application", required = true)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return String.format("typeFile{id=%d,name=%s}",
                this.id,this.name);
    }

}

