package com.utepsa.models;

import io.swagger.annotations.ApiModel;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by David on 29/01/2017.
 */
@Entity
@Table(name = "student_migrationlog")
@ApiModel(value = "StudentMigrationlog entity", description = "Complete info of a entity StudentMigrationlog")
public class StudentMigrationlog {
    private long id;
    private Student student;
    private TypeMigrationlog type;
    private String source;
    private Date dateExecuted;
    private String comments;
    private boolean state;

    public StudentMigrationlog() {
    }

    public StudentMigrationlog(long id) {
        this.id = id;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @ManyToOne
    @JoinColumn(name = "id_student", referencedColumnName = "id")
    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    @ManyToOne
    @JoinColumn(name = "id_type", referencedColumnName = "id", nullable = false)
    public TypeMigrationlog getType() {
        return type;
    }

    public void setType(TypeMigrationlog type) {
        this.type = type;
    }

    @Basic
    @Column(name = "source", nullable = false)
    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    @Column(name = "date_executed", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    public Date getDateExecuted() {
        return dateExecuted;
    }

    public void setDateExecuted(Date dateExecuted) {
        this.dateExecuted = dateExecuted;
    }

    @Basic
    @Column(name = "comments")
    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    @Basic
    @Column(name = "state", nullable = false)
    public boolean isState() {
        return state;
    }

    public void setState(boolean state) {
        this.state = state;
    }
}
