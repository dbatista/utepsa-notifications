package com.utepsa.models;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;

/**
 * Created by David on 22/01/2017.
 */
@Entity
@ApiModel(value = "faculty entity", description = "Complete info of a entity faculty")
public class Faculty {

    private long id;
    private String name;
    private String grade;
    private String abbreviation;

    public Faculty() {
    }

    public Faculty(long id) {
        this.id = id;
    }

    public Faculty(long id, String name) {
        this.id = id;
        this.name = name;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    @ApiModelProperty(value = "The id of the faculty in application", required = true)
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "name", nullable = false)
    @ApiModelProperty(value = "The name of the faculty in application", required = true)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "abbreviation", nullable = false)
    @ApiModelProperty(value = "The abbreviation of the faculty in application", required = true)
    public String getAbbreviation() {
        return abbreviation;
    }

    public void setAbbreviation(String abbreviation) {
        this.abbreviation = abbreviation;
    }

    @Basic
    @Column(name = "grade", nullable = false)
    @ApiModelProperty(value = "The grade of the faculty in application", required = true)
    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    @Override
    public String toString() {
        return String.format("faculty{id=%d,name=%s}",
                this.id,this.name);
    }
}
