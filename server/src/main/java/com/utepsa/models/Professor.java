package com.utepsa.models;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;

/**
 * Created by Luana Chavez on 24/02/2017.
 */
@Entity
@Table(name = "professor")
@org.hibernate.annotations.NamedQueries({
        @org.hibernate.annotations.NamedQuery(
                name = "com.utepsa.models.Professor.getProfessorByAgendCode",
                query = "SELECT d FROM Professor d WHERE d.agend_code = :agendCode"
        )
})
@ApiModel(value = "Professor entity", description = "Complete info of a entity professor")
public class Professor {
    private long id;
    private String agend_code;
    private String name;
    private String father_lastname;
    private String mother_lastname;
    private boolean state;

    public Professor() {
    }

    public Professor(long id,String agend_code, String name, boolean state, String father_lastname, String mother_lastname) {
        this.id = id;
        this.agend_code = agend_code;
        this.name = name;
        this.father_lastname = father_lastname;
        this.mother_lastname = mother_lastname;
        this.state=state;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    @ApiModelProperty(value = "The id of the professor in application", required = true)
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }


    @Basic
    @Column(name = "agend_code", nullable = false)
    @ApiModelProperty(value = "The agend_code of the professor in application", required = true)
    public String getAgend_code() {
        return agend_code;
    }

    public void setAgend_code(String agend_code) {
        this.agend_code = agend_code;
    }

    @Basic
    @Column(name = "name", nullable = false)
    @ApiModelProperty(value = "The name of the professor in application", required = true)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "father_lastname", nullable = false)
    @ApiModelProperty(value = "The father_lastname of the professor in application", required = true)
    public String getFather_lastname() {
        return father_lastname;
    }

    public void setFather_lastname(String father_lastname) {
        this.father_lastname = father_lastname;
    }

    @Basic
    @Column(name = "mother_lastname", nullable = false)
    @ApiModelProperty(value = "The mother_lastname of the professor in application", required = true)
    public String getMother_lastname() {
        return mother_lastname;
    }

    public void setMother_lastname(String mother_lastname) {
        this.mother_lastname = mother_lastname;
    }

    @Basic
    @Column(name = "state", nullable = false)
    @ApiModelProperty(value = "The state of the professor in application", required = true)
    public boolean isState() {
        return state;
    }

    public void setState(boolean state) {
        this.state = state;
    }

    @Override
    public String toString() {
        return String.format("professor{id=%d,agend_code=%s,name=%s,father_lastname=%s,mother_lastname=%s,state=%s}",
                this.id,this.agend_code,this.name,this.father_lastname,this.mother_lastname,this.state);
    }
}
