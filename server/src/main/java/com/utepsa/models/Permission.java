package com.utepsa.models;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.persistence.*;

/**
 * Created by Gerardo on 13/02/2017.
 */
@Entity
@Table(name = "permission")
@ApiModel(value = "Permission entity", description = "Complete info of a entity permission")
public class Permission {
    private long id;
    private String name;
    private String tag;

    public Permission (){

    }

    public Permission (long id, String name, String tag){
        this.id = id;
        this.name = name;
        this.tag = tag;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    @ApiModelProperty(value = "The id of the Permission in application", required = true)
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "name", nullable = false)
    @ApiModelProperty(value = "The name of the Permission in application", required = true)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "tag", nullable = false)
    @ApiModelProperty(value = "The tag of the Permission in application", required = true)
    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    @Override
    public String toString() {
        return String.format("Permission{id=%d,name=%s,tag=%s}",
                this.id,this.name,this.tag);
    }
}
