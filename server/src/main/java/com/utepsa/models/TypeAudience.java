package com.utepsa.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;

/**
 * Created by David on 22/01/2017.
 */
@Entity
@Table(name = "type_audience")
@ApiModel(value = "type_audience entity", description = "Complete info of a entity type_audience")
public class TypeAudience {

    private long id;
    private String name;

    public TypeAudience() {
    }

    public TypeAudience(long id, String name) {
        this.id = id;
        this.name = name;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    @ApiModelProperty(value = "The id of the type_audience in application", required = true)
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "name", nullable = false)
    @ApiModelProperty(value = "The name of the type_audience in application", required = true)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return String.format("typeAudience{id=%d,name=%s}",
                this.id,this.name);
    }
}
