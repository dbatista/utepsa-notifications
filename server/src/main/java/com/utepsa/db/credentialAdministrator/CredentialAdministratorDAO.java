package com.utepsa.db.credentialAdministrator;

import com.utepsa.models.CredentialAdministrator;
import java.util.List;

/**
 * Created by Gerardo on 12/02/2017.
 */
public interface CredentialAdministratorDAO {
    CredentialAdministrator login(String username, String password) throws Exception;
    long create( CredentialAdministrator credentialAdministrator)throws Exception;
    List<CredentialAdministrator> getAll() throws Exception;
    CredentialAdministrator getById (long id) throws Exception;
    CredentialAdministrator getByUsername(String username) throws Exception;
    CredentialAdministrator delete(long id) throws Exception;
    void registerLastConnection(CredentialAdministrator credentialAdministrator) throws Exception;
    void registerGcmId(long idCredentialAdministrator, String gcmId) throws Exception;
    List<String> getAllGcmId() throws Exception;
    void changePasswordForced(CredentialAdministrator credentialAdministrator, String newPassword) throws Exception;
}
