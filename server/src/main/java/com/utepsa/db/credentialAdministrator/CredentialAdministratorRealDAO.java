package com.utepsa.db.credentialAdministrator;

import com.google.inject.Inject;
import com.utepsa.models.CredentialAdministrator;
import io.dropwizard.hibernate.AbstractDAO;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

/**
 * Created by Gerardo on 12/02/2017.
 */
public class CredentialAdministratorRealDAO extends AbstractDAO<CredentialAdministrator> implements CredentialAdministratorDAO {

    @Inject
    public CredentialAdministratorRealDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    @Override
    public CredentialAdministrator login(String username, String password) throws Exception {
        return  uniqueResult(namedQuery("com.utepsa.models.credentialAdministrator.login").setParameter("username", username).setParameter("password", password));
    }

    @Override
    public long create(CredentialAdministrator credentialAdministrator) throws Exception{
        credentialAdministrator.setUsername(credentialAdministrator.getUsername().toUpperCase());
        final CredentialAdministrator created = persist(credentialAdministrator);
        if(created == null) throw new Exception("Admin credential was not created.");
        return created.getId();
    }

    @Override
    public List<CredentialAdministrator> getAll() throws Exception {
        return list(namedQuery("com.utepsa.models.credentialAdministrator.getAll"));
    }

    @Override
    public CredentialAdministrator delete(long id) throws Exception {
        CredentialAdministrator credentialAdministratorDelete = get(id);
        if(credentialAdministratorDelete.isState() == true) credentialAdministratorDelete.setState(false);
        persist(credentialAdministratorDelete);
        return credentialAdministratorDelete;
    }

    @Override
    public CredentialAdministrator getById (long id) throws Exception{
        return  uniqueResult(namedQuery("com.utepsa.models.credentialAdministrator.getById").setParameter("id", id));
    }

    @Override
    public CredentialAdministrator getByUsername(String username) throws Exception{
        return uniqueResult(namedQuery("com.utepsa.models.credentialAdministrator.getByUsername").setParameter("username", username));
    }

    @Override
    public void registerLastConnection(CredentialAdministrator credentialAdministrator) throws Exception {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());
        credentialAdministrator.setLastConnection(timeStamp);
        persist(credentialAdministrator);
    }

    @Override
    public void registerGcmId(long idCredentialAdministrator, String gcmId) throws Exception {
        CredentialAdministrator credentialAdministrator = get(idCredentialAdministrator);
        credentialAdministrator.setGcmId(gcmId);
        persist(credentialAdministrator);
    }

    @Override
    public List<String> getAllGcmId() throws Exception {
        Query query = currentSession().getNamedQuery("com.utepsa.models.ViewGcmStudent.getAll");
        return query.list();
    }

    @Override
    public void changePasswordForced(CredentialAdministrator credentialAdministrator, String newPassword) throws Exception{
        credentialAdministrator.setPassword(newPassword);
        credentialAdministrator.setChangePasswordForced(false);
        persist(credentialAdministrator);
    }
}
