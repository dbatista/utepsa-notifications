package com.utepsa.db.schedules;

import com.utepsa.models.Schedule;

/**
 * Created by Gerardo on 14/03/2017.
 */
public interface SchedulesDAO {

    long create(Schedule schedule) throws Exception;
    Schedule getByCodeUtepsa(String codeUtepsa) throws Exception;
}
