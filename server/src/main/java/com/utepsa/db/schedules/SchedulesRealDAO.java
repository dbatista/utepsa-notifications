package com.utepsa.db.schedules;

import com.google.inject.Inject;
import com.utepsa.models.Schedule;
import io.dropwizard.hibernate.AbstractDAO;
import org.hibernate.SessionFactory;

/**
 * Created by Gerardo on 14/03/2017.
 */
public class SchedulesRealDAO extends AbstractDAO<Schedule> implements SchedulesDAO {
    @Inject
    public SchedulesRealDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    @Override
    public long create(Schedule schedule) throws Exception {
        Schedule created = persist(schedule);
        if(created == null) throw new Exception("The schedules was not created.");
        return created.getId();
    }

    @Override
    public Schedule getByCodeUtepsa(String codeUtepsa) {
        return uniqueResult(namedQuery("com.utepsa.models.Schedule.getByCodeUtepsa").setParameter("codeUtepsa",codeUtepsa));
    }
}
