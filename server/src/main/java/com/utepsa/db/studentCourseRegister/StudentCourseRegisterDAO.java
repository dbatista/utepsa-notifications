package com.utepsa.db.studentCourseRegister;

import com.utepsa.models.StudentCourseRegister;

import java.util.List;

/**
 * Created by Luana Chavez on 24/02/2017.
 */
public interface StudentCourseRegisterDAO {

    long create(StudentCourseRegister studentCourseRegister) throws Exception;
    List<StudentCourseRegister> getCourseRegisterByStudent(long idStudent, String semester) throws Exception;
    List<StudentCourseRegister> getAllCourseRegisterByStudent(long idStudent, String semester) throws Exception;
    StudentCourseRegister getCourseRegisterById(long id) throws Exception;
    void deleteAllByStudentSemester(long idStudent, String semester) throws  Exception;
}
