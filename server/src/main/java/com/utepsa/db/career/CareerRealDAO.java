package com.utepsa.db.career;

import com.google.inject.Inject;
import com.utepsa.models.Career;
import io.dropwizard.hibernate.AbstractDAO;
import org.hibernate.SessionFactory;

import java.util.List;

/**
 * Created by David on 18/01/2017.
 */
public class CareerRealDAO extends AbstractDAO<Career> implements CareerDAO {
    @Inject
    public CareerRealDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    @Override
    public long create(Career career) throws Exception {
        final Career created = persist(career);
        if(created == null) throw new Exception("The career was not created.");
        return created.getId();
    }

    @Override
    public List<Career> getAll() throws Exception {
        List<Career> careers = currentSession().createSQLQuery("SELECT cc.id,cc.code_utepsa,cc.name,cc.current_pensum,cc.id_faculty,cc.plan,cc.same_career,cc.career_active " +
                "FROM career cc WHERE cc.career_active=TRUE").addEntity(Career.class).list();
        return careers;
    }

    @Override
    public List<Career> getByGrade(String grade) throws Exception {
        return list(namedQuery("com.utepsa.models.Career.getByGrade").setParameter("grade", grade));
    }

    @Override
    public Career getByCodeUtepsa(String codeUtepsa) throws Exception {
        return uniqueResult(namedQuery("com.utepsa.models.Career.getByCodeUtepsa").setParameter("codeUtepsa", codeUtepsa));
    }

    @Override
    public Career getById(long id) throws Exception {
        return get(id);
    }
}
