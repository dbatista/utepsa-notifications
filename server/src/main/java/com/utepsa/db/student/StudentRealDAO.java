package com.utepsa.db.student;

import com.google.inject.Inject;
import com.utepsa.models.Student;
import io.dropwizard.hibernate.AbstractDAO;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;

import java.util.List;

/**
 * Created by shigeo on 01-09-16.
 */
public class StudentRealDAO extends AbstractDAO<Student> implements StudentDAO {
    @Inject
    public StudentRealDAO (SessionFactory factory) {super(factory);}


    @Override
    public long create(Student student) throws Exception{
        return persist(student).getId();
    }

    @Override
    public List<Student> getAll() throws Exception {
        return currentSession().createCriteria(Student.class).list();
    }

    @Override
    public Student getById(long id) throws Exception {
        Student student = get(id);

        if(student == null) return null;
        student.setFirstSemester(this.getFirstSemesterByStudent(student.getId()));

        return student;
    }

    @Override
    public Student getByRegisterNumber(String registerNumber) throws Exception {
        Student student = (Student) currentSession().createCriteria(Student.class)
                .add(Restrictions.eq("registerNumber", registerNumber))
                .uniqueResult();

        if(student == null) return null;
        student.setFirstSemester(this.getFirstSemesterByStudent(student.getId()));
        return student;
    }

    @Override
    public List<Student> getByAgendCode(String agendCode) throws Exception {
        return list(namedQuery("com.utepsa.models.student.getByAgendCode").setParameter("agendCode", agendCode));
    }

    @Override
    public List getRegisterNumbersWhitoutHistoryNotes(int limit) throws Exception {
        return currentSession().createSQLQuery("SELECT s.register_number FROM student s Left Outer Join (SELECT sm.id_student FROM student_migrationlog sm WHERE sm.id_type = 2) sm  ON s.id = sm.id_student WHERE sm.id_student IS null LIMIT "+ limit).list();
    }

    @Override
    public List getRegisterNumbersWhitoutDocuments(int limit) throws Exception {
        return currentSession().createSQLQuery("SELECT s.register_number FROM student s Left Outer Join (SELECT sm.id_student FROM student_migrationlog sm WHERE sm.id_type = 3) sm  ON s.id = sm.id_student WHERE sm.id_student IS null LIMIT "+ limit).list();
    }

    @Override
    public List<Student> getActiveStudents(String semester) throws Exception {
        return currentSession().createSQLQuery(String.format("SELECT s.id, s.register_number, s.name, s.father_lastname, s.mother_lastname, s.gender, s.email1, s.email2, s.agend_code, s.birthday, s.phone_number1, phone_number2, s.document_type, s.document_number, s.pensum, s.id_career" +
                " FROM student s " +
                " INNER JOIN student_course_register scr ON scr.id_student = s.id WHERE scr.semester = '%s' GROUP BY s.id", semester)).addEntity(Student.class).list();
    }

    @Override
    public List<Student> searchStudentByCareerFullnameOrRegisterNumber(String query) throws Exception {
        query = query.replace(" ", "%");

        return   currentSession().createSQLQuery("SELECT s.id, s.register_number, s.name, s.father_lastname, s.mother_lastname, s.gender, s.email1, s.email2,\n" +
                " s.agend_code, s.birthday, s.phone_number1, phone_number2, s.document_type, s.document_number, s.pensum, s.id_career\n" +
                "FROM student AS s " +
                "INNER JOIN career c ON c.id = s.id_career "+
                "WHERE (s.register_number || s.name || s.father_lastname || s.mother_lastname || c.name) LIKE '%"+ query +"%' ORDER BY s.father_lastname ASC LIMIT 10").addEntity(Student.class).list();
    }

    @Override
    public String getFirstSemesterByStudent(long idStudent) {
        return (String) currentSession().createSQLQuery(String.format("select semester from history_note where id_student = %d ORDER BY semester, module LIMIT 1", idStudent)).uniqueResult();
    }
}
