package com.utepsa.db.permissionCredential;

import com.utepsa.models.PermissionCredential;

import java.util.List;

/**
 * Created by Gerardo on 13/02/2017.
 */
public interface PermissionCredentialDAO {
    long create(PermissionCredential permissionCredential)throws Exception;
    List<PermissionCredential> getByIdCredential (long idCredential) throws Exception;
    PermissionCredential getByIdCredentialPermission (long idCredential, long idPermission) throws Exception;
    void modify(List<PermissionCredential> permissionCredentials) throws Exception;
}
