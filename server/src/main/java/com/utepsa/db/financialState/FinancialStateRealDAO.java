package com.utepsa.db.financialState;

import com.google.inject.Inject;
import com.utepsa.models.FinancialState;
import com.utepsa.models.Student;
import io.dropwizard.hibernate.AbstractDAO;
import org.hibernate.SessionFactory;
import java.util.List;

/**
 * Created by Gerardo on 29/03/2017.
 */
public class FinancialStateRealDAO extends AbstractDAO<FinancialState> implements FinancialStateDAO{

    @Inject
    public FinancialStateRealDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    @Override
    public long create(FinancialState financialState) throws Exception{
        final FinancialState created = persist(financialState);
        if(created == null) throw new Exception("financial state was not created.");
        return created.getId();
    }

    @Override
    public FinancialState getByIdStudent(Student idStudent) throws Exception {
        return uniqueResult(namedQuery("com.utepsa.models.FinancialState.getByIdStudent").setParameter("idStudent", idStudent));
    }

    @Override
    public void updateFinancialState(FinancialState financialState) throws Exception{
        FinancialState financialStateUpdate = get(financialState.getId());
        persist(financialStateUpdate);
    }

    @Override
    public List<FinancialState> getAllFinancialStateWithFairValue()throws Exception{
        return currentSession().createSQLQuery("select * from financial_state As f WHERE f.expiration_date is not null").addEntity(FinancialState.class).list();
    }
}
