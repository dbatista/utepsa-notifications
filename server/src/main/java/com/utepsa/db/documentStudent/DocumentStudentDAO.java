package com.utepsa.db.documentStudent;

import com.utepsa.models.DocumentStudent;

import java.util.List;

/**
 * Created by shigeots on 31-10-16.
 */
public interface DocumentStudentDAO {
    boolean create (DocumentStudent documentsStudent) throws Exception;
    List<DocumentStudent> getByStudent(long idStudent) throws Exception;
}
