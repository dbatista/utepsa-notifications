package com.utepsa.db.Profile;

import com.google.inject.Inject;
import com.utepsa.models.Profile;
import io.dropwizard.hibernate.AbstractDAO;
import org.hibernate.SessionFactory;
import java.util.List;

/**
 * Created by Gerardo on 02/02/2017.
 */
public class ProfileRealDAO extends AbstractDAO<Profile> implements ProfileDAO {
    @Inject
    public ProfileRealDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    @Override
    public long create(Profile profile) throws Exception {
        final Profile created = persist(profile);
        if(created == null) throw new Exception("The Profile was not created.");
        return created.getId();
    }

    @Override
    public Profile getById(long id) throws Exception {
        return get(id);
    }

    @Override
    public List<Profile> getAll() throws Exception {
        return currentSession().createCriteria(Profile.class).list();
    }

    @Override
    public void modify(Profile profile) throws Exception {
        Profile profileModify = get(profile.getId());
        if (profile.getEmail() != null ) profileModify.setEmail(profile.getEmail());
        if (profile.getFatherLastname() != null) profileModify.setFatherLastname(profile.getFatherLastname());
        if (profile.getMotherLastname() != null) profileModify.setMotherLastname(profile.getMotherLastname());
        if (profile.getName() != null) profileModify.setName(profile.getName());
        if (profile.getOcupation() != null) profileModify.setOcupation(profile.getOcupation());
        if (profile.getPhoneNumber() != null) profileModify.setPhoneNumber(profile.getPhoneNumber());
        persist(profileModify);
    }
}
