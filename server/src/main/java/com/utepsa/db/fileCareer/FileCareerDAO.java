package com.utepsa.db.fileCareer;

import com.utepsa.models.FileCareer;
import java.util.List;

/**
 * Created by Leonardo on 15/05/2017.
 */
public interface FileCareerDAO {
    List<FileCareer> getAllFilesPerCareer(long idCareer) throws Exception;
}
