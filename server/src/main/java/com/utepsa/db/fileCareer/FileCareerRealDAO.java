package com.utepsa.db.fileCareer;
import com.google.inject.Inject;
import com.utepsa.models.FileCareer;
import io.dropwizard.hibernate.AbstractDAO;
import org.hibernate.SessionFactory;

import java.util.List;

/**
 * Created by Leonardo on 15/05/2017.
 */
public class FileCareerRealDAO extends AbstractDAO<FileCareer> implements FileCareerDAO {

    @Inject
    public FileCareerRealDAO (SessionFactory factory) {super(factory);}

    @Override
    public List<FileCareer> getAllFilesPerCareer(long idCareer) throws Exception {
        return list(namedQuery("com.utepsa.models.FileCareer.getAllFilesByIdCareer")
                .setParameter("idCareer", idCareer));
    }
}
