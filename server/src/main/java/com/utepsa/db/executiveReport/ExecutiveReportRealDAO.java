package com.utepsa.db.executiveReport;

import com.google.inject.Inject;
import com.utepsa.api.custom.ExecutiveReport;
import com.utepsa.api.custom.StudentCourseRegisterCustom;
import com.utepsa.api.custom.StudentCustom;
import io.dropwizard.hibernate.AbstractDAO;
import org.hibernate.SQLQuery;
import org.hibernate.SessionFactory;
import org.hibernate.transform.Transformers;
import org.hibernate.type.LongType;
import org.hibernate.type.StringType;

import java.text.DecimalFormat;
import java.util.List;

/**
 * Created by Luana Chavez on 17/04/2017.
 */
public class ExecutiveReportRealDAO extends AbstractDAO<ExecutiveReport> implements ExecutiveReportDAO {

    @Inject
    public ExecutiveReportRealDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    @Override
    public String getotalAverage() throws Exception {
        SQLQuery average =currentSession().createSQLQuery("SELECT AVG(t.average_student) " +
                "FROM (SELECT average_student FROM aveage_career) t");
        DecimalFormat df = new DecimalFormat("0.00");
        String  avarage = df.format(average.uniqueResult()).toString();
        return avarage;
    }

    @Override
    public String getAverageByCareer(Long idCareer) throws Exception {
        SQLQuery average = currentSession().createSQLQuery("SELECT average_student FROM aveage_career " +
                "WHERE id_career = "+idCareer+"");
        DecimalFormat df = new DecimalFormat("0.00");
        String  avarage = df.format(average.uniqueResult()).toString();
        return avarage;
    }

    @Override
    public List<StudentCustom> getStudentCustom() throws Exception {
        SQLQuery studentCustom = currentSession().createSQLQuery("SELECT s.id as idStudent, s.id_career as idCareer, s.gender as gender FROM student s " +
                "INNER JOIN career cc on cc.id = s.id_career " +
                "WHERE cc.career_active = TRUE");
        studentCustom.addScalar("idStudent", LongType.INSTANCE);
        studentCustom.addScalar("idCareer", LongType.INSTANCE);
        studentCustom.addScalar("gender", StringType.INSTANCE);

        studentCustom.setResultTransformer(Transformers.aliasToBean(StudentCustom.class));

        List<StudentCustom> studentCustoms = studentCustom.list();
        if (studentCustoms.size() < 1)
        {
            return null;
        }
        return studentCustoms;
    }

    @Override
    public List<StudentCourseRegisterCustom> getStudentCourseRegisterCustom(String semester) throws Exception {

        SQLQuery studentCourseRegisterCustom = currentSession().createSQLQuery("SELECT s.id as idStudent, cc.id as idCareer, s.gender as gender, ssc.semester as semester, ssc.module as module" +
                " FROM student_course_register ssc " +
                "INNER JOIN student s on s.id = ssc.id_student " +
                "INNER JOIN career cc on cc.id = s.id_career " +
                "INNER JOIN current_semester css on css.semester = ssc.semester " +
                "WHERE cc.career_active = TRUE AND ssc.semester = '"+semester+"' " +
                "GROUP BY s.id, cc.id, ssc.semester, ssc.module limit "+getStudentCustom().size()+"");
        studentCourseRegisterCustom.addScalar("idStudent", LongType.INSTANCE);
        studentCourseRegisterCustom.addScalar("idCareer", LongType.INSTANCE);
        studentCourseRegisterCustom.addScalar("gender", StringType.INSTANCE);
        studentCourseRegisterCustom.addScalar("semester", StringType.INSTANCE);
        studentCourseRegisterCustom.addScalar("module", StringType.INSTANCE);

        studentCourseRegisterCustom.setResultTransformer(Transformers.aliasToBean(StudentCourseRegisterCustom.class));

        List<StudentCourseRegisterCustom> studentCourseRegisterCustoms = studentCourseRegisterCustom.list();
        if (studentCourseRegisterCustoms.size() < 1)
        {
            return null;
        }
        return studentCourseRegisterCustoms;
    }


}
