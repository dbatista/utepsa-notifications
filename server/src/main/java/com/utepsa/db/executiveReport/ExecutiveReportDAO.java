package com.utepsa.db.executiveReport;

import com.utepsa.api.custom.StudentCourseRegisterCustom;
import com.utepsa.api.custom.StudentCustom;

import java.util.List;

/**
 * Created by Luana Chavez on 17/04/2017.
 */
public interface ExecutiveReportDAO {
    String getotalAverage() throws Exception;
    String getAverageByCareer(Long idCareer) throws Exception;
    List<StudentCustom> getStudentCustom() throws Exception;
    List<StudentCourseRegisterCustom> getStudentCourseRegisterCustom(String semester) throws Exception;
}
