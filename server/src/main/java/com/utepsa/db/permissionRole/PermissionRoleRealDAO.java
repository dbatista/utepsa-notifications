package com.utepsa.db.permissionRole;

import com.google.inject.Inject;
import com.utepsa.models.PermissionRole;
import io.dropwizard.hibernate.AbstractDAO;
import org.hibernate.SessionFactory;

import java.util.List;

/**
 * Created by Gerardo on 14/02/2017.
 */
public class PermissionRoleRealDAO extends AbstractDAO<PermissionRole> implements PermissionRoleDAO {
    @Inject
    public PermissionRoleRealDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    @Override
    public List<PermissionRole> getByIdRole (long idRole) throws Exception{
        return list(namedQuery("com.utepsa.models.PermissionRole.getByIdRole").setParameter("idRole", idRole));
    }

    @Override
    public PermissionRole getByRoleAndPermission (long idRole, long idPermission) throws Exception{
        return uniqueResult(namedQuery("com.utepsa.models.PermissionRole.getByRoleAndPermission").setParameter("idRole", idRole).setParameter("idPermission", idPermission));
    }

    @Override
    public void modify(List<PermissionRole> permissionRoles) throws Exception {
        for (PermissionRole permissionRole:permissionRoles) {
            PermissionRole permissionRoleModify =getByRoleAndPermission(permissionRole.getRole().getId(),permissionRole.getPermission().getId());
            permissionRoleModify.setState(permissionRole.isState());
            persist(permissionRoleModify);
        }
    }
}
