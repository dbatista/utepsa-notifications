package com.utepsa.db.permission;

import com.google.inject.Inject;
import com.utepsa.models.Permission;
import io.dropwizard.hibernate.AbstractDAO;
import org.hibernate.SessionFactory;
import java.util.List;

/**
 * Created by Gerardo on 15/02/2017.
 */
public class PermissionRealDAO extends AbstractDAO<Permission> implements PermissionDAO {
    @Inject
    public PermissionRealDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    @Override
    public List<Permission> getAll() throws Exception {
        return currentSession().createCriteria(Permission.class).list();
    }
}
