package com.utepsa.db.permission;

import com.utepsa.models.Permission;

import java.util.List;

/**
 * Created by Gerardo on 15/02/2017.
 */
public interface PermissionDAO{
    List<Permission> getAll() throws Exception;
}
