package com.utepsa.db.professor;


import com.google.inject.Inject;
import com.utepsa.models.Professor;
import io.dropwizard.hibernate.AbstractDAO;
import org.hibernate.SessionFactory;

/**
 * Created by Luana Chavez on 02/03/2017.
 */
public class ProfessorRealDAO extends AbstractDAO<Professor> implements ProfessorDAO {
    @Inject
    public ProfessorRealDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    @Override
    public Professor getProfessorByAgendCode(String agendCode) {
        return uniqueResult(namedQuery("com.utepsa.models.Professor.getProfessorByAgendCode").setParameter("agendCode",agendCode));
    }

    @Override
    public long create(Professor professor) throws Exception {
        final Professor created = persist(professor);
        if(created == null) throw new Exception("The professor was not created.");
        return created.getId();
    }
}
