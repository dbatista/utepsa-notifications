package com.utepsa.db.course;

import com.google.inject.Inject;
import com.utepsa.models.Course;
import io.dropwizard.hibernate.AbstractDAO;
import org.hibernate.SessionFactory;

/**
 * Created by David on 18/01/2017.
 */
public class CourseRealDAO extends AbstractDAO<Course> implements CourseDAO {
    @Inject
    public CourseRealDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    @Override
    public long create(Course course) throws Exception {
        final Course created = persist(course);
        if(created == null) throw new Exception("The course was not created.");
        return created.getId();
    }

    @Override
    public Course getById(long id) throws Exception {
        return get(id);
    }

    @Override
    public Course getByCodeUtepsa(String codeUtepsa) throws Exception {
        return uniqueResult(namedQuery("com.utepsa.models.Course.getByCodeUtepsa").setParameter("codeUtepsa", codeUtepsa));
    }
}
