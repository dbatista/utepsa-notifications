package com.utepsa.db.academicArea;

import com.utepsa.models.AcademicArea;

import java.util.List;

/**
 * Created by Gerardo on 24/04/2017.
 */
public interface AcademicAreaDAO {
    AcademicArea getById(long id) throws Exception;
    AcademicArea getByCode(String code) throws Exception;
    List<AcademicArea> getAll () throws Exception;
}
