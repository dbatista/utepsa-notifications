package com.utepsa.db.studentMigrationlog;

import com.google.inject.Inject;
import com.utepsa.models.StudentMigrationlog;
import io.dropwizard.hibernate.AbstractDAO;
import org.hibernate.SessionFactory;

/**
 * Created by David on 29/01/2017.
 */
public class StudentMigrationlogRealDAO extends AbstractDAO<StudentMigrationlog> implements StudentMigrationlogDAO {
    @Inject
    public StudentMigrationlogRealDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    @Override
    public long create(StudentMigrationlog studentMigrationlog) throws Exception{
        return persist(studentMigrationlog).getId();
    }
}
