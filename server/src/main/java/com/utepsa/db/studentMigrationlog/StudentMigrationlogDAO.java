package com.utepsa.db.studentMigrationlog;

import com.utepsa.models.StudentMigrationlog;

/**
 * Created by David on 29/01/2017.
 */
public interface StudentMigrationlogDAO {
    long create(StudentMigrationlog studentMigrationlog) throws Exception;
}
