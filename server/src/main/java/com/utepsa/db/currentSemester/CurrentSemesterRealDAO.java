package com.utepsa.db.currentSemester;

import com.google.inject.Inject;
import com.utepsa.models.CurrentSemester;
import io.dropwizard.hibernate.AbstractDAO;
import org.hibernate.SessionFactory;

/**
 * Created by Luana Chavez on 24/02/2017.
 */
public class CurrentSemesterRealDAO extends AbstractDAO<CurrentSemester> implements CurrentSemesterDAO {
    @Inject
    public CurrentSemesterRealDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    @Override
    public CurrentSemester getCurrentSemester() throws Exception {
        return uniqueResult(namedQuery("com.utepsa.models.CurrentSemester.getCurrentSemester").setMaxResults(1));
    }
}
