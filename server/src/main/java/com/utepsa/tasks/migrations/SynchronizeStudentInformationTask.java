package com.utepsa.tasks.migrations;

import com.google.common.collect.ImmutableMultimap;
import com.google.inject.Inject;
import com.utepsa.adapters.utepsa.documentsStudent.DocumentsStudentAdapter;
import com.utepsa.adapters.utepsa.documentsStudent.DocumentsStudentData;
import com.utepsa.adapters.utepsa.historyNotes.HistoryNotesAdapter;
import com.utepsa.adapters.utepsa.historyNotes.HistoryNotesData;
import com.utepsa.adapters.utepsa.professor.ProfessorAdapter;
import com.utepsa.adapters.utepsa.professor.ProfessorData;
import com.utepsa.adapters.utepsa.studentCourseRegister.StudentCourseRegisterAdapter;
import com.utepsa.adapters.utepsa.studentCourseRegister.StudentCourseRegisterData;
import com.utepsa.adapters.utepsa.students.StudentAdapter;
import com.utepsa.adapters.utepsa.students.StudentData;
import com.utepsa.db.course.CourseDAO;
import com.utepsa.db.document.DocumentDAO;
import com.utepsa.db.documentStudent.DocumentStudentDAO;
import com.utepsa.db.historyNote.HistoryNotesDAO;
import com.utepsa.db.professor.ProfessorDAO;
import com.utepsa.db.schedules.SchedulesDAO;
import com.utepsa.db.student.StudentDAO;
import com.utepsa.db.studentCourseRegister.StudentCourseRegisterDAO;
import com.utepsa.models.*;
import io.dropwizard.servlets.tasks.Task;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.context.internal.ManagedSessionContext;
import org.hibernate.resource.transaction.spi.TransactionStatus;

import java.io.PrintWriter;
import java.net.Socket;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by David-SW on 27/03/2017.
 */
public class SynchronizeStudentInformationTask extends Task {
    private SessionFactory sessionFactory;
    private StudentDAO studentDAO;
    private StudentAdapter studentAdapter;

    private CourseDAO courseDAO;
    private HistoryNotesDAO historyNotesDAO;
    private HistoryNotesAdapter historyNotesAdapter;

    private DocumentDAO documentDAO;
    private DocumentStudentDAO documentStudentDAO;
    private DocumentsStudentAdapter documentsStudentAdapter;

    private ProfessorDAO professorDAO;
    private SchedulesDAO schedulesDAO;
    private StudentCourseRegisterDAO studentCourseRegisterDAO;
    private StudentCourseRegisterAdapter studentCourseRegisterAdapter;
    private ProfessorAdapter professorAdapter;

    @Inject
    public SynchronizeStudentInformationTask(SessionFactory sessionFactory, StudentDAO studentDAO, HistoryNotesDAO historyNotesDAO, StudentAdapter studentAdapter, HistoryNotesAdapter historyNotesAdapter,
                                             DocumentsStudentAdapter documentsStudentAdapter, StudentCourseRegisterAdapter studentCourseRegisterAdapter, CourseDAO courseDAO, DocumentDAO documentDAO, DocumentStudentDAO documentStudentDAO,
                                             ProfessorAdapter professorAdapter, ProfessorDAO professorDAO, SchedulesDAO schedulesDAO, StudentCourseRegisterDAO studentCourseRegisterDAO) {
        super("SynchronizeStudentInformation");
        this.sessionFactory = sessionFactory;
        this.studentDAO = studentDAO;
        this.studentAdapter = studentAdapter;

        this.courseDAO = courseDAO;
        this.historyNotesDAO = historyNotesDAO;
        this.historyNotesAdapter = historyNotesAdapter;

        this.documentDAO = documentDAO;
        this.documentStudentDAO = documentStudentDAO;
        this.documentsStudentAdapter = documentsStudentAdapter;

        this.professorDAO = professorDAO;
        this.schedulesDAO = schedulesDAO;
        this.studentCourseRegisterDAO = studentCourseRegisterDAO;
        this.studentCourseRegisterAdapter = studentCourseRegisterAdapter;
        this.professorAdapter = professorAdapter;
    }

    @Override
    public void execute(ImmutableMultimap<String, String> immutableMultimap, PrintWriter printWriter) throws Exception {

        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        try {
            ManagedSessionContext.bind(session);

            if(immutableMultimap.get("semester").isEmpty()){
                printWriter.write("Semester not found \n");
                return;
            }
            String semester = immutableMultimap.get("semester").toArray()[0].toString();


            List<String> students = studentCourseRegisterAdapter.getActiveStudents(semester);
            if(students.isEmpty()){
                printWriter.write("No existen estudiantes activos en este semestre \n");
                return;
            }

            for(String registerStudent : students){
                transaction = session.beginTransaction();

                Student student = studentDAO.getByRegisterNumber(registerStudent);
                if(student == null) {
                    continue;
                }
                boolean synchronizedStudent = false;

                this.SyncStudent(student);

                do{
                    try{
                        if(isOnlineNet()){
                            this.SyncHistoryNotes(student);
                            synchronizedStudent = true;
                        }else{
                            Thread.sleep(5000);
                        }
                    }catch (Exception e){
                        Thread.sleep(5000);
                        printWriter.write(e.getMessage() + " \n");
                    }
                }while (!synchronizedStudent);

                synchronizedStudent = false;
                do{
                    try{
                        if(isOnlineNet()){
                            this.SyncDocumentsStudent(student);
                            synchronizedStudent = true;
                        }else{
                            Thread.sleep(5000);
                        }
                    }catch (Exception e){
                        Thread.sleep(5000);
                        printWriter.write(e.getMessage() + " \n");
                    }
                }while (!synchronizedStudent);

                synchronizedStudent = false;
                do{
                    try{
                        if(isOnlineNet()){
                            this.SyncStudentCourseRegisters(student, semester);
                            synchronizedStudent = true;
                        }else{
                            Thread.sleep(5000);
                        }
                    }catch (Exception e){
                        Thread.sleep(5000);
                        printWriter.write(e.getMessage() + " \n");
                    }
                }while (!synchronizedStudent);
                transaction.commit();
            }

        }catch (Exception e){
            if ( transaction.getStatus() == TransactionStatus.ACTIVE ) {
                transaction.rollback();
            }
        }finally {
            session.close();
            ManagedSessionContext.unbind(sessionFactory);
        }

    }

    public void SyncStudent(Student student) throws Exception{
        StudentData data = studentAdapter.getStudent(student.getRegisterNumber());

        student.setName(data.getAgd_nombres().trim());
        student.setFatherLastname(data.getAgd_appaterno().trim());
        student.setMotherLastname(data.getAgd_apmaterno().trim());
        student.setPhoneNumber1(data.getAgd_telf1().trim());
        student.setPhoneNumber2(data.getAgd_telf2().trim());
        student.setEmail1(data.getCorreo().trim());
    }

    public void SyncHistoryNotes(Student student) throws Exception {
        List<HistoryNotesData> historyNotesUtepsa = historyNotesAdapter.getAllHistoryNotesStudent(student.getRegisterNumber());
        List<HistoryNotes> historyNotesApp = historyNotesDAO.getByStudent(student.getId());

        if(historyNotesApp.size() == 0 && historyNotesUtepsa.size() > 0){
            for(HistoryNotesData data : historyNotesUtepsa){
                createHistoryNote(data, student.getId());
            }
            return;
        }

        if(historyNotesUtepsa.size() <= 0) return;

        for(HistoryNotesData data : historyNotesUtepsa){
            this.updateHistoryNote(data, historyNotesApp, student.getId());
        }
    }

    private void createHistoryNote(HistoryNotesData data,long idStudent) throws Exception {
        Course course = courseDAO.getByCodeUtepsa(data.getMat_codigo().trim());
        HistoryNotes historyNote = new HistoryNotes();
        historyNote.setIdStudent(idStudent);
        historyNote.setCourse(course);
        historyNote.setMinimunNote((int) data.getPln_notaminima());
        historyNote.setNote((int) data.getNot_nota());
        historyNote.setModule(data.getMdu_codigo().trim());
        historyNote.setSemester(data.getSem_codigo().trim());
        historyNotesDAO.create(historyNote);
    }

    private void updateHistoryNote(HistoryNotesData data, List<HistoryNotes> historyNotesApp , long idStudent) throws Exception {
        Course course = courseDAO.getByCodeUtepsa(data.getMat_codigo().trim());
        for(HistoryNotes historyNote: historyNotesApp){
            if(historyNote.getSemester().equals(data.getSem_codigo()) && historyNote.getModule().equals(data.getMdu_codigo()) && historyNote.getCourse().getId() == course.getId()){
                if(historyNote.getNote() != data.getNot_nota()){
                    historyNote.setNote((int) data.getNot_nota());
                }
                return;
            }
        }

        this.createHistoryNote(data, idStudent);
    }

    public void SyncDocumentsStudent(Student student) throws Exception{
        List<DocumentStudent> documentsStudentApp = documentStudentDAO.getByStudent(student.getId());
        List<DocumentsStudentData> documentsStudentUtepsa = documentsStudentAdapter.getAllDocumentsStudentForStudent(student.getRegisterNumber());

        if(documentsStudentApp.size() == 0 && documentsStudentUtepsa.size() > 0){
            for(DocumentsStudentData data: documentsStudentUtepsa){
                createDocumentStudent(data, student.getId());
            }
            return;
        }

        if(documentsStudentUtepsa.size() <= 0) return;

        for(DocumentsStudentData data: documentsStudentUtepsa){
            updateDocumentStudent(data, documentsStudentApp, student.getId());
        }

    }

    private void createDocumentStudent(DocumentsStudentData documentsStudentData, long idStudent) throws Exception {
        DocumentStudent documentStudent = new DocumentStudent();

        Document document = documentDAO.getByCodeUtepsa(documentsStudentData.getDoc_codigo().trim());
        documentStudent.setIdStudent(idStudent);
        documentStudent.setTypePaper(documentsStudentData.getAlmdoc_tipopapel().trim());
        documentStudent.setDocument(document);

        if(documentsStudentData.getESTADO().trim().contains("ENTREGADO")){
            documentStudent.setState(true);
        }else {
            documentStudent.setState(false);
        }
        if(documentsStudentData.getAlmdoc_fechaentregar() == null){
            documentStudent.setDateDelivery(null);
        }
        else{
            SimpleDateFormat formaterTimeZone = new SimpleDateFormat("yyyy-MM-dd");
            Date dateDelivery = formaterTimeZone.parse(documentsStudentData.getAlmdoc_fechaentregar().trim());
            documentStudent.setDateDelivery(dateDelivery);
        }

        if(documentsStudentData.getAlmdoc_fecharecepcion() == null){
            documentStudent.setDateReceipt(null);
        }
        else{
            SimpleDateFormat formaterTimeZone = new SimpleDateFormat("yyyy-MM-dd");
            Date dateReceipt = formaterTimeZone.parse(documentsStudentData.getAlmdoc_fecharecepcion().trim());
            documentStudent.setDateDelivery(dateReceipt);
        }

        documentStudentDAO.create(documentStudent);
    }

    private void updateDocumentStudent(DocumentsStudentData data, List<DocumentStudent> documentsStudentApp , long idStudent) throws Exception {
        Document document = documentDAO.getByCodeUtepsa(data.getDoc_codigo().trim());

        for(DocumentStudent documentStudent: documentsStudentApp){
            if(documentStudent.getDocument().getId() == document.getId()){
                if(!documentStudent.getTypePaper().equals(data.getAlmdoc_tipopapel().trim())){
                    documentStudent.setTypePaper(data.getAlmdoc_tipopapel().trim());
                }


                SimpleDateFormat formaterTimeZone = new SimpleDateFormat("yyyy-MM-dd");
                if(data.getAlmdoc_fechaentregar() != null){
                    Date dateDelivery = formaterTimeZone.parse(data.getAlmdoc_fechaentregar().trim());

                    if(documentStudent.getDateDelivery() != dateDelivery){
                        documentStudent.setDateDelivery(dateDelivery);
                    }
                }

                if(data.getAlmdoc_fecharecepcion() != null){
                    Date dateReceipt = formaterTimeZone.parse(data.getAlmdoc_fecharecepcion().trim());
                    if(documentStudent.getDateReceipt() != dateReceipt){
                        documentStudent.setDateReceipt(dateReceipt);
                    }
                }

                boolean state;
                if(data.getESTADO().trim().contains("ENTREGADO")){
                    state = true;
                }else{
                    state = false;
                }

                if(documentStudent.isState() != state){
                    documentStudent.setState(state);
                }
                return;
            }
        }

        this.createDocumentStudent(data, idStudent);
    }

    public void SyncStudentCourseRegisters(Student student, String semester) throws Exception{
        List<StudentCourseRegister> studentCoursesRegistersApp =  studentCourseRegisterDAO.getAllCourseRegisterByStudent(student.getId(), semester);
        List<StudentCourseRegisterData> studentCoursesRegistersUtepsa = studentCourseRegisterAdapter.getCourseRegisterByRegisterNumberAndSemester(student.getRegisterNumber(), semester);

        if(studentCoursesRegistersApp.size() == 0 && studentCoursesRegistersUtepsa.size() > 0){
            for(StudentCourseRegisterData data : studentCoursesRegistersUtepsa){
                createStudentCourseRegistered(data, student.getId());
            }
            return;
        }

        if(studentCoursesRegistersUtepsa.size() <= 0) return;

        for(StudentCourseRegisterData data: studentCoursesRegistersUtepsa){
            updateStudentCourseRegistered(data, studentCoursesRegistersApp);
        }
    }

    private void createStudentCourseRegistered(StudentCourseRegisterData data, long idStudent) throws Exception{
        StudentCourseRegister studentCourseRegister = new StudentCourseRegister();

        Course course = courseDAO.getByCodeUtepsa(data.getMat_codigo().trim());

        if(data.getAgd_docente() == null){
            studentCourseRegister.setProfessor(null);
        }else{
            Professor professor = professorDAO.getProfessorByAgendCode(data.getAgd_docente().trim());
            if(professor == null) {
                professor = registerProfessor(data.getAgd_docente().trim());
            }
            studentCourseRegister.setProfessor(professor);
        }

        if(data.getTur_codigo() == null){
            studentCourseRegister.setSchedule(null);
        }else{
            Schedule schedule = schedulesDAO.getByCodeUtepsa(data.getTur_codigo().trim());
            studentCourseRegister.setSchedule(schedule);
        }

        studentCourseRegister.setIdStudent(idStudent);
        studentCourseRegister.setCourse(course);
        studentCourseRegister.setClassroom(data.getAul_codigo().trim());
        studentCourseRegister.setGroup(data.getGrp_grupo().trim());
        studentCourseRegister.setModule(data.getMdu_codigo().trim());
        studentCourseRegister.setNumberEnrolled(data.getGrp_nroinscritos());
        studentCourseRegister.setState(data.getObservacion().trim());
        studentCourseRegister.setSemester(data.getSem_codigo().trim());
        studentCourseRegister.setNote(data.getNot_nota());

        studentCourseRegisterDAO.create(studentCourseRegister);
    }

    private void updateStudentCourseRegistered(StudentCourseRegisterData data, List<StudentCourseRegister> studentCoursesRegistersApp) throws Exception{
        for(StudentCourseRegister studentCourseRegister : studentCoursesRegistersApp){
            if(studentCourseRegister.getCodeUtepsa() == data.getGrp_detalle_id()){
                studentCourseRegister.setClassroom(data.getAul_codigo().trim());
                studentCourseRegister.setNumberEnrolled(data.getGrp_nroinscritos());
                studentCourseRegister.setState(data.getObservacion().trim());
                studentCourseRegister.setNote(data.getNot_nota());

                if(data.getAgd_docente() != null){
                    Professor professor = professorDAO.getProfessorByAgendCode(data.getAgd_docente().trim());
                    if(professor == null) {
                        professor = registerProfessor(data.getAgd_docente().trim());
                    }
                    if(studentCourseRegister.getProfessor() == null){
                        studentCourseRegister.setProfessor(professor);
                    }else{
                        if(studentCourseRegister.getProfessor().getId() != professor.getId()){
                            studentCourseRegister.setProfessor(professor);
                        }
                    }
                }

                if(data.getTur_codigo() != null){
                    Schedule schedule = schedulesDAO.getByCodeUtepsa(data.getTur_codigo().trim());
                    if(studentCourseRegister.getSchedule() != null){
                        if(studentCourseRegister.getSchedule().getId() != schedule.getId()){
                            studentCourseRegister.setSchedule(schedule);
                        }
                    }else{
                        studentCourseRegister.setSchedule(schedule);
                    }
                }
                break;
            }
        }
    }

    private Professor registerProfessor(String registerCode) throws Exception {
        ProfessorData professorData = professorAdapter.getProfessor(registerCode);

        if(professorData == null) return null;

        Professor newProfessor = new Professor();
        newProfessor.setAgend_code(professorData.getAgd_codigo());
        newProfessor.setName(professorData.getAgd_nombres());
        newProfessor.setFather_lastname(professorData.getAgd_appaterno());
        newProfessor.setMother_lastname(professorData.getAgd_apmaterno());
        newProfessor.setState(true);
        professorDAO.create(newProfessor);
        return newProfessor;
    }

    private boolean isOnlineNet(){
        try {
            Socket s = new Socket("www.google.com", 80);

            if(s.isConnected()){
                return true;
            }
            return false;
        }catch (Exception e){
            return false;
        }
    }
}
