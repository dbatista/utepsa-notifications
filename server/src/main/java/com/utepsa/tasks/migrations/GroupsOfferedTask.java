package com.utepsa.tasks.migrations;

import com.google.common.collect.ImmutableMultimap;
import com.google.inject.Inject;
import com.utepsa.adapters.utepsa.groupsOffered.GroupsOfferedAdapter;
import com.utepsa.adapters.utepsa.groupsOffered.GroupsOfferedData;
import com.utepsa.db.course.CourseDAO;
import com.utepsa.db.groupsOffered.GroupsOfferedDAO;
import com.utepsa.db.professor.ProfessorDAO;
import com.utepsa.db.schedules.SchedulesDAO;
import com.utepsa.models.Course;
import com.utepsa.models.GroupsOffered;
import com.utepsa.models.Professor;
import com.utepsa.models.Schedule;
import io.dropwizard.servlets.tasks.Task;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.context.internal.ManagedSessionContext;
import java.io.PrintWriter;
import java.util.List;

/**
 * Created by Gerardo on 25/04/2017.
 */
public class GroupsOfferedTask extends Task {

    private SessionFactory sessionFactory;
    private GroupsOfferedAdapter groupsOfferedAdapter;
    private GroupsOfferedDAO groupsOfferedDAO;
    private CourseDAO courseDAO;
    private ProfessorDAO professorDAO;
    private SchedulesDAO schedulesDAO;

    @Inject
    public GroupsOfferedTask(SessionFactory sessionFactory,GroupsOfferedAdapter groupsOfferedAdapter, GroupsOfferedDAO groupsOfferedDAO, CourseDAO courseDAO, ProfessorDAO professorDAO, SchedulesDAO schedulesDAO) {
        super("migration_groups_offered");
        this.sessionFactory = sessionFactory;
        this.groupsOfferedAdapter = groupsOfferedAdapter;
        this.groupsOfferedDAO = groupsOfferedDAO;
        this.courseDAO = courseDAO;
        this.professorDAO = professorDAO;
        this.schedulesDAO = schedulesDAO;
    }

    @Override
    public void execute(ImmutableMultimap<String, String> immutableMultimap, PrintWriter printWriter) throws Exception {
        Session session = sessionFactory.openSession();
        try {
            ManagedSessionContext.bind(session);
            String semester = immutableMultimap.get("semester").toArray()[0].toString();
            Transaction transaction = session.beginTransaction();
            try {
                getAllGroupsOffered(semester);
                transaction.commit();
            }
            catch (Exception e) {
                transaction.rollback();
                throw new RuntimeException(e);
            }
        } finally {
            session.close();
            ManagedSessionContext.unbind(sessionFactory);
        }
    }

    private void getAllGroupsOffered(String semester) throws Exception {
        List<GroupsOfferedData> groupsOfferedDatas = groupsOfferedAdapter.getAllGroupsOffered(semester);
        for (GroupsOfferedData groupsOfferedData: groupsOfferedDatas) {
            Course course = courseDAO.getByCodeUtepsa(groupsOfferedData.getMat_codigo().trim());
            if(course != null){
                GroupsOffered groupsOffered = groupsOfferedDAO.getBySemesterCourseGroupAndModule(groupsOfferedData.getSemestre().trim(), course, groupsOfferedData.getGrupo().trim(), groupsOfferedData.getModulo().trim());
                if(groupsOffered == null){
                    GroupsOffered newGroupsOffered = new GroupsOffered();
                    newGroupsOffered.setCourse(course);
                    Schedule schedule = schedulesDAO.getByCodeUtepsa(groupsOfferedData.getTur_codigo().trim());
                    newGroupsOffered.setSchedule(schedule);
                    if(groupsOfferedData.getDocente().trim().isEmpty() || groupsOfferedData.getDocente().trim() == null){
                        newGroupsOffered.setProfessor(null);
                    } else {
                        Professor professor = professorDAO.getProfessorByAgendCode(groupsOfferedData.getDocente().trim());
                        newGroupsOffered.setProfessor(professor);
                    }
                    newGroupsOffered.setSemester(groupsOfferedData.getSemestre().trim());
                    newGroupsOffered.setModule(groupsOfferedData.getModulo().trim());
                    newGroupsOffered.setGroup(groupsOfferedData.getGrupo().trim());
                    newGroupsOffered.setNumberEnrolled(groupsOfferedData.getGrp_nroinscritos());
                    newGroupsOffered.setClassroom(groupsOfferedData.getAul_codigo().trim());
                    groupsOfferedDAO.create(newGroupsOffered);
                } else {
                    groupsOffered.setCourse(course);
                    Schedule schedule = schedulesDAO.getByCodeUtepsa(groupsOfferedData.getTur_codigo().trim());
                    groupsOffered.setSchedule(schedule);
                    if(groupsOfferedData.getDocente().trim().isEmpty() || groupsOfferedData.getDocente().trim() == null){
                        groupsOffered.setProfessor(null);
                    } else {
                        Professor professor = professorDAO.getProfessorByAgendCode(groupsOfferedData.getDocente().trim());
                        groupsOffered.setProfessor(professor);
                    }
                    groupsOffered.setSemester(groupsOfferedData.getSemestre().trim());
                    groupsOffered.setModule(groupsOfferedData.getModulo().trim());
                    groupsOffered.setGroup(groupsOfferedData.getGrupo().trim());
                    groupsOffered.setNumberEnrolled(groupsOfferedData.getGrp_nroinscritos());
                    groupsOffered.setClassroom(groupsOfferedData.getAul_codigo().trim());
                    groupsOfferedDAO.update(groupsOffered);
                }
            } else {
                throw new Exception("Course doesn't exist");
            }

        }
    }
}
