package com.utepsa.tasks.migrations;

import com.google.common.collect.ImmutableMultimap;
import com.google.inject.Inject;
import com.utepsa.adapters.utepsa.schedules.SchedulesAdapter;
import com.utepsa.adapters.utepsa.schedules.SchedulesDATA;
import com.utepsa.db.schedules.SchedulesDAO;
import com.utepsa.models.Schedule;
import io.dropwizard.servlets.tasks.Task;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.context.internal.ManagedSessionContext;
import java.io.PrintWriter;
import java.util.List;

/**
 * Created by Gerardo on 14/03/2017.
 */
public class SchedulesTask extends Task {

    private SessionFactory sessionFactory;
    private SchedulesAdapter schedulesAdapter;
    private SchedulesDAO schedulesDAO;

    @Inject
    public SchedulesTask(SessionFactory sessionFactory, SchedulesAdapter schedulesAdapter,SchedulesDAO schedulesDAO) {
        super("migration_schedule");
        this.sessionFactory = sessionFactory;
        this.schedulesAdapter = schedulesAdapter;
        this.schedulesDAO = schedulesDAO;
    }

    @Override
    public void execute(ImmutableMultimap<String, String> immutableMultimap, PrintWriter printWriter) throws Exception {
        Session session = sessionFactory.openSession();
        try {
            ManagedSessionContext.bind(session);
            Transaction transaction = session.beginTransaction();
            try {
                registerSchudels();
                transaction.commit();
            }
            catch (Exception e) {
                transaction.rollback();
                throw new RuntimeException(e);
            }
        } finally {
            session.close();
            ManagedSessionContext.unbind(sessionFactory);
        }
    }

    public void registerSchudels() throws Exception {
        List<SchedulesDATA> schedulesUtepsa = schedulesAdapter.getAllSchudels();
        if (schedulesUtepsa != null){
            for(SchedulesDATA scheduleUtepsa : schedulesUtepsa) {
                if(schedulesDAO.getByCodeUtepsa(scheduleUtepsa.getTur_codigo()) == null){
                    Schedule schedules = new Schedule();
                    schedules.setCodeUtepsa(scheduleUtepsa.getTur_codigo().trim());
                    schedules.setDescription(scheduleUtepsa.getTur_descripcion().trim());
                    schedules.setSchedule(scheduleUtepsa.getTur_horario().trim());
                    schedulesDAO.create(schedules);
                }
            }
        }
    }
}
