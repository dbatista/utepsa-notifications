package com.utepsa.tasks.migrations;

import com.google.common.collect.ImmutableMultimap;
import com.google.inject.Inject;
import com.utepsa.adapters.utepsa.financialState.FinancialStateAdapter;
import com.utepsa.adapters.utepsa.financialState.FinancialStateData;
import com.utepsa.db.financialState.FinancialStateDAO;
import com.utepsa.db.student.StudentDAO;
import com.utepsa.models.FinancialState;
import com.utepsa.models.Student;
import io.dropwizard.servlets.tasks.Task;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.context.internal.ManagedSessionContext;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by Gerardo on 29/03/2017.
 */
public class FinancialStateTask extends Task {

    private SessionFactory sessionFactory;
    private FinancialStateAdapter financialStateAdapter;
    private FinancialStateDAO financialStateDAO;
    private StudentDAO studentDAO;

    @Inject
    public FinancialStateTask(SessionFactory sessionFactory,FinancialStateAdapter financialStateAdapter, FinancialStateDAO financialStateDAO, StudentDAO studentDAO) {
        super("migration_financial_state");
        this.sessionFactory = sessionFactory;
        this.financialStateAdapter = financialStateAdapter;
        this.financialStateDAO = financialStateDAO;
        this.studentDAO = studentDAO;

    }

    @Override
    public void execute(ImmutableMultimap<String, String> immutableMultimap, PrintWriter printWriter) throws Exception {
        Session session = sessionFactory.openSession();
        try {
            ManagedSessionContext.bind(session);
            Transaction transaction = session.beginTransaction();
            try {
                registerFinancialState();
                transaction.commit();
            }
            catch (Exception e) {
                transaction.rollback();
                throw new RuntimeException(e);
            }
        } finally {
            session.close();
            ManagedSessionContext.unbind(sessionFactory);
        }
    }

    public void registerFinancialState() throws Exception{
        List<FinancialStateData> financialStateUtepsa = financialStateAdapter.getAllFinancialState();
        if (financialStateUtepsa.size() > 0){
            for(FinancialStateData financialStates : financialStateUtepsa) {
                List<Student> students = studentDAO.getByAgendCode(financialStates.getAgd_codigo().trim());
                if(students != null){
                    for (Student student: students) {
                        FinancialState financialState = financialStateDAO.getByIdStudent(student);
                        if(financialState == null){
                            FinancialState newFinancialState = new FinancialState();
                            newFinancialState.setIdStudent(student);
                            newFinancialState.setDebitBalance(financialStates.getSaldoDeudor());
                            newFinancialState.setDelinquentBalance(financialStates.getSaldoenMora());
                            newFinancialState.setOutstandingBalance(financialStates.getSaldoaVencer());
                            if(financialStates.getFechaAVencer() == null){
                                newFinancialState.setExpirationDate(null);
                            } else {
                                SimpleDateFormat formaterTimeZone = new SimpleDateFormat("yyyy-MM-dd");
                                Date dateDelivery = formaterTimeZone.parse(financialStates.getFechaAVencer().trim());
                                newFinancialState.setExpirationDate(dateDelivery);
                            }
                            financialStateDAO.create(newFinancialState);
                        } else if(financialState != null) {
                            financialState.setIdStudent(student);
                            financialState.setDebitBalance(financialStates.getSaldoDeudor());
                            financialState.setDelinquentBalance(financialStates.getSaldoenMora());
                            financialState.setOutstandingBalance(financialStates.getSaldoaVencer());
                            if(financialStates.getFechaAVencer() == null){
                                financialState.setExpirationDate(null);
                            } else {
                                SimpleDateFormat formaterTimeZone = new SimpleDateFormat("yyyy-MM-dd");
                                Date dateDelivery = formaterTimeZone.parse(financialStates.getFechaAVencer().trim());
                                financialState.setExpirationDate(dateDelivery);
                            }
                            financialStateDAO.updateFinancialState(financialState);
                        }
                    }
                }
            }
        }
    }
}
