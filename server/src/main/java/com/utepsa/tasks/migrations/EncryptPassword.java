package com.utepsa.tasks.migrations;

import com.google.common.collect.ImmutableMultimap;
import com.google.inject.Inject;
import com.utepsa.core.Operations;
import com.utepsa.db.credential.CredentialDAO;
import com.utepsa.models.Credential;
import io.dropwizard.servlets.tasks.Task;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.context.internal.ManagedSessionContext;

import java.io.PrintWriter;
import java.util.List;

/**
 * Created by David-SW on 19/05/2017.
 */
public class EncryptPassword  extends Task {
    private CredentialDAO credentialDAO;
    private SessionFactory sessionFactory;

    @Inject
    public EncryptPassword(SessionFactory sessionFactory, CredentialDAO credentialDAO) {
        super("encrypt_password");
        this.credentialDAO = credentialDAO;
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void execute(ImmutableMultimap<String, String> immutableMultimap, PrintWriter printWriter) throws Exception {

        Session session = sessionFactory.openSession();
        try {
            ManagedSessionContext.bind(session);
            Transaction transaction = session.beginTransaction();
            try {
                List<Credential> credentials =  credentialDAO.getAllCredentials();
                credentials.stream().forEach( credential -> {
                    if(credential.getPassword().trim().isEmpty()){
                        String newPassword = getPasswordRegisterCode(credential.getUsername());
                        credential.setPassword(Operations.encryptPassword(newPassword));
                        credential.setChangePasswordForced(true);
                    }else{
                        credential.setPassword(Operations.encryptPassword(credential.getPassword()));
                    }
                });
                transaction.commit();
            }
            catch (Exception e) {
                transaction.rollback();
                throw new RuntimeException(e);
            }
        } finally {
            session.close();
            ManagedSessionContext.unbind(sessionFactory);
        }
    }

    private String getPasswordRegisterCode(String registerCode){
        String newPassword = "";
        for (int i = 0; i < registerCode.length(); i++){
            if(registerCode.charAt(i) != '0'){
                newPassword = registerCode.substring(i, registerCode.length());
                return newPassword;
            }
        }
        return "";
    }
}
