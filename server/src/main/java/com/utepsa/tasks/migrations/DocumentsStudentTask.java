package com.utepsa.tasks.migrations;

import com.google.common.collect.ImmutableMultimap;
import com.google.inject.Inject;
import com.utepsa.adapters.utepsa.documentsStudent.DocumentsStudentAdapter;
import com.utepsa.adapters.utepsa.documentsStudent.DocumentsStudentData;
import com.utepsa.core.Migrationlog;
import com.utepsa.db.document.DocumentDAO;
import com.utepsa.db.documentStudent.DocumentStudentDAO;
import com.utepsa.db.student.StudentDAO;
import com.utepsa.db.studentMigrationlog.StudentMigrationlogDAO;
import com.utepsa.models.*;
import io.dropwizard.servlets.tasks.Task;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.context.internal.ManagedSessionContext;
import org.hibernate.resource.transaction.spi.TransactionStatus;

import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by David on 05/12/2016.
 */
public class DocumentsStudentTask extends Task {
    private SessionFactory sessionFactory;
    private StudentMigrationlogDAO studentMigrationlogDAO;
    private DocumentStudentDAO documentStudentDAO;
    private StudentDAO studentDAO;
    private DocumentDAO documentDAO;
    private DocumentsStudentAdapter documentsStudentAdapter;

    @Inject
    public DocumentsStudentTask(SessionFactory sessionFactory,DocumentsStudentAdapter documentsStudentAdapter, StudentMigrationlogDAO studentMigrationlogDAO, DocumentStudentDAO documentStudentDAO, DocumentDAO documentDAO,StudentDAO studentDAO) {
        super("migration_documentsstudent");
        this.studentMigrationlogDAO = studentMigrationlogDAO;
        this.documentStudentDAO = documentStudentDAO;
        this.studentDAO = studentDAO;
        this.documentDAO = documentDAO;
        this.documentsStudentAdapter = documentsStudentAdapter;
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void execute(ImmutableMultimap<String, String> immutableMultimap, PrintWriter printWriter) throws Exception {
        Session session = sessionFactory.openSession();
        try {
            ManagedSessionContext.bind(session);
            Transaction transaction = session.beginTransaction();
            try {
                List<String> students = studentDAO.getRegisterNumbersWhitoutDocuments(100);
                while (students.size() > 0){
                    for(String registerCode : students){
                        this.getAndRegisterDocumentsStudentByStudent(registerCode);
                    }
                    try {
                        transaction.commit();
                    }
                    catch (Exception e) {
                        if ( transaction.getStatus() == TransactionStatus.ACTIVE ) {
                            transaction.rollback();
                        }
                    }
                    students = studentDAO.getRegisterNumbersWhitoutDocuments(100);
                }
            }
            catch (Exception e) {
                transaction.rollback();
                throw new RuntimeException(e);
            }
        } finally {
            session.close();
            ManagedSessionContext.unbind(sessionFactory);
        }
    }

    public void getAndRegisterDocumentsStudentByStudent(String registerCode) throws Exception {
        List<DocumentsStudentData> listDocumentsStudentsData = documentsStudentAdapter.getAllDocumentsStudentForStudent(registerCode);
        Student student = studentDAO.getByRegisterNumber(registerCode);
        if(listDocumentsStudentsData != null){
            if(listDocumentsStudentsData.size() == 0){
                registerStudentLog(student.getId(), "No documents", true);
                return;
            }

            for(DocumentsStudentData documentsStudentData : listDocumentsStudentsData ){
                this.createDocumentsStudent(documentsStudentData, student.getId());
            }
            registerStudentLog(student.getId(), null, true);
        }else{
            registerStudentLog(student.getId(), "No documents", true);
            return;
        }

    }

    private void createDocumentsStudent(DocumentsStudentData documentsStudentData, long idStudent) throws Exception {
        DocumentStudent documentStudent = new DocumentStudent();

        Document document = documentDAO.getByCodeUtepsa(documentsStudentData.getDoc_codigo().trim());
        documentStudent.setIdStudent(idStudent);
        documentStudent.setTypePaper(documentsStudentData.getAlmdoc_tipopapel().trim());
        documentStudent.setDocument(document);

        if(documentsStudentData.getESTADO().trim().contains("ENTREGADO")){
            documentStudent.setState(true);
        }else {
            documentStudent.setState(false);
        }
        if(documentsStudentData.getAlmdoc_fechaentregar() == null){
            documentStudent.setDateDelivery(null);
        }
        else{
            SimpleDateFormat formaterTimeZone = new SimpleDateFormat("yyyy-MM-dd");
            Date dateDelivery = formaterTimeZone.parse(documentsStudentData.getAlmdoc_fechaentregar().trim());
            documentStudent.setDateDelivery(dateDelivery);
        }

        if(documentsStudentData.getAlmdoc_fecharecepcion() == null){
            documentStudent.setDateReceipt(null);
        }
        else{
            SimpleDateFormat formaterTimeZone = new SimpleDateFormat("yyyy-MM-dd");
            Date dateReceipt = formaterTimeZone.parse(documentsStudentData.getAlmdoc_fecharecepcion().trim());
            documentStudent.setDateDelivery(dateReceipt);
        }

        documentStudentDAO.create(documentStudent);
    }

    public void registerStudentLog(long idStudent, String comments, boolean state) throws Exception {
        StudentMigrationlog studentMigrationlog = new StudentMigrationlog();
        studentMigrationlog.setStudent(new Student(idStudent));
        studentMigrationlog.setType(new TypeMigrationlog(Migrationlog.MIGRATION_STUDENT_DOCUMENTS));
        studentMigrationlog.setSource("TASK");
        Date date = new Date();
        studentMigrationlog.setDateExecuted(date);
        studentMigrationlog.setComments(comments);
        studentMigrationlog.setState(state);

        studentMigrationlogDAO.create(studentMigrationlog);
    }
}
