package com.utepsa.tasks.migrations;

import com.google.common.collect.ImmutableMultimap;
import com.google.inject.Inject;
import com.utepsa.adapters.utepsa.careers.CareersAdapter;
import com.utepsa.adapters.utepsa.careers.CareersDATA;
import com.utepsa.db.career.CareerDAO;
import com.utepsa.models.Career;
import com.utepsa.models.Faculty;
import io.dropwizard.servlets.tasks.Task;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.context.internal.ManagedSessionContext;

import java.io.PrintWriter;
import java.util.List;

/**
 * Created by David on 28/01/2017.
 */
public class CareersTask extends Task {
    private SessionFactory sessionFactory;
    private CareersAdapter careersAdapter;
    private CareerDAO careerDAO;

    @Inject
    public CareersTask(SessionFactory sessionFactory, CareersAdapter careersAdapter, CareerDAO careerDAO) {
        super("migration_careers");
        this.sessionFactory = sessionFactory;
        this.careersAdapter = careersAdapter;
        this.careerDAO = careerDAO;
    }

    @Override
    public void execute(ImmutableMultimap<String, String> immutableMultimap, PrintWriter printWriter)  {
        Session session = sessionFactory.openSession();
        try {
            ManagedSessionContext.bind(session);
            Transaction transaction = session.beginTransaction();
            try {
                registerCareers();
                transaction.commit();
            }
            catch (Exception e) {
                transaction.rollback();
                throw new RuntimeException(e);
            }
        } finally {
            session.close();
            ManagedSessionContext.unbind(sessionFactory);
        }
    }

    public void registerCareers() throws Exception {
        List<CareersDATA> careersUtepsa;
        careersUtepsa = careersAdapter.getAllCareers();
        for(CareersDATA careerUtepsa: careersUtepsa){
            if(careerDAO.getByCodeUtepsa(careerUtepsa.getCrr_codigo()) == null){
                Career career = new Career();
                career.setCodeUtepsa(careerUtepsa.getCrr_codigo().trim());
                career.setName(careerUtepsa.getCrr_descripcion().trim());
                long idFaculty = getFaculty(careerUtepsa.getSca_descripcion().trim());
                career.setFaculty(new Faculty(idFaculty));
                if(careerUtepsa.getCrr_pensumactual().trim().isEmpty()){
                    career.setCurrentPensum(1);
                }else{
                    career.setCurrentPensum(Integer.parseInt(careerUtepsa.getCrr_pensumactual().trim()) );
                }
                careerDAO.create(career);
            }
        }
    }

    private long getFaculty(String faculty){
        if(faculty.equals("FACULTAD DE CIENCIA Y TECNOLOGÍA")){
            return 1;
        }
        if(faculty.equals("FACULTAD DE CIENCIAS EMPRESARIALES")){
            return 2;
        }
        if(faculty.equals("FACULTAD DE CIENCIAS JURÍDICAS Y SOCIALES")){
            return 3;
        }
        if(faculty.equals("COLEGIO DE POST GRADO")){
            return 4;
        }
        return 5;
    }
}
