package com.utepsa.api.careers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.utepsa.models.Career;
import io.dropwizard.jackson.Jackson;
import org.junit.Before;
import org.junit.Test;

import static io.dropwizard.testing.FixtureHelpers.fixture;
import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by David on 08/11/2016.
 */
public class CareerTest {
    private static final ObjectMapper MAPPER = Jackson.newObjectMapper();

    private Career career;

    @Before
    public void setup() throws Exception {
        //this.career = new Career(1, "108", "Sistemas", "01", "FECT");
    }

    @Test
    public void serializesToJSON() throws Exception {
        final String expected = MAPPER.writeValueAsString(
                MAPPER.readValue(fixture("fixtures/career.json"), Career.class));

        assertThat(MAPPER.writeValueAsString(career)).isEqualTo(expected);
    }

    @Test
    public void deserializesFromJSON() throws Exception {
        assertThat(MAPPER.readValue(fixture("fixtures/career.json"), Career.class))
                .isEqualTo(this.career);
    }
}
