package com.utepsa.api.courses;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.utepsa.models.Course;
import io.dropwizard.jackson.Jackson;
import org.junit.Before;
import org.junit.Test;

import static io.dropwizard.testing.FixtureHelpers.fixture;
import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by JoseCarlos on 27-07-16.
 */
public class CourseTest {

    private static final ObjectMapper MAPPER = Jackson.newObjectMapper();

    private Course course;

    @Before
    public void setup() throws Exception {
        //this.course = new Course(1, "00003", "AAD-140", "ADMINISTRACION 140", "002","01");
    }

    @Test
    public void serializesToJSON() throws Exception {
        final String expected = MAPPER.writeValueAsString(
                MAPPER.readValue(fixture("fixtures/course.json"), Course.class));

        assertThat(MAPPER.writeValueAsString(course)).isEqualTo(expected);
    }

    @Test
    public void deserializesFromJSON() throws Exception {
        assertThat(MAPPER.readValue(fixture("fixtures/course.json"), Course.class))
                .isEqualTo(this.course);
    }
}
