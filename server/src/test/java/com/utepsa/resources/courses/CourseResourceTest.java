package com.utepsa.resources.courses;

import com.utepsa.models.Course;
import com.utepsa.db.course.CourseDAO;
//import com.utepsa.db.course.CourseFakeDAO;
import io.dropwizard.testing.junit.ResourceTestRule;
import org.apache.http.HttpStatus;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by Roberto Perez on 01-08-16.
 */
public class CourseResourceTest {

    /*final private static CourseDAO dao = new CourseFakeDAO();
    final private static CoursesService coursesService = new CoursesService(dao);

    @ClassRule
    public static final ResourceTestRule resources = ResourceTestRule.builder()
            .addResource(new CoursesResource(coursesService))
            .build();

    private Course course;
    private List<Course> courses;

    @Before
    public void setUp() {
        this.courses = new ArrayList<>();
        courses.add(new Course(1, "00003", "AAD-140", "ADMINISTRACION 140", "001","03"));
        courses.add(new Course(2, "00003", "AAD-140", "ADMINISTRACION 140", "002","02"));
        courses.add(new Course(3, "00003", "AAD-140", "ADMINISTRACION 140", "002","03"));

        courses.forEach(course -> this.dao.create(course));
    }

    @Test
    public void getExistingCourseWithGET() throws Exception {
        this.course = new Course(2, "00003", "AAD-140", "ADMINISTRACION 140", "002","02");
        Course response = resources.client().target("/courses/00003/002/02").request().get(Course.class);
        assertThat(response).isEqualTo(this.course).isNotNull();
    }

    @Test
    public void getNotExistingCourseWithGET() throws Exception {
        Course response = resources.client().target("/courses/0000asd/0asdf/fa2").request().get(Course.class);
        assertThat(response).isNull();
    }

    @Test
    public void createCourseWithPOST() throws Exception {
        this.course = new Course(4, "00003", "AAD-140", "ADMINISTRACION 140", "003","01");
        Response response = resources.client().target("/courses").request().post(Entity.json(this.course));
        assertThat(response.getStatus()).isEqualTo(HttpStatus.SC_CREATED);
    }

    @Test
    public void getCoursesByCareerAndPensumWithGET() throws Exception{
        this.courses = new ArrayList<>();
        courses.add(new Course(1, "00003", "AAD-140", "ADMINISTRACION 140", "001","03"));

        List<Course> response = resources.client().target("/courses/001/03").request().get(new GenericType<List<Course>>(){});
        assertThat(response).isEqualTo(this.courses).isNotNull();
    }*/
}
