package com.utepsa.integration;

import com.utepsa.NotificationServerApp;
import com.utepsa.config.NotificationServerConfig;
import io.dropwizard.client.JerseyClientBuilder;
import io.dropwizard.testing.ResourceHelpers;
import io.dropwizard.testing.junit.DropwizardAppRule;
import org.apache.http.HttpStatus;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;

import javax.ws.rs.client.Client;
import javax.ws.rs.core.Response;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by Gerardo on 21/11/2016.
 */
public class GetStudentRegisterAcceptanceTest {
    private static final String CONFIG_PATH = ResourceHelpers.resourceFilePath("config-test.yml");

    @ClassRule
    public static final DropwizardAppRule<NotificationServerConfig> RULE =
            new DropwizardAppRule<>(NotificationServerApp.class, CONFIG_PATH);

    @Test
    public void requestStudentRegisterWithExistingRegisterCode() {
        Client client = new JerseyClientBuilder(RULE.getEnvironment()).build("Existing Student Register");

        Response response = client.target(
                String.format("http://localhost:%d/api/studentRegister/0000403201", RULE.getLocalPort()))
                .request().get();

        assertThat(response.getStatus()).isEqualTo(HttpStatus.SC_OK);
    }

    @Test
    public void requestStudentRegisterWithNotExistingRegisterCode() {
        Client client = new JerseyClientBuilder(RULE.getEnvironment()).build("Not Existing Register Code");

        Response response = client.target(
                String.format("http://localhost:%d/api/studentRegister/1000", RULE.getLocalPort()))
                .request().get();

        assertThat(response.getStatus()).isEqualTo(HttpStatus.SC_NO_CONTENT);
    }
}
