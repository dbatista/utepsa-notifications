var hourToTimestamp;
        
var formatRegisterCode = function(accountOrRegisterCode, tamano) {
    return new Array(tamano + 1 - (accountOrRegisterCode + '').length).join('0') + accountOrRegisterCode;
}

function LlenarComboBox()
{ 
  $.ajax({
    type: "GET",
    contentType: "application/json; charset=utf-8",
    url: "http://52.43.108.208:9090/api/careers",
    data: { get_param: 'value' },
    dataType: "json",
    success: function (data) {
      var selector=document.getElementById("career");
      for (var i = 0; i < data.length; i++) {
        selector.options[i]=new Option(data[i].description.trim(),data[i].careerCode);
      }
    },
    error: function (result) {
      alert("Error");
    }
  });
}

function sendData(){
  var register = formatRegisterCode($("#register").val(),10);
  if(register == ""){
    if (document.getElementById("careerCheck").checked) 
    {
      register = $("#career option:selected").val();
    }
    else
    {
      register = "PUBLIC"
    }
  }
  var formData = 
  {
    "id": 0,
    "audience": [
    {
      "target": register,
      "type": type
    }
    ],
    "creationTimestamp": hourToTimestamp,
    "title": $("#title").val(),
    "content": $("#contentArea").val(),
    "type": $("#optionNotice").val()
    }
  $.ajax({
   url : "http://52.43.108.208:9090/api/notifications",
   type: 'post', 
   data : JSON.stringify(formData),
   contentType: 'application/json',
   dataType: 'json',
   statusCode: {
   201: function (response){
    alert("notificacion enviada a los estudiantes");
   },
   406: function (response){
    alert("notificacion no enviada a los estudiantes");
   }
  }
  });
}

function showContent(){
  element = document.getElementById("content");
  element2 = document.getElementById("career");
  element3 = document.getElementById("career2");
  check = document.getElementById("publicCheck");
  check = document.getElementById("studentCheck");
  check1 = document.getElementById("careerCheck");
  if (check1.checked) 
    {  
      element2.style.display='block';
      element3.style.display='block';
      element.style.display='none';
    }
  else {
    element2.style.display = 'none';
    element3.style.display='none';      
  }
  if(check.checked) 
      element.style.display='block';
  else {
      element.style.display = 'none';
  }
}

var type;
function typeNotice(){
  check1 = document.getElementById("publicCheck");
  check1 = document.getElementById("studentCheck");
  check2 = document.getElementById("careerCheck");
  if(check1.checked)
    type="STUDENT";
  else 
  {
    if(check2.checked)
      type="CAREER";
    else{type="PUBLIC";}
  }
}

function cleanTextBox(){
    document.getElementById("contentArea").value = null;
    document.getElementById("title").value = null;
    document.getElementById("register").value = null;
}
 
var dateFull = new Date();
var day = dateFull.getDate();
var month = dateFull.getMonth();
var year = dateFull.getFullYear();
var hour = dateFull.getHours();
var minutes = dateFull.getMinutes();
var seconds = dateFull.getSeconds();

function humanToTime(){
  var humDate = new Date(Date.UTC(year,
  (month),
  (day),
  (hour),
  (minutes),
  (seconds)));
  hourToTimestamp = (humDate.getTime()/1000.0);
}

